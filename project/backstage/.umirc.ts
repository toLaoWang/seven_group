import { defineConfig } from 'umi';
// import MonacoWebpackPlugin from 'monaco-editor-webpack-plugin';
export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  fastRefresh: {},
  antd: {},
  dva: {
    immer: true,
    hmr: false,
  }
});
