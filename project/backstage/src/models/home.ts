import {makeAutoObservable} from 'mobx';

class Home{
    constructor(){
        makeAutoObservable(this);
    }
}

export default Home;
