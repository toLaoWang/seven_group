import { makeAutoObservable, runInAction } from 'mobx';
import { deleteFile, getFileList } from '@/services'
import { IOptions, FileList } from '@/types'
import { message } from 'antd'
class File {
    fileList: FileList[] = []
    total = 0
    constructor() {
        makeAutoObservable(this);
    }
    async getFileList(obj: IOptions) {
        let result = await getFileList(obj)
        if (result.statusCode === 200) {
            runInAction(() => {
                this.fileList = result.data[0]
                this.total = result.data[1]
            })
        }
    }
    async deleteFile(id: string) {
        let result = await deleteFile(id)
        return result
    }
}

export default File;
