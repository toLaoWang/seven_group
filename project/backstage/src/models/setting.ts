import { commitModification, getSetting, testEmail } from '@/services';
import { Analytics, Seo, SettingObject, Smtp, SystemSettings } from '@/types/module/setting';
import { makeAutoObservable } from 'mobx';
import { Drawer, Button, message } from 'antd';
class Setting {
    settings: Partial<SettingObject> = {}
    constructor() {
        makeAutoObservable(this);
    }
    async getSetting() {
        let result = await getSetting()
        if (result.statusCode === 200) {
            console.log(JSON.parse(result.data.i18n));
            
            result.data.i18n=JSON.parse(result.data.i18n)
            this.settings = result.data
        }
    }
    async commitModification(options: SystemSettings | Seo | Analytics | Smtp) {
        let result = await commitModification(options)
        if (result.statusCode === 201) {
            this.getSetting()
        }
    }
    async testEmail() {
        let result = await testEmail({
            subject: "测试",
            text: "测试邮件",
            to: "5"
        })
        if (result.statusCode === 200) {
            message.success("测试成功")
        }else{
            message.error("测试失败")
        }
    }
    changeSettings(value: string, type: keyof SettingObject) {
        this.settings[type] = value
    }
}

export default Setting;
