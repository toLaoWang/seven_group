import { getPageList, getDelPage, getOutPage } from '@/services';
import { IPageList } from '@/types';
import { makeAutoObservable } from 'mobx';
import {message} from 'antd'
import React from 'react';

class Page {
  pageLise: IPageList[] = [];
  pageCount = 0

  constructor() {
    makeAutoObservable(this);
  }
  // 页面列表
  async getPageList(data: { page: number, pageSize?: number, name?: string, path?: string, status?: string }) {
    let result = await getPageList(data);
    if (result.statusCode === 200) {
      this.pageLise = result.data[0];
      this.pageCount = result.data[1]
    }
  }
  // 删除
  async getDelPage(id: string) {
    let result = await getDelPage(id);
    return result
  }
  // 下线
  async getOutPage(id: string, status: string) {
    let result = await getOutPage({ id, status });
    return result
  }
  // 下线
  async getOutPages(array: React.Key[],status:string) {
    array.forEach(async item => {
      let result = await getOutPage({ id: item as string, status});
      if (result.statusCode === 200) {
        message.success('操作成功');
        this.getPageList({page:1,pageSize:12})
      }
    })
  }

}

export default Page;
