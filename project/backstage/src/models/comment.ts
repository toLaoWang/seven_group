import { makeAutoObservable, runInAction } from 'mobx';
import { commentData, searchComment, updateComment, deleteComment, getArticleList } from '@/services'
import { commentItem, commentPagePageSize, commentRuery } from '@/types';
import { message } from 'antd';
import { IArticleItem } from '@/types/module/article';

class Comment {
    commentList: Array<commentItem> = [];
    commentNum: number = 0;
    page: number = 1;//原始页码
    pageSize: number = 6;//原始每一页数据
    articleWorkList: Array<IArticleItem> = [];
    constructor() {
        makeAutoObservable(this);
    }

    //获取页面文章前六条数据
    async getarticleWorkList(data: { page: number, pageSize?: number, title?: string, status?: string, category?: string }) {
        let result = await getArticleList(data);
        console.log(result);
        if (result.statusCode === 200) {
            this.articleWorkList = result.data[0];
            console.log(this.articleWorkList);
        }
    }

    //获取评论页数据
    async commentData(data: commentPagePageSize) {
        let result = await commentData(data);
        console.log('login result...', result);
        if (result.data) {
            runInAction(() => {
                this.commentList = result.data[0];
                this.commentNum = result.data[1]
            })
        }
    }

    //模糊搜索
    async searchComment(data: commentPagePageSize, params: commentRuery) {
        let result = await searchComment(data, params);
        console.log('login result...', result);
        if (result.data) {
            runInAction(() => {
                this.commentList = result.data[0];
                this.commentNum = result.data[1]
            })
        }
    }

    //更改评论页的状态
    async updateComment(ids: string[], status: { [key: string]: boolean }, page: number, pageSize: number) {
        message.loading('操作中');
        Promise.all(ids.map(id => updateComment(id, status)))
            .then(res => {
                message.destroy();
                message.success('操作成功');
                this.commentData({ page, pageSize })
            })
            .catch(err => {
                message.destroy();
                message.error('操作失败')
            })
    }

    //删除评论
    async delComment(ids: string[], page: number, pageSize: number) {
        message.loading('操作中');
        Promise.all(ids.map(id => deleteComment(id)))
            .then(res => {
                message.destroy();
                message.success('操作成功');
                this.commentData({ page, pageSize })
            })
            .catch(err => {
                message.destroy();
                message.error('操作失败')
            })
    }

}

export default Comment;
