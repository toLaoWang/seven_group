import { deleteKnowledge, getKnowledge, knowledgeAdd, knowledgeStatus } from '@/services';
import { IImageObject, IObj, IParam, IParams, IParamsNew, IRootObject } from '@/types';
import { makeAutoObservable } from 'mobx';
import { message} from 'antd';
class KnowLedge {
  total = 0;
  visible = false;
  KnowLedgeList: Array<IRootObject> = [];
  imgList: IImageObject[] = [];
  allNum = 0;
  flag = false;
  cover='';
  content='更新';
  constructor() {
    makeAutoObservable(this);
  }
  async getKnowledge(
    page: number,
    limit: number,
    params: Partial<IParams>,
    num?: number,
    obj?: Partial<IObj>,
  ) {
    let result = await getKnowledge(page, limit, params, num, obj!);
    if (num) {
      if (result.statusCode == 200) {
        this.imgList = result.data[0];
        this.allNum = result.data[1];
      }
    } else {
      if (result.statusCode == 200) {
        this.KnowLedgeList = result.data[0].map((item: IRootObject) => {
          item.type = false;
          return item;
        });
        this.total = result.data[1];
      }
    }
  }
  async deleteKnowledge(id: string) {
    let result = await deleteKnowledge(id);
    if (result.statusCode == 200) {
      this.KnowLedgeList = this.KnowLedgeList.filter(
        (item) => item.title !== result.data.title,
      );
    }
  }

  async knowledgeStatus(id: string, status: string) {  
    let result = await knowledgeStatus(id, status);
  }
  async knowledgeAdd(id: string, status: IParam) {  
    let result = await knowledgeAdd(id, status);
    if(result.statusCode == 200  ){
    message.success('更新成功')
    }
  }
  
  // async newKnowledge(params:IParamsNew) {  
  //   let result = await newKnowledge(params);
  // }
}

export default KnowLedge;
