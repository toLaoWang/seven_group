import { makeAutoObservable } from 'mobx';
import { getUserList, editUserList } from '@/services';
import { IUserList, IUserParams } from "@/types";
import { message } from "antd";
class User {
    userList: Array<IUserList> = [];
    total: number = 0;
    constructor() {
        makeAutoObservable(this);
    }
    async getUserList(page: number, pageSize: number, params: Partial<IUserParams> = { }) {
        let result = await getUserList(page, pageSize, params);
        if (result.data) {
            this.userList = result.data[0];
            this.total = result.data[1];
        }
        return result.data;
    }

    async editUserList(arr: IUserList[], page: number, pageSize: number, params: Partial<IUserParams> = { }) {
        message.loading('操作中');
        Promise.all(arr.map(item => editUserList(item)))
            .then(res => {
                message.destroy();
                message.success('操作成功');
                this.getUserList(page, pageSize, params);
            })
            .catch(err => {
                message.error('操作失败');
                this.getUserList(page, pageSize, params);
            })
    }
}
export default User;
