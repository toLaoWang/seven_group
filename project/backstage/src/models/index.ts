import Article from './article'
import Comment from './comment'
import File from './file'
import Home from './home'
import KnowLedge from './knowLedge'
import Login from './login'
import Mail from './mail'
import OwnSpace from './ownSpace'
import Poster from './poster'
import Search from './search'
import Setting from './setting'
import User from './user'
import View from './view'
import Page from './page'

export default {
    article: new Article,
    comment: new Comment,
    file: new File,
    home: new Home,
    knowLedge: new KnowLedge,
    login: new Login,
    mail: new Mail,
    page: new Page,
    ownSpace: new OwnSpace,
    poster: new Poster,
    search: new Search,
    setting: new Setting,
    user: new User,
    view: new View,
}
