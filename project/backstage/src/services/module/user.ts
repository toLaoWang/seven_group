import { request } from 'umi';
import { IUserParams ,IUserList} from "@/types";

//获取搜索记录数据与搜索功能
export function getUserList(page: number = 1, pageSize: number = 12, params: Partial<IUserParams> = { }) {
    return request(`/api/user?page=${page}&pageSize=${pageSize}`, { params })
}



export function editUserList(userObj: IUserList) {
    return request('/api/user/update', { method: 'POST', data: { ...userObj } })
}

