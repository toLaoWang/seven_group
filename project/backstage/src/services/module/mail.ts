import { request } from 'umi'
import {Params} from '@/types'
export const getEmailList = (obj:Params) => {
    return request("/api/smtp",{params:obj})
}
export const deleteEmail = (id:string) =>{
    return request('/api/smtp',{params:{id}})
}