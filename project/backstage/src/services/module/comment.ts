import { request } from 'umi'
import { commentPagePageSize, commentRuery } from '@/types'

// 获取评论
export function commentData(data: commentPagePageSize) {
    return request(`/api/comment?page=${data.page}&pageSize=${data.pageSize}`)
}

//评论模糊搜索
export function searchComment(data: commentPagePageSize, params: commentRuery) {
    return request(`/api/comment?page=${data.page}&pageSize=${data.pageSize}&name=${params.name ? params.name : ''}&email=${params.email ? params.email : ''}&pass=${params.pass ? params.pass : ''}`)
}

//更改评论状态
export function updateComment(id: string, data: { [key: string]: boolean } = { }) {
    return request(`/api/comment/${id}`, {
        method: 'PATCH',
        data
    })
}

//删除评论
export function deleteComment(id: string) {
    return request(`/api/comment/${id}`, {
        method: 'DELETE'
    })
}
