import { request } from 'umi'
// 获取页面列表
export const getPageList = function (payload: {page:number,pageSize?:number,name?:string,path?:string,status?:string}) {
   return request('/api/page', { params: payload })
}
// 删除
export const getDelPage = function (id:string) {
   return request(`/api/page/${id}`,{method:'DELETE'})
}
// 下线
export const getOutPage = function (data:{id:string,status:string}) {
   return request(`/api/page/${data.id}`,{method:'PATCH',data:{status:data.status}})
}