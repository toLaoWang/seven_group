import { request } from 'umi';
import { IViewParams } from "@/types";
//获取访问统计数据
export function getViewList(page: number, pageSize: number = 12, params: Partial<IViewParams> = { }) {
    return request(`/api/view?page=${page}&pageSize=${pageSize}`, {params})
}
//删除
export function delViewList(id: string) {
    return request(`/api/view/${id}`, {
        method: 'DELETE',
    })
}

