import { IObj, IParam, IParams, IParamsNew } from '@/types';
import {request} from 'umi';

//获取数据请求
export function getKnowledge(page:number,limit:number,params:Partial<IParams>={},num?:number,obj:Partial<IObj>={}) { 
    return request(`/api/${num ? 'file' :'knowledge'}?page=${page}&pageSize=${limit}${obj?.originalname ? '&originalname=' +`${obj?.originalname}` : ''}${obj?.type ? '&type=' +`${obj?.type}` : ''}`,{
        params,
    })
 }

//发布上线下线
export function knowledgeStatus(id:string,status:string) {   
      console.log(status);
    return request(`/api/knowledge/${id}`,{
        method:'PATCH',
        data: {
            status
        }
    })
 }

 //发布添加编辑
export function knowledgeAdd(id:string,status:IParam) {   
    console.log(status);
  return request(`/api/knowledge/${id}`,{
      method:'PATCH',
      data: status
  })
}
 //删除请求

 export function deleteKnowledge(id:string) {  
    return request(`/api/knowledge/${id}`,{
        method:'DELETE'
    })
 }

 //新建接口
//  export function newKnowledge(params:IParamsNew) {  
//     return request(`/api/knowledge/book`,{
//         method:'POST',
//         data:{
//             params
//         }
//     })
//  }