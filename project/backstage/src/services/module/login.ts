import {request} from 'umi'
import {Registry} from '@/types/index'
export const member = (payload:Registry,url:string) => {
    if(url==="auth/login") delete payload.confirm
    return request(`/api/${url}`,{method:"POST",data:payload})
}