import { message } from 'antd';
import { RequestConfig, history } from 'umi';
import StateContext from '@/context/stateContext'
import store from '@/models'
import React from 'react';
import { getToken } from './utils';
const whiteList = ['/login', '/registry']
export function onRouteChange({location}:any){  
  let authorization = getToken();
  if (authorization) {
    if (location.pathname === '/login') {
      history.replace('/')
    }
  }else{
    console.log(!whiteList.includes(location.pathname));
    if(!whiteList.includes(location.pathname)){
      console.log(location.pathname);
      history.replace('/login?from='+encodeURIComponent(location.pathname))
    }
  }
}
export function rootContainer(container: React.ReactNode) {
  return React.createElement(StateContext.Provider, { value: store }, container);
}
let basrUrl = "https://creationapi.shbwyz.com";
export const request: RequestConfig = {
  timeout: 10000,
  errorConfig: {},
  middlewares: [],
  requestInterceptors: [
    (url, options) => {
      const token = `Bearer ${getToken()}`
      return {
        url: `${basrUrl}${url}`,
        options: { ...options, interceptors: true, headers: { authorization: token } },
      };
    },
  ],
  responseInterceptors: [
    response => {
      const codeMaps: { [key: string]: string } = {
        // 400:'文章标题已存在',
        403: "访客无权进行该操作",
        502: '网关错误。',
        503: '服务不可用，服务器暂时过载或维护。',
        504: '网关超时。',
      };
      if (Object.keys(codeMaps).includes(response.status.toString())) {
        message.error(codeMaps[response.status]);
      }
      return response;
    }
  ],
};