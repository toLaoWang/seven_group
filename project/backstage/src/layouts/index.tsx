import React from 'react';
import { Layout, Menu, Button, Dropdown, Affix, Avatar } from 'antd';
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
} from '@ant-design/icons';
import './index.less';
import { useHistory } from 'umi';
import { useState } from 'react';
import { NavLink } from 'react-router-dom';
import { urlMatching, urlMatch } from '@/utils/urlRule';
import { removeToken } from '@/utils';
const { Header, Sider, Content, Footer } = Layout;
const { SubMenu } = Menu;
const Index: React.FC = (props) => {
  const menu = (
    <Menu onClick={onClick}>
      <Menu.Item key="/article/amEditor">新建文章-协同编辑器</Menu.Item>
      <Menu.Item key="/article/editor">新建文章</Menu.Item>
      <Menu.Item key="/page/editor">新建页面</Menu.Item>
    </Menu>
  );
  const menus = (
    <Menu onClick={onClick}>
      <Menu.Item key="/onwspace">个人中心</Menu.Item>
      <Menu.Item key="/user">用户管理</Menu.Item>
      <Menu.Item key="/setting">系统设置</Menu.Item>
      <Menu.Item key="/login">退出登录</Menu.Item>
    </Menu>
  );
  function onClick({ key }: { key: string }) {
    if (key == '/login') {
      localStorage.removeItem('user');
      removeToken();
    }
    history.push(key);
  }
  const history = useHistory();
  const [collapsed, setCollapsed] = useState(false);
  function toggle() {
    setCollapsed(!collapsed);
  }
  let whitelist = ['/editor', '/login', '/amEditor', '/qChatRoom', '/chatroom'];
  return (
    <div id="exterior">
      {!whitelist.some((item) => history.location.pathname.includes(item)) ? (
        <Layout>
          <Sider
            style={{
              overflow: 'hidden',
              height: '100vh',
              left: 0,
            }}
            trigger={null}
            collapsible
            collapsed={collapsed}
          >
            <div className="logo" />
            <div className="topuser">
              <img
                style={{ width: '32px', height: '32px', borderRadius: '4px' }}
                src="https://bwcreation.oss-cn-beijing.aliyuncs.com/2021-07-26/logo2.4a842ec4.png"
                alt=""
              />
              {!collapsed && (
                <span style={{ marginLeft: '4px' }}>管理后台</span>
              )}
            </div>
            <div className="newbtn">
              <Dropdown overlay={menu}>
                <Button type="primary" size="large">
                  +{!collapsed && <span>新建</span>}
                </Button>
              </Dropdown>
            </div>
            <Menu theme="dark" mode="inline">
              {urlMatching.map((item, index) => {
                if (!item.children) {
                  return (
                    <Menu.Item key={index} style={{ background: 'none' }}>
                      <NavLink
                        exact
                        activeClassName="layputicon"
                        activeStyle={{ color: 'var(--primary-color)' }}
                        to={item.to!}
                      >
                        <item.icon style={{ marginRight: '10px' }} />
                        {item.title}
                      </NavLink>
                    </Menu.Item>
                  );
                } else {
                  return (
                    <SubMenu
                      key={index * 100}
                      icon={<item.icon />}
                      title={item.title}
                    >
                      {item.children.map((item1, index1) => {
                        return (
                          <Menu.Item
                            key={index1 + 20}
                            style={{ background: 'none' }}
                          >
                            <NavLink
                              exact
                              activeClassName="layputicon"
                              activeStyle={{ color: 'var(--primary-color)' }}
                              to={item1.to!}
                            >
                              {' '}
                              <item1.icon style={{ marginRight: '10px' }} />
                              {item1.title}
                            </NavLink>
                          </Menu.Item>
                        );
                      })}
                    </SubMenu>
                  );
                }
              })}
            </Menu>
          </Sider>
          <Layout className="site-layout">
            <Affix>
              <Header className="site-layout-background" style={{ padding: 0 }}>
                {React.createElement(
                  collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                  {
                    className: 'trigger',
                    onClick: toggle,
                  },
                )}
                <Dropdown overlay={menus}>
                  <div style={{ marginRight: '16px' }}>
                    <Avatar
                      src={JSON.parse(localStorage.user).avatar}
                      icon={<UserOutlined />}
                    />
                    <span className="avatar_span">
                      Hi，{JSON.parse(localStorage.user).name}
                    </span>
                  </div>
                </Dropdown>
              </Header>
            </Affix>
            <Content>
              <Affix offsetTop={64}>
                <header className="lay_header">
                  {history.location.pathname !== '/onwspace' && (
                    <p>
                      <NavLink to="/" style={{ color: '#333' }}>
                        工作台
                      </NavLink>
                      {history.location.pathname
                        .match(/(\w)+/g)
                        ?.map((item, index) => {
                          return (
                            <NavLink
                              key={index}
                              style={{ color: '#333' }}
                              to={`/${
                                index == 0
                                  ? item
                                  : history.location.pathname
                                      .match(/(\w)+/g)
                                      ?.join('/')
                              }`}
                            >
                              &nbsp;/&nbsp;{`${urlMatch[item]}`}
                            </NavLink>
                          );
                        })}
                    </p>
                  )}
                  {history.location.pathname == '/' && (
                    <div>
                      <h1>您好，{JSON.parse(localStorage.user).name}</h1>
                      <p>
                        您的角色：
                        {JSON.parse(localStorage.user).role == 'visitor'
                          ? '访客'
                          : '管理员'}
                      </p>
                    </div>
                  )}
                </header>
              </Affix>
              <main
                className="site-layout-background"
                style={{
                  margin: 0,
                  padding: '24px 16px',
                  overflowY: 'scroll',
                  minHeight: '70%',
                  maxHeight: '85%',
                  background: '#f5f5f5',
                  boxSizing: 'border-box',
                }}
              >
                {props.children}
                <Footer style={{ textAlign: 'center', background: 'none' }}>
                  Ant Design ©2018 Created by Ant UED
                </Footer>
              </main>
            </Content>
          </Layout>
        </Layout>
      ) : (
        props.children
      )}
    </div>
  );
};
export default Index;
