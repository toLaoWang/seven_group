export interface mailObject {
  id: string;
  from: string;
  to: string;
  subject: string;
  text?: any;
  html: string;
  createAt: string;
}
export interface Params {
  page: number;
  pageSize: number;
  from?: string;
  to?: string;
  subject?: string;
}