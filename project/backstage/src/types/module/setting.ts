export interface SettingObject {
  id: string;
  i18n: language;
  systemUrl: string;
  systemTitle: string;
  systemLogo: string;
  systemFavicon: string;
  systemFooterInfo: string;
  adminSystemUrl: string;
  baiduAnalyticsId: string;
  googleAnalyticsId?: any;
  seoKeyword: string;
  seoDesc: string;
  oss: string;
  smtpHost: string;
  smtpPort: string;
  smtpUser: string;
  smtpPass: string;
  smtpFromUser: string;
  createAt: string;
  updateAt: string;
}

export interface SystemSettings {
  systemUrl: string;
  systemTitle: string;
  systemLogo: string;
  systemFavicon: string;
  systemFooterInfo: string;
  adminSystemUrl: string;
}
export interface Seo {
  seoKeyword: string;
  seoDesc: string;
}
export interface Analytics {
  baiduAnalyticsId: string;
  googleAnalyticsId: string;
}
export interface Smtp {
  smtpHost: string;
  smtpPort: string;
  smtpUser: string;
  smtpPass: string;
  smtpFromUser: string;
}



export interface  language {
  en: any;
  zh: any;
  [key:string]:any
}

// interface En {
//   zh: string;
//   en: string;
//   serverNotAvaliable: string;
//   pageMissing: string;
//   archives: string;
//   total: string;
//   totalSearch: string;
//   piece: string;
//   passwd: string;
//   wrongPasswd: string;
//   protectedArticleMsg: string;
//   backHome: string;
//   confirm: string;
//   unknownTitle: string;
//   articleCover: string;
//   publishAt: string;
//   readings: string;
//   copyrightInfo: string;
//   copyrightContent: string;
//   categoryArticle: string;
//   gettingArticle: string;
//   comment: string;
//   gettingKnowledge: string;
//   knowledgeBooks: string;
//   readingCount: string;
//   startReading: string;
//   pleaseWait: string;
//   otherKnowledges: string;
//   unknownKnowledgeChapter: string;
//   recommendToReading: string;
//   yu: string;
//   tagRelativeArticles: string;
//   all: string;
//   readingCountTemplate: string;
//   articleCountTemplate: string;
//   share: string;
//   empty: string;
//   categoryTitle: string;
//   commentNamespace: CommentNamespace;
//   loading: string;
//   copySuccess: string;
//   copy: string;
//   article: string;
//   searchArticle: string;
//   searchArticlePlaceholder: string;
//   shareNamespace: ShareNamespace;
//   tagTitle: string;
//   toc: string;
// }

// interface ShareNamespace {
//   title: string;
//   createingPoster: string;
//   createdPosterSuccess: string;
//   createdPosterError: string;
//   qrcode: string;
//   shareFrom: string;
// }

// interface CommentNamespace {
//   reply: string;
//   emoji: string;
//   replyPlaceholder: string;
//   publish: string;
//   close: string;
//   commentSuccess: string;
//   userInfoTitle: string;
//   userInfoName: string;
//   userInfoNameValidMsg: string;
//   userInfoEmail: string;
//   userInfoEmailValidMsg: string;
//   userInfoConfirm: string;
//   userInfoCancel: string;
// }