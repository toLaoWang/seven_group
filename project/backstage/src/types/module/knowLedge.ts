
//数据接口
export interface IRootObject {
  [x: string]: any;
  id: string;
  parentId?: any;
  order: number;
  title: string;
  cover: string;
  summary: string;
  content?: any;
  html?: any;
  toc?: any;
  status: string;
  views: number;
  likes: number;
  iscommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  type:boolean;
}

export interface IParams{
    title:string;
    status:string;
}

export interface IObj{
  originalname:string;
  type:string
}

export interface IImageObject {
  id: string;
  originalname: string;
  filename: string;
  type: string;
  size: number;
  url: string;
  createAt: string;
}

export interface IParam{
  cover?:string;
  iscommentable?:boolean;
  summary?:string;
  title?:string;
}

export interface IParamsNew{
  cover:string;
  isCommentable:boolean;
  summary:string;
  title:string;
}