export interface commentPagePageSize {
    page: number;
    pageSize: number
}

export interface commentItem {
    id: string;
    name: string;
    email: string;
    content: string;
    html: string;
    pass: boolean;
    userAgent: string;
    hostId: string;
    url: string;
    parentCommentId?: string;
    replyUserName?: string;
    replyUserEmail?: string;
    createAt: string;
    updateAt: string;
    key?: string;
    parentcontent?: string;
}

export interface commentRuery {
    name: string | undefined;
    email: string | undefined;
    pass: string | undefined;
}