export interface IUserList {
    id: string;
    key: string;
    name: string;
    avatar?: string;
    email?: string;
    role: string;
    status: string;
    createAt: string;
    updateAt: string;
}

export interface IUserParams {
    page: number;
    pageSize: number;
    name: string;
    email: string;
    role: string;
    status:string;
}