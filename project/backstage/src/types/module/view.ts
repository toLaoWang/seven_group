
//访问统计分页数据类型
export interface IViewList {
    key: string;
    id: string;
    ip: string;
    userAgent: string;
    url: string;
    count: number;
    address: string;
    browser: string;
    engine: string;
    os: string;
    device: string;
    createAt: string;
    updateAt: string;
}
//访问统计搜索数据类型
export interface IViewParams {
    page: number;
    pageSize: number;
    ip: string;
    userAgent: string;
    url: string;
    address: string;
    browser: string;
    engine: string;
    os: string,
    device:string
}





export interface ViewEcharts {
    tooltip: Tooltip;
    toolbox: Toolbox;
    legend: Legend;
    xAxis: XAxi[];
    yAxis: YAxi[];
    series: Series[];
}

interface Series {
    name: string;
    type: string;
    data: number[];
    yAxisIndex?: number;
}

interface YAxi {
    type: string;
    name: string;
    min: number;
    max: number;
    interval: number;
    axisLabel: AxisLabel;
}

interface AxisLabel {
    formatter: string;
}

interface XAxi {
    type: string;
    data: string[];
    axisPointer: AxisPointer2;
}

interface AxisPointer2 {
    type: string;
}

interface Legend {
    data: string[];
}

interface Toolbox {
    feature: Feature;
}

interface Feature {
    dataView: DataView;
    magicType: MagicType;
    restore: Restore;
    saveAsImage: Restore;
}

interface Restore {
    show: boolean;
}

interface MagicType {
    show: boolean;
    type: string[];
}

interface DataView {
    show: boolean;
    readOnly: boolean;
}

interface Tooltip {
    trigger: string;
    axisPointer: AxisPointer;
}

interface AxisPointer {
    type: string;
    crossStyle: CrossStyle;
}

interface CrossStyle {
    color: string;
}