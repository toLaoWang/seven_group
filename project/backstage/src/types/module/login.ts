export interface Registry {
  name: string;
  password: string;
  confirm?: string;
}