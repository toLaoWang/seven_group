import React, { useEffect } from 'react'
import Viewer from 'viewerjs';
import 'viewerjs/dist/viewer.css';
import { observer } from 'mobx-react-lite';
const ImageView: React.FC = (props) => {
    const dom = React.createRef<HTMLDivElement>();
    useEffect(() => {
        //创建查看器
        const viewer = new Viewer(dom.current!);
        //提供监视对DOM树所做更改的功能。它是作为DOM3事件规范中较旧的Mutation Events特性的替代而设计的。
        let ob = new MutationObserver(() => {
            viewer.update();
        });
        //指示用户代理观察给定的目标(节点)，并根据选项(对象)给出的标准报告任何变化。
        ob.observe(dom.current!, {
            //如果观察到突变不仅针对目标，而且针对目标的后代，则设为true。   
            childList: true,
            //如果观察到目标子节点的突变，则设为true。
            subtree: true
        })
        return () => {
            //阻止观察者观察任何突变。在再次使用observe()方法之前，不会调用观察者的回调函数
            ob.disconnect();
            //销毁查看器并移除实例
            viewer.destroy()
        }

    }, [])
    return (
        <div ref={dom}>
            {
                props.children
            }
        </div>
    )
}

export default observer(ImageView);