import useStore from '@/context/useStore';
import { InboxOutlined } from '@ant-design/icons';
import { observer } from 'mobx-react-lite';
import './index.less';
import { Drawer, Form, Button, Input, Switch, Upload, message } from 'antd';
import { useEffect, useState } from 'react';
import { IParam, IRootObject } from '@/types';
interface IProp {
  name: string;
  multiple: boolean;
  action: string;
  onChange(info: any): void;
  onDrop(e: any): void;
 
}
interface IProps {
  item: IRootObject;
}
const IDrawer: React.FC<IProps> = (props) => {
  const store = useStore();
  let { item } = props;
  const [form] = Form.useForm();
  const [page, setpage] = useState(1);
  const [limit, setlimit] = useState(12);
  const [params, setParams] = useState({});
  const [check, setCheck] = useState(false);
  useEffect(() => {
    if(store.knowLedge.cover){
      item.cover=store.knowLedge.cover;
    }
      form.setFieldsValue(item);
     if(!item.id && store.knowLedge.content){
      form.setFieldsValue({title:'',summary:'',iscommentable:false,cover:''});  
     }
  }, [item, props,store.knowLedge.cover]);
  useEffect(() => {}, [store.knowLedge.visible]);
  async function onClose(e:any) {
     if( e.target.childNodes[0] && e.target.childNodes[0].data == '更 新'){
      store.knowLedge.visible = false; 
      let { title, summary, iscommentable, cover }: Partial<IParam> =
        form.getFieldsValue();
        
      iscommentable = check;
      let res = await store.knowLedge.knowledgeAdd(item.id,{
        title,
        summary,
        iscommentable,
        cover,
      });
   
      let result = await store.knowLedge.getKnowledge(page, limit, params);
     }else{
      let { title, summary, iscommentable, cover }: Partial<IParam> =
      form.getFieldsValue();
      cover=store.knowLedge.cover;
     
       console.log( title, summary, iscommentable, cover,form.getFieldsValue());
      // let res = await store.knowLedge.newKnowledge()
     }
     store.knowLedge.visible = false;
  }
  useEffect(() => {}, [store.knowLedge.flag, item, props]);
  const { Dragger } = Upload;
  const prop: IProp = {
    name: 'file',
    multiple: true,
    action: `https://www.mocky.io/v2/5cc8019d300000980a055e76`,
    onChange(info) {
      const { status } = info.file;
      if (status !== 'uploading') {
        console.log(info.file, info.fileList);
      }
      if (status === 'done') {
        message.success(`${info.file.name} file uploaded successfully.`);
      } else if (status === 'error') {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
    onDrop(e) {
      console.log('Dropped files', e.dataTransfer.files);
    },
  };
  function handleFile(e: React.MouseEvent<HTMLElement, MouseEvent>) {
    store.knowLedge.getKnowledge(
      page,
      limit,
      params,
      e.currentTarget.firstChild!.ATTRIBUTE_NODE,
    );
    store.knowLedge.flag = true;
    store.knowLedge.visible = false;
    store.knowLedge.content='';
   
    
  }
  function handleDelete() {
    item.cover = '';
    store.knowLedge.cover='';
  }
  function onChange(checked: boolean) {
    setCheck(!check);
  }
  const onFinish = (values: any) => {
    console.log('Success:', values);
  };
  useEffect(() => {
    setCheck(item.isCommentable);
  }, [item]);
 
  
  
  return (
    
    <>
      <Drawer
        title="新建知识库"
        width={600}
        onClose={onClose}
        visible={store.knowLedge.visible}
        bodyStyle={{ paddingBottom: 80 }}
        footer={
          <div
            style={{
              textAlign: 'right',
            }}
          >
            <Button onClick={onClose} style={{ marginRight: 8 }}>
              取消
            </Button>
            {item.id ?  <Button onClick={onClose} type="primary">
            更新
            </Button>  : <Button onClick={onClose} type="primary">
            创建
            </Button> }
           
          </div>
        }
      >
        <Form
          wrapperCol={{ span: 20 }}
          form={form}
          layout="horizontal"
          onFinish={onFinish}
          name="validate_other"
          hideRequiredMark
        >
          <Form.Item
            colon={false}
            name="title"
            label="名称"
            style={{ alignItems: 'center' }}
          >
            <Input style={{ width: 480 }} placeholder="" value={item.title} />
          </Form.Item>

          <Form.Item
            colon={false}
            name="summary"
            style={{ alignItems: 'center' }}
            label="描述"
          >
            <Input.TextArea
              style={{ width: 480 }}
              placeholder=""
              value={item.summary}
            />
          </Form.Item>

          <Form.Item
            colon={false}
            name="iscommentable"
            label="评论"
            style={{ alignItems: 'center' }}
          >
            <Switch checked={check} onChange={onChange} />
          </Form.Item>

          <Form.Item
            colon={false}
            name="cover"
            label="封面"
            style={{ alignItems: 'center' }}
          >
            <div>
              <Dragger {...prop} style={{ background: '#fff', width: 480 }}>
                {item.cover ? (
                  <img
                    src={item.cover}
                    alt=""
                    style={{
                      width: '100%',
                      height: 150,
                      objectFit: 'cover',
                      objectPosition: 'center',
                    }}
                  />
                ) : (
                  <div>
                    <p className="ant-upload-drag-icon">
                      <InboxOutlined />
                    </p>
                    <p className="ant-upload-text">
                      点击选择文件或将文件拖拽到此处
                    </p>
                    <p className="ant-upload-hint">
                      文件将上传到 OSS, 如未配置请先配置
                    </p>
                  </div>
                )}
              </Dragger>
              <Input
                placeholder=""
                readOnly={true}
                style={{ marginTop: 16, width: 480 }}
                value={item.cover}
              />
              <Button style={{ marginTop: 16 }} onClick={handleFile}>
                选择文件
              </Button>
              {item.cover ? (
                <Button
                  style={{
                    marginTop: 16,
                    marginLeft: 10,
                    border: '1px solid red',
                    color: 'red',
                  }}
                  onClick={handleDelete}
                >
                  移除
                </Button>
              ) : (
                ''
              )}
            </div>
          </Form.Item>
        </Form>
      </Drawer>
    </>
  );
};
export default observer(IDrawer);
