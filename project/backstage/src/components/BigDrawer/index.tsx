import React, { useEffect, useState } from 'react';
import {
  Drawer,
  Button,
  Form,
  Row,
  Col,
  Input,
  Upload,
  message,
  Card,
  Spin,
  Empty,
  ConfigProvider,
} from 'antd';
import { observer } from 'mobx-react-lite';
import useStore from '@/context/useStore';
import zhCN from 'antd/es/locale/zh_CN';
import styles from './index.less';
import Pagination from '../Pagination';
import ImageView from '../ImageView';

const BigDrawer: React.FC = () => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(true);
  const [page, setpage] = useState(1);
  const [local, setLocal] = useState(zhCN);
  const [limit, setlimit] = useState(12);
  const [params, setParams] = useState({});
  const [obj, setObj] = useState({});
  const [num, setNum] = useState(1);
  const [pageNum, setPageNum] = useState(['8', '12', '24', '36']);
  const store = useStore();
  useEffect(() => {}, [store.knowLedge.flag,store.knowLedge.cover]);
  const onClose = () => {
    store.knowLedge.flag = false;
    store.knowLedge.visible = true;
  };
  useEffect(() => {
    store.knowLedge.getKnowledge(page, limit, params, num, obj);
    return () => {};
  }, [page, limit, pageNum, obj]);



  const getFields = () => {
    const children = [];
    children.push(
      <Col span={8} key={1} style={{ display: 'flex' }}>
        <Form.Item
          colon={false}
          name="originalname"
          style={{ width: 180 }}
          label={`名称`}
        >
          <Input placeholder="请输入文件名称" />
        </Form.Item>
        <Form.Item
          colon={false}
          name="type"
          style={{ width: 180 }}
          label={`状态`}
        >
          <Input placeholder="请输入文件类型" />
        </Form.Item>
      </Col>,
    );
    return children;
  };
  const onFinish = (values: any) => {
    let obj = {};
    for (let key in values) {
      values[key] && (obj[key] = values[key]);
    }
    setObj(obj);
  };
  const props = {
    name: 'file',
    action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
    headers: {
      authorization: 'authorization-text',
    },
    onChange(info: any) {
      if (info.file.status !== 'uploading') {
        console.log(info.file, info.fileList);
      }
      if (info.file.status === 'done') {
        message.success(`${info.file.name} file uploaded successfully`);
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
  };

  function onChange(page: number, pageSize: number | undefined) {
    setpage(page);
    setlimit(pageSize!);
    store.knowLedge.getKnowledge(page, limit, params, num);
  }

  return (
    <>
      <Drawer
        title="文件选择"
        width={786}
        placement="right"
        onClose={onClose}
        visible={store.knowLedge.flag}
      >
        <Form
          style={{ padding: '24px 12px' }}
          form={form}
          name="advanced_search"
          className="ant-advanced-search-form"
          onFinish={onFinish}
        >
          <Row gutter={24}>{getFields()}</Row>
          <Row>
            <Col span={24} style={{ textAlign: 'right' }}>
              <Button type="primary" htmlType="submit">
                搜索
              </Button>
              <Button
                style={{ marginLeft: '8px' }}
                onClick={() => {
                  form.resetFields();
                }}
              >
                重置
              </Button>
            </Col>
          </Row>
        </Form>
        <div style={{ background: 'rgb(255, 255, 255)', padding: '24px 12px' }}>
          <div className={styles.upLoad}>
            <Upload {...props}>
              <Button>上传文件</Button>
            </Upload>
          </div>

          <Spin size="large" spinning={loading}>
            <div className={styles.image}>
              {store.knowLedge.imgList.length > 0 ? (
                store.knowLedge.imgList.map((item) => {
                  return (
                    <div key={item.id}>
                      <Card
                      onLoadCapture={()=>{setLoading(false)}}
                        hoverable
                        style={{
                          width: 160,
                          marginBottom: '10px',
                          height: 200,
                        }}
                        cover={
                          <ImageView>
                            {' '}
                            <img alt="example" src={item.url} />{' '}
                          </ImageView>
                        }
                      >
                        <p className={styles.content}  onClick={()=>{
                          store.knowLedge.cover=item.url;
                          store.knowLedge.flag = false;
                          store.knowLedge.visible = true;  
                        }}>
                          {item.originalname}
                        </p>
                      </Card>
                    </div>
                  );
                })
              ) : (
                <ConfigProvider locale={local}>
                  {' '}
                  <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />{' '}
                </ConfigProvider>
              )}
            </div>
          </Spin>
         <div className={styles.bottom}>
         <Pagination
            limit={limit}
            page={page}
            pageNum={pageNum}
            onChange={onChange}
            totals={store.knowLedge.allNum}
          />
         </div>
         
        </div>
      </Drawer>
    </>
  );
};

export default observer(BigDrawer);
