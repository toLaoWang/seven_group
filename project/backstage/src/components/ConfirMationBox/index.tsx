import { Popconfirm, message } from 'antd';
import React, { FC } from 'react';
import useStore from '@/context/useStore';
const ConfirMationBox: FC<{ title: string, id?: string, selectedRow?: React.Key[] }> = (props) => {
  const [visible, setVisible] = React.useState(false);
  const store = useStore()
  let { title, id, selectedRow } = props
  const showPopconfirm = (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
    e.defaultPrevented
    setVisible(true);
  };
  const handleOk = async () => {
    setVisible(false);
    if (id) {
      if (!(localStorage.user.rule === "visitor")) {
        let result = await store.mail.deleteEmail(id!)
        result.statusCode === 200 ? message.success("成功") : message.error("失败")
      } else {
        message.warning("访客无权进行该操作")
      }
      return
    }
    if (selectedRow!.length) {
        if (!(localStorage.user.rule === "visitor")) {
              store.mail.deleteEmails(selectedRow!)

        } else {
          message.warning("访客无权进行该操作")
        }
    }
  };
  const handleCancel = () => {
    setVisible(false);
  };

  return (
    <>
      <Popconfirm
        title={title}
        visible={visible}
        onConfirm={handleOk}
        onCancel={handleCancel}
        okText="确认"
        cancelText="取消"
      >
        {
          React.Children.map(props.children, (item: any) => {
            return React.cloneElement(item, { onClick: showPopconfirm })
          })
        }
      </Popconfirm>
    </>
  );
};
export default ConfirMationBox