import { Popconfirm, message } from 'antd';
import React, { FC } from 'react';
import useStore from '@/context/useStore';
const ConfirMationBox: FC<{ title: string, id?: string, selectedRow?: React.Key[], page: number, pageSize: number }> = (props) => {
    const [visible, setVisible] = React.useState(false);
    const store = useStore()
    let { title, id, selectedRow, page, pageSize } = props
    const showPopconfirm = (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
        e.defaultPrevented
        setVisible(true);
    };
    const handleOk = async () => {
        setVisible(false);
        if (id) {
            if (localStorage.user.rule !== "visitor") {
                let result = await store.article.getDelArticle(id!)
                result.statusCode === 200 ? message.success("成功") : message.error("失败")
                store.article.getArticleList({ page, pageSize })
            } else {
                message.warning("访客无权进行该操作")
            }
            return
        }
        if (selectedRow!.length) {
            console.log(selectedRow);

            selectedRow!.forEach(async item => {
                if (localStorage.user.rule !== "visitor") {
                    let result = await store.article.getDelArticle(item! as string)
                    result.statusCode === 200 ? message.success("成功") : message.error("失败")
                    store.article.getArticleList({ page, pageSize })
                } else {
                    message.warning("访客无权进行该操作")
                }
            })
        }
    };
    const handleCancel = () => {
        setVisible(false);
    };

    return (
        <>
            <Popconfirm
                title={title}
                visible={visible}
                onConfirm={handleOk}
                onCancel={handleCancel}
                okText="确认"
                cancelText="取消"
            >
                {
                    React.Children.map(props.children, (item: any) => {
                        return React.cloneElement(item, { onClick: showPopconfirm })
                    })
                }
            </Popconfirm>
        </>
    );
};
export default ConfirMationBox