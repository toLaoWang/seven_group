import React, { useEffect } from 'react'
import styles from './index.less'
import Form from '@/components/Form'
export default function index() {
    useEffect(() => {
        localStorage.clear()
        return () => {
        }
    }, [])
    return (
        <div id={styles.login}>
            <Form flag={true}></Form>
        </div>
    )
}
