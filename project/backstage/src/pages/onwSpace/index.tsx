import React, { useEffect, useState } from 'react';
import { Card, Col, Row, Form, Input, Button, Avatar, List, Typography } from 'antd';
import UserOutlined from '@ant-design/icons/lib/icons/UserOutlined';

const data = [
    'Racing car sprays burning fuel into crowd.',
    'Japanese princess to wed commoner.',
    'Australian walks 100km after outback crash.',
    'Man charged over missing wedding girl.',
    'Los Angeles battles huge wildfires.',
];
const OwnSpace = () => {

    const [key, setKey] = useState("tab1")

    useEffect(() => {

    }, []) 

    const tabList = [
        {
            key: 'tab1',
            tab: '基本设置',
        },
        {
            key: 'tab2',
            tab: '更新密码',
        },
    ];

    const onFinish = (values: any) => {
        console.log('Success:', values);
    };



    const contentList = {
        tab1: <Form
            name="basic"
            wrapperCol={{ span: 20 }}
            labelCol={{ span: 3 }}
            initialValues={{ remember: true }}
            onFinish={onFinish}
            autoComplete="off"
        >
            <Form.Item style={{ display: "flex", justifyContent: "center" }}>
                <div style={{ margin: "0 auto", display: "flex", justifyContent: "center" }}><Avatar size={64} /></div>
            </Form.Item>
            <Form.Item
                label="用户名"
                name="username"
            >
                <Input />
            </Form.Item>

            <Form.Item
                label="邮箱"
                name="mailbox"
            >
                <Input />
            </Form.Item>

            <Form.Item>
                <Button type="primary" htmlType="submit">
                    保存
                </Button>
            </Form.Item>
        </Form>,
        tab2: <Form
            name="basic"
            labelCol={{ span: 4 }}
            wrapperCol={{ span: 20 }}
            initialValues={{ remember: true }}
            onFinish={onFinish}
            autoComplete="off"
        >
            <Form.Item
                label="原密码"
                name="rowpassword"
            >
                <Input.Password />
            </Form.Item>
            <Form.Item
                label="新密码"
                name="newpassword"
            >
                <Input.Password />
            </Form.Item>
            <Form.Item
                label="确认密码"
                name="affirmpassword"
            >
                <Input.Password />
            </Form.Item>
            <Form.Item>
                <Button type="primary" htmlType="submit">
                    更新
                </Button>
            </Form.Item>
        </Form>,
    };


    const onTabChange = (key: string, type: string) => {
        setKey(key)
    };
    return (
        <div className="ownSpace">
            <Row gutter={24}>
                <Col span={12}>
                    <List
                        style={{ background: "#fff", border: "none" }}
                        header={<div>系统概览</div>}
                        dataSource={data}
                        bordered
                        renderItem={item => (
                            <List.Item>
                                {item}
                            </List.Item>
                        )}
                    />
                </Col>
                <Col span={12}>
                    <Card
                        style={{ width: '100%' }}
                        title="个人资料"
                        tabList={tabList}
                        activeTabKey={key}
                        onTabChange={key => {
                            onTabChange(key, 'key');
                        }}
                    >
                        {contentList[key]}
                    </Card>
                </Col>

            </Row>
        </div>
    )
}
export default OwnSpace;