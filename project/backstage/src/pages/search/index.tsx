import { useEffect, useState, Key } from 'react';
import { Form, Row, Col, Input, Button, Table, Tooltip, Popconfirm, Space, Badge, ConfigProvider } from 'antd';
import "./index.less";
import { ISearchList, ISearchParams } from "@/types/index";
import { observer } from 'mobx-react-lite';
import useStore from '@/context/useStore';
import zhCN from 'antd/es/locale/zh_CN';
import { fomatTime } from '@/utils/fomatTime';


const Search = () => {
    const [selectedRowKeys, setSelectedRowKeys] = useState<Key[]>([]);
    const [form] = Form.useForm();
    const [page, setPage] = useState(1);
    const [pageSize, setPageSize] = useState(12);
    const [params, setParams] = useState({});
    const store = useStore();

    useEffect(() => {
        store.search.getSearchList(page, pageSize, params);
    }, [page, pageSize, params])

    const children = [
        <Col span={6} key={1}>
            <Form.Item
                name='type'
                label='类型'
            >
                <Input placeholder="请输入类型" />
            </Form.Item>
        </Col>,
        <Col span={6} key={2}>
            <Form.Item
                name='keyword'
                label='搜索词'
            >
                <Input placeholder="请输入搜索词" />
            </Form.Item>
        </Col>,
        <Col span={6} key={3}>
            <Form.Item
                name='count'
                label='搜索量'
            >
                <Input placeholder="请输入搜索量" />
            </Form.Item>
        </Col>,
    ]

    const columns = [
        { title: '搜索词', dataIndex: 'keyword', key: 'keyword' },
        {
            title: '搜索量',
            dataIndex: 'count',
            key: 'count',
            render: (text: ISearchList) => {
                return <Space>
                    <Badge
                        className="site-badge-count-109"
                        showZero={true}
                        count={text}
                        style={{ backgroundColor: '#52c41a' }}
                    />
                </Space>
            }
        },
        {
            title: '搜索时间', dataIndex: 'createAt', key: 'createAt', render: (time: string) => {
                return fomatTime(time)
            }
        },
        {
            title: '操作',
            dataIndex: '',
            key: 'x',
            render: (text: ISearchList) => <Popconfirm placement="top" title="确认删除这个搜索记录？"
                onConfirm={() => {
                    //删除
                    store.search.delSearchList([text.id] as string[], page, pageSize, params);
                    setSelectedRowKeys([]);
                }} okText="确定" cancelText="取消">
                <a>删除</a>
            </Popconfirm>
        },
    ];

    //搜索
    function onFinish(values: ISearchParams) {
        setParams(values);
        setPage(1);
    };


    //监听多选框选中状态
    function onSelectChange(selectedRowKeys: Key[]) {
        setSelectedRowKeys(selectedRowKeys)
    };

    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange,
    };



    //分页切换
    function changePage(pagination: any) {
        setPage(pagination.current);
        setPageSize(pagination.pageSize);
    }

    return (
        <>
            <Form
                form={form}
                name="advanced_search"
                className="ant-advanced-search-form"
                onFinish={onFinish}
            >
                <Row gutter={20}>{children}</Row>
                <Row>
                    <Col
                        span={24}
                        style={{
                            textAlign: 'right',
                        }}
                    >
                        <Button type="primary" htmlType="submit">
                            搜索
                        </Button>
                        <Button
                            style={{
                                margin: '0 8px',
                            }}
                            onClick={() => {
                                form.resetFields();
                                setParams({});
                            }}
                        >
                            重置
                        </Button>
                    </Col>
                </Row>
            </Form>


            <div className="table">
                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                    {selectedRowKeys.length ?
                        <Popconfirm placement="top" title="确认删除？"
                            onConfirm={() => {
                                //删除
                                store.search.delSearchList(selectedRowKeys as string[], page, pageSize, params);
                                setSelectedRowKeys([]);
                            }} okText="确定" cancelText="取消">
                            <Button danger>删除</Button>
                        </Popconfirm> : <div></div>}
                    <Tooltip placement="top" title="刷新">
                        <svg onClick={() => {
                            store.search.getSearchList(page, pageSize);
                            setSelectedRowKeys([]);
                        }} style={{ cursor: 'pointer' }} viewBox="64 64 896 896" focusable="false" data-icon="reload" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M909.1 209.3l-56.4 44.1C775.8 155.1 656.2 92 521.9 92 290 92 102.3 279.5 102 511.5 101.7 743.7 289.8 932 521.9 932c181.3 0 335.8-115 394.6-276.1 1.5-4.2-.7-8.9-4.9-10.3l-56.7-19.5a8 8 0 00-10.1 4.8c-1.8 5-3.8 10-5.9 14.9-17.3 41-42.1 77.8-73.7 109.4A344.77 344.77 0 01655.9 829c-42.3 17.9-87.4 27-133.8 27-46.5 0-91.5-9.1-133.8-27A341.5 341.5 0 01279 755.2a342.16 342.16 0 01-73.7-109.4c-17.9-42.4-27-87.4-27-133.9s9.1-91.5 27-133.9c17.3-41 42.1-77.8 73.7-109.4 31.6-31.6 68.4-56.4 109.3-73.8 42.3-17.9 87.4-27 133.8-27 46.5 0 91.5 9.1 133.8 27a341.5 341.5 0 01109.3 73.8c9.9 9.9 19.2 20.4 27.8 31.4l-60.2 47a8 8 0 003 14.1l175.6 43c5 1.2 9.9-2.6 9.9-7.7l.8-180.9c-.1-6.6-7.8-10.3-13-6.2z"></path></svg>
                    </Tooltip>
                </div>
                <ConfigProvider locale={zhCN}>
                    <Table
                        columns={columns}
                        rowSelection={rowSelection}
                        onChange={changePage}
                        pagination={{
                            pageSizeOptions: ["8", "12", "24", "36"],
                            showSizeChanger: true,
                            current: page,
                            pageSize: pageSize,
                            total: store.search.total,
                            showTotal: () => `共${store.search.total}条`
                        }}
                        dataSource={store.search.searchList.map(item => {
                            item.key = item.id;
                            return item
                        })
                        }
                    />
                </ConfigProvider>
            </div>
        </>
    );
};

export default observer(Search);