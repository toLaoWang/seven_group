import React, { useEffect, useState } from 'react'
import styles from './index.less'
import useStore from '@/context/useStore'
import { Form, Row, Col, Input, Button, Table, Pagination, } from 'antd';
import { observer } from 'mobx-react-lite';
import { RedoOutlined } from '@ant-design/icons'
import { mailObject } from '@/types/module/mail';
import ConfirMationBox from '@/components/ConfirMationBox'
const columns = [
    {
        title: '发件人',
        dataIndex: 'from',
    },
    {
        title: '收件人',
        dataIndex: 'to',
    },
    {
        title: '主题',
        dataIndex: 'subject',
    },
    {
        title: '发送时间',
        dataIndex: 'createAt',
    },
    {
        title: '操作',
        render: (item: mailObject) => {
            return <ConfirMationBox title="确认删除这个邮件？" id={item.id}><a>删除</a></ConfirMationBox>
        }
    },
];
const Mail: React.FC = () => {
    const store = useStore()
    const [form] = Form.useForm();
    let [page, setPage] = useState(1)
    let [pageSize, setPageSize] = useState(12)
    let { emailLisr, emailTotal, isSucceed } = store.mail
    let [show, setShow] = useState(false)
    let [rotate, setRotate] = useState(false)
    let [selectedRow, setSelectedRow] = useState<React.Key[]>([])
    useEffect(() => {
        store.mail.getEmailList({ page, pageSize })
    }, [page, pageSize])
    useEffect(() => {
        if (isSucceed) {
            let values = form.getFieldsValue()
            store.mail.getEmailList({ page, pageSize, from: values.addresser, to: values.recipients, subject: values.theme })
            store.mail.changeIsSucceed()
        }
    }, [isSucceed])
    const onFinish = (values: any) => {
        store.mail.getEmailList({ page, pageSize, from: values.addresser, to: values.recipients, subject: values.theme })
    };
    const ChangePage = (page: number, pageSize?: number) => {
        setPage(page)
        setPageSize(Number(pageSize))
    }
    const rowSelection = {
        onChange: (selectedRowKeys: React.Key[]) => {
            selectedRowKeys.length ? setShow(true) : setShow(false)
            setSelectedRow(selectedRowKeys)
        },
    };

    return (
        <div id={styles.mail}>
            <Form
                form={form}
                name="advanced_search"
                className="ant-advanced-search-form"
                onFinish={onFinish}
            >
                <Row gutter={24}>
                    <Col span={6}>
                        <Form.Item
                            name="addresser"
                            label='发件人'
                        >
                            <Input placeholder="请输入发件人" />
                        </Form.Item>
                    </Col>
                    <Col span={6}>
                        <Form.Item
                            name="recipients"
                            labelCol={{ span: 6 ,offset: 2}}
                            label='收件人'
                        >
                            <Input placeholder="请输入收件人" />
                        </Form.Item>
                    </Col>
                    <Col span={6}>
                        <Form.Item
                            name="theme"
                            label='主题'
                            labelCol={{ span: 6 ,offset: 2}}
                        >
                            <Input placeholder="请输入主题" />
                        </Form.Item>
                    </Col>
                </Row>
                <Row>
                    <Col span={24} style={{ textAlign: 'right' }}>
                        <Button type="primary" htmlType="submit">
                            搜索
                        </Button>
                        <Button
                            style={{ margin: '0 8px' }}
                            onClick={() => {
                                form.resetFields();
                            }}
                        >
                            重置
                        </Button>
                    </Col>
                </Row>
            </Form>
            <div className={styles.searchResultlist}>
                <div className={styles.operation}>
                    <ConfirMationBox title="确认删除？" selectedRow={selectedRow}><Button style={{ opacity: show ? 1 : 0 }} danger>删除</Button></ConfirMationBox>
                    <span onClick={async () => {
                        setRotate(true)
                        await store.mail.getEmailList({ page, pageSize })
                        setRotate(false)
                    }}><RedoOutlined spin={rotate} /></span>
                </div>
                <Table columns={columns} dataSource={emailLisr} rowKey="id" scroll={{ x: 1400 }} pagination={false} rowSelection={{ ...rowSelection }} />
                <div className={styles.pagination}>
                    <Pagination
                        total={emailTotal}
                        pageSizeOptions={["8", "12", "24", "32"]}
                        showSizeChanger
                        pageSize={pageSize}
                        current={page}
                        showTotal={(total: number) => `共 ${total} 条`}
                        onChange={ChangePage}
                    />
                </div>
            </div>
        </div >
    )
}
export default observer(Mail)
