import React, { useEffect, useState } from 'react'
import styles from './index.less'
import { Row, Col, Form, Button, Input, Tabs, Card, Drawer, Pagination, Modal } from 'antd'
import useStore from '@/context/useStore';
import { observer } from 'mobx-react-lite';
import { language, SettingObject } from '@/types/module/setting';
import { FileImageOutlined } from '@ant-design/icons'
import ImageView from '@/components/ImageViews';
import MonacoEditor, { monaco } from 'react-monaco-editor'
const { Meta } = Card;
const { TextArea } = Input;
const { TabPane } = Tabs;



const Setting: React.FC = (props) => {
    const store = useStore()
    const { settings, testEmail } = store.setting
    const [page, setPage] = useState(1)
    const [pageSize, setPageSize] = useState(12)
    const { fileList, total } = store.file
    const [visible, setVisible] = useState(false);
    const [form] = Form.useForm()
    const [isFlag, setIsFlag] = useState(true)
    useEffect(() => {
        store.file.getFileList({ page, pageSize })
    }, [page, pageSize])
    useEffect(() => {
        store.setting.getSetting()
    }, [])
    //修改配置项
    const changeObj = (e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>, type: keyof SettingObject) => {
        store.setting.changeSettings(e.target.value, type)
    }
    //判断Logo和Favicon那个被点击
    const showDrawer = (type: string) => {
        if (type === "systemFavicon") {
            setIsFlag(true)
        } else {
            setIsFlag(false)
        }
        setVisible(true);
    };
    //关闭
    const onClose = () => {
        setVisible(false);
    };
    //请求数据
    const onFinish = (values: any) => {
        store.file.getFileList({ page, pageSize, originalname: values.fileName, type: values.fileType })
    };

    //添加或删除tab
    const onEdit = (targetKey: string | React.MouseEvent<Element, MouseEvent> | React.KeyboardEvent<Element>, action: string) => {
        console.log("add 添加", "remove 删除");
        console.log(targetKey);
        if (action === "add") {
            showModal()
        } else {
            delete settings.i18n![Object.keys(settings.i18n!)[parseInt(targetKey as string)]]
        }

    }
    //react-monaco-editor事件
    // const editorDidMountHandle = (editor:monaco.editor.IStandaloneCodeEditor, monacos: typeof monaco) => {
    //     editor.focus();
    //     monacos.editor.setTheme("hc-black")
    // monaco.languages.typescript.javascriptDefaults.setDiagnosticsOptions({
    //     noSemanticValidation: true,
    //     noSyntaxValidation: false
    //   })

    // compiler options
    //   monaco.languages.typescript.javascriptDefaults.setCompilerOptions({
    //     target: monaco.languages.typescript.ScriptTarget.JSON,
    //     allowNonTsExtensions: true
    //   })
    console.log(monaco.languages);

    // }

    //弹框
    const [visibles, setVisibles] = React.useState(false);
    const [modalText, setModalText] = React.useState('');
    const showModal = () => {
        setVisibles(true);
    };
    //确认
    const handleOk = () => {
        setVisibles(false);
        settings.i18n![modalText] = {}
        setModalText('')
    };
    //取消
    const handleCancel = () => {
        setModalText('')
        setVisibles(false);
    };
    return (
        <div className={styles.setting}>                    
            <Tabs tabPosition={"left"}>
                <TabPane tab="系统设置" key="1">
                    <p className={styles.minTitle}>系统地址</p>
                    <Input
                        placeholder="Basic usage"
                        value={settings.systemUrl}
                        onChange={e => changeObj(e, "systemUrl")} />
                    <p className={styles.minTitle}>后台地址</p>
                    <Input
                        placeholder="Basic usage"
                        value={settings.adminSystemUrl}
                        onChange={e => changeObj(e, "adminSystemUrl")} />
                    <p className={styles.minTitle}>系统标题</p>
                    <Input
                        placeholder="Basic usage"
                        value={settings.systemTitle}
                        onChange={e => changeObj(e, "systemTitle")} />
                    <p className={styles.minTitle}>Logo</p>
                    <Input addonAfter={<FileImageOutlined onClick={() => showDrawer("systemLogo")} />} value={settings.systemLogo} />
                    <p className={styles.minTitle}>Favicon</p>
                    <Input addonAfter={<FileImageOutlined onClick={() => showDrawer("systemFavicon")} />} value={settings.systemFavicon} />
                    <p className={styles.minTitle}>页脚信息</p>
                    <TextArea
                        value={settings.systemFooterInfo}
                        onChange={e => changeObj(e, "systemFooterInfo")}
                        autoSize={{ minRows: 3, maxRows: 5 }}
                    />
                    <p className={styles.btnBox}>
                        <Button onClick={() => store.setting.commitModification({
                            adminSystemUrl: settings.adminSystemUrl!,
                            systemFavicon: settings.systemFavicon!,
                            systemFooterInfo: settings.systemFooterInfo!,
                            systemLogo: settings.systemLogo!,
                            systemTitle: settings.systemTitle!,
                            systemUrl: settings.systemUrl!
                        })} type="primary">保存</Button>
                    </p>
                </TabPane>
                <TabPane tab="国际化设置" key="2">
                    <Tabs
                        type="editable-card"
                        onEdit={onEdit}
                    >
                        {
                            settings.i18n && (Object.keys(settings.i18n!)).map((item, index) => {
                                return <TabPane tab={item} key={index} closable={true}>
                                    <MonacoEditor
                                        onChange={(e) => {
                                            console.log(e);
                                        }}
                                        height="460"
                                        // editorDidMount={editorDidMountHandle}
                                        value={JSON.stringify(settings.i18n![item], null, 2)}
                                    />

                                </TabPane>
                            })
                        }
                    </Tabs>
                    <p className={styles.btnBox}>
                        <Button onClick={() => console.log(settings.i18n)
                        } type="primary">保存</Button>
                    </p>
                </TabPane>
                <TabPane tab="SEO设置" key="3">
                    <p className={styles.minTitle}>关键词</p>
                    <Input
                        placeholder="Basic usage"
                        value={settings.seoKeyword}
                        onChange={e => changeObj(e, "seoKeyword")} />
                    <p className={styles.minTitle}>描述信息</p>
                    <TextArea
                        value={settings.seoDesc}
                        onChange={e => changeObj(e, "seoDesc")}
                        autoSize={{ minRows: 3, maxRows: 5 }}
                    />
                    <p className={styles.btnBox}>
                        <Button onClick={() => store.setting.commitModification({
                            seoDesc: settings.seoDesc!,
                            seoKeyword: settings.seoKeyword!
                        })} type="primary">保存</Button>
                    </p>
                </TabPane>
                <TabPane tab="数据统计" key="4">
                    <p className={styles.minTitle}>百度统计</p>
                    <Input
                        placeholder="请输入谷歌分析ID"
                        value={settings.baiduAnalyticsId}
                        onChange={e => changeObj(e, "baiduAnalyticsId")} />
                    <p className={styles.minTitle}>谷歌分析</p>
                    <Input
                        placeholder="请输入谷歌分析ID"
                        value={settings.googleAnalyticsId}
                        onChange={e => changeObj(e, "googleAnalyticsId")} />
                    <p className={styles.btnBox}>
                        <Button onClick={() => store.setting.commitModification({
                            baiduAnalyticsId: settings.baiduAnalyticsId!,
                            googleAnalyticsId: settings.googleAnalyticsId!
                        })} type="primary">保存</Button>
                    </p>
                </TabPane>
                <TabPane tab="OSS设置" key="5">
                    <MonacoEditor
                        height="500"
                        value={"wo shi ni die "}
                        language={'json'}
                    />
                </TabPane>
                <TabPane tab="SMTP服务" key="6">
                    <p className={styles.minTitle}>SMTP地址</p>
                    <Input
                        placeholder="请输入SMTP"
                        value={settings.smtpHost}
                        onChange={e => changeObj(e, "smtpHost")} />
                    <p className={styles.minTitle}>SMTP端口(强制使用SSL连接)</p>
                    <Input
                        placeholder="请输入SMTP端口"
                        value={settings.smtpPort}
                        onChange={e => changeObj(e, "smtpPort")} />
                    <p className={styles.minTitle}>SMTP用户</p>
                    <Input
                        placeholder="请输入SMTP用户"
                        value={settings.smtpUser}
                        onChange={e => changeObj(e, "smtpUser")} />
                    <p className={styles.minTitle}>SMTP密码</p>
                    <Input
                        placeholder="请输入SMTP密码"
                        value={settings.smtpPass}
                        onChange={e => changeObj(e, "smtpPass")} />
                    <p className={styles.minTitle}>发件人</p>
                    <Input
                        placeholder="请输入发件人"
                        value={settings.smtpFromUser}
                        onChange={e => changeObj(e, "smtpFromUser")} />
                    <p className={styles.btnBox}>
                        <Button onClick={() => store.setting.commitModification({
                            smtpFromUser: settings.smtpFromUser!,
                            smtpHost: settings.smtpHost!,
                            smtpPass: settings.smtpPass!,
                            smtpPort: settings.smtpPort!,
                            smtpUser: settings.smtpUser!,
                        })} style={{ marginRight: "16px" }} type="primary">保存</Button>
                        <Button onClick={() => testEmail()}>测试</Button></p>
                </TabPane>
            </Tabs>
            <Drawer width={786} footer={
                <div>123</div>
            } title="文件选择" placement="right" onClose={onClose} visible={visible}>
                <Form
                    form={form}
                    name="advanced_search"
                    className="ant-advanced-search-form"
                    onFinish={onFinish}
                >
                    <Row gutter={24}>
                        <Col span={9}>
                            <Form.Item
                                name={`fileName`}
                                label={`文件名称`}
                                labelCol={{ span: 7 }}
                            >
                                <Input placeholder="请输入文件名称" />
                            </Form.Item>
                        </Col>
                        <Col span={9} >
                            <Form.Item
                                name={`fileType`}
                                label={`文件类型`}
                                labelCol={{ span: 7 }}
                            >
                                <Input placeholder="请输入文件类型" />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={24} style={{ textAlign: 'right' }}>
                            <Button type="primary" htmlType="submit">
                                搜索
                            </Button>
                            <Button
                                style={{ margin: '0 8px' }}
                                onClick={() => {
                                    form.resetFields();
                                }}
                            >
                                重置
                            </Button>
                        </Col>
                    </Row>
                </Form>
                <Row gutter={[16, 16]}>
                    {
                        fileList.map(item => {
                            return <Col span={6} key={item.id}>
                                <Card
                                    bodyStyle={{ padding: "16px", height: "auto" }}
                                    hoverable
                                    style={{ width: "100%", height: "170px" }}
                                    cover={<ImageView isShowWidth={true}><img alt="example" src={item.url} /></ImageView>}
                                >
                                    <span onClick={() => {
                                        setVisible(false)
                                        store.setting.changeSettings(item.url, isFlag ? "systemFavicon" : "systemLogo")
                                    }}>
                                        <Meta className={styles.mata} title={item.originalname} />
                                    </span>
                                </Card>
                            </Col>
                        })
                    }
                </Row>
                <div className={styles.pagination}>

                    <Pagination onChange={(current) => {
                        setPage(current)
                    }}
                        current={page}
                        onShowSizeChange={(page, size) => {
                            setPageSize(size)
                        }}
                        pageSize={pageSize}
                        pageSizeOptions={["8", "12", "24", "36"]}
                        defaultCurrent={1}
                        total={total}
                        showSizeChanger={true} />
                </div>
            </Drawer>
            <Modal
                title="请输入语言名称（英文）"
                visible={visibles}
                onOk={handleOk}
                onCancel={handleCancel}
            >
                <Input type="text" value={modalText} onChange={(e) => { setModalText(e.target.value) }} />
            </Modal>
        </div>
    )
}
export default observer(Setting)
