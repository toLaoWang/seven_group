import React from 'react'
import { Card, Input, Button, Row, Col, Form, Popconfirm, message } from 'antd'
import style from './index.less'
import { useEffect } from 'react'
import useStore from '@/context/useStore';
import { useState } from 'react';
import { getAddTag, getDelTag, getEditTag } from '@/services';
import { observer } from 'mobx-react-lite';
const Tag: React.FC = (props) => {
    const store = useStore()
    const [name, setName] = useState('');
    const [minname, setminName] = useState('');
    const [show, setShow] = useState(true);
    const [id, setid] = useState('');
    useEffect(() => {
        store.article.getTagList()
    }, [])
    const [visible, setVisible] = React.useState(false);
    // 点击事件
    const showPopconfirm = () => {
        setVisible(true);
    };
    // 确定按钮
    const handleOk = () => {
        setVisible(false);
        getDel(id)
    };
    // 取消按钮
    const handleCancel = () => {
        setVisible(false);
    };
    // 添加
    async function getAdd(name: string, minname: string) {
        if (name && minname) {
            let result = await getAddTag(name, minname);
            if (result.statusCode === 201) {
                message.success('添加标签成功');
                store.article.getTagList()
                setName('')
                setminName('')
            }
        } else {
            message.error('未知错误');
        }
    }
    // 删除
    async function getDel(id: string) {
        let result = await getDelTag(id);
        if (result.statusCode === 200) {
            message.success('删除标签成功');
            store.article.getTagList();
            setShow(true);
            setName('')
            setminName('')
            setid('')
        }
    }
    // 更新编辑
    async function getEdit(id: string, name: string, minname: string) {
        if (name) {
            let result = await getEditTag(id, name, minname);
            if (result.statusCode === 200) {
                message.success('更新标签成功');
                store.article.getTagList();
                setShow(true);
                setName('')
                setminName('')
                setid('')
            }
        }
    }
    return (
        <div className={style.category}>
            <Row>
                <Col className={style.topCard}>
                    <Card title={`${show ? '添加' : '管理'}标签`} bordered={false} style={{ height: '270px' }}>
                        <Row>
                            <Col span={24}>
                                <Form.Item>
                                    <Input placeholder='输入标签名称' value={name} onChange={(e) => setName(e.target.value)} />
                                </Form.Item>

                            </Col>
                        </Row>
                        <Row>
                            <Col span={24}>
                                <Form.Item>
                                    <Input placeholder='输入标签值(请输入英文，作为路由使用)' width='100%' value={minname} onChange={(e) => setminName(e.target.value)} />
                                </Form.Item>

                            </Col>
                        </Row>
                        <Row>
                            {show ?
                                <Col span={24}>
                                    <Button type="primary" onClick={() => getAdd(name, minname)}>保存</Button>
                                </Col> :
                                <Col span={24} style={{ display: 'flex', justifyContent: 'space-between' }}>
                                    <span><Button type="primary" onClick={() => getEdit(id, name, minname)}>更新</Button>
                                        <Button type="dashed" onClick={() => {
                                            setShow(true);
                                            setName('')
                                            setminName('')
                                            setid('')
                                        }}>返回添加</Button></span>
                                    <Popconfirm
                                        title="确定删除这个标签？"
                                        visible={visible}
                                        okText="确定"
                                        cancelText="取消"
                                        onConfirm={handleOk}
                                        onCancel={handleCancel}
                                    >
                                        <Button danger onClick={showPopconfirm}>删除</Button>
                                    </Popconfirm>


                                </Col>}
                        </Row>

                    </Card>

                </Col>
                <Col className={style.bomCard} >
                    <Card title="所有标签" bordered={false}>
                        <ul>
                            {
                                store.article.tagList.map(item => {
                                    return <li onClick={() => {
                                        setShow(false);
                                        setName(item.label)
                                        setminName(item.value)
                                        setid(item.id)
                                    }} key={item.id}>{item.label}</li>
                                })
                            }
                        </ul>
                    </Card>
                </Col>
            </Row>
        </div >
    )
}
export default observer(Tag)

