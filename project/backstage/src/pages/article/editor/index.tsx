import React, { useEffect, useState } from 'react'
import ForEditor from 'for-editor'
import { makeHtml, makeToc } from '@/utils/markdown'
import { Button, Input, Menu, Dropdown, message, Drawer, Form, Select, Switch, Popconfirm } from 'antd'
import { CloseOutlined, EllipsisOutlined } from '@ant-design/icons'
import style from './editor.less'
import useStore from '@/context/useStore'
import { ICategoryItem, ITagItem } from '@/types'
import { useHistory } from 'umi'
import { observer } from 'mobx-react-lite';

const { Option } = Select;
const Editor: React.FC = (props) => {
    const [title, setTitle] = useState('')
    const [content, setContent] = useState("");
    const [cover, setCover] = useState('');
    const [form] = Form.useForm()
    const store = useStore()
    const history = useHistory()
    const [isCommentable, setIsCommentable] = useState(false)
    const [isRecommended, setIsRecommended] = useState(false)

    const toolbar = {
        h1: true, // h1
        h2: true, // h2
        h3: true, // h3
        h4: true, // h4
        img: true, // 图片
        link: true, // 链接
        code: true, // 代码块
        preview: true, // 预览
        expand: true, // 全屏
        /* v0.0.9 */
        undo: true, // 撤销
        redo: true, // 重做
        save: true, // 保存
        /* v0.2.3 */
        subfield: true, // 单双栏模式
    }
    const [visible, setVisible] = useState(false)
    useEffect(() => {
        store.article.getTagList()
        store.article.getCategoryList()
    }, [])
    // 设置
    const showDrawer = () => {
        if (title) {
            setVisible(true)
        } else {
            message.warning('请输入文章标题')
        }
    };

    const onClose = () => {
        setVisible(false)
    };

    const menu = (
        <Menu>
            <Menu.Item key="11" disabled={history.location.pathname==='/article/editor'}>查看</Menu.Item>
            <Menu.Item key="21" onClick={showDrawer}>设置</Menu.Item>
            <Menu.Item key="31" onClick={()=>submit('draft')}>保存草稿</Menu.Item>
            <Menu.Item key="46" disabled={history.location.pathname==='/article/editor'}>删除</Menu.Item>
        </Menu>
    );
    
    // 点击发布按钮
    async function submit(status:string) {
        let values: { [key: string]: string | boolean | null } = form.getFieldsValue()

        if (!title) {
            message.warning('请输入文章标题');
            return
        }
        // md
        values.content = content;
        // html
        values.html = makeHtml(content)
        // toc
        values.toc = JSON.stringify(makeToc(makeHtml(content) as string))
        values.title = title;
        values.status = status;
        if (cover) {
            values.cover = cover;
        } else {
            values.cover = null;
        }

        if (!content) {
            message.warning('请输入文章内容')
        } else {
            let result = await store.article.getPublish(values)
            if (result) {
                if(status==='publish'){
                    message.warning('文章发布成功')
                }else{
                    message.warning('文章保存草稿成功')
                }
                history.push(`/article/editor/${result.data.id}`)
            } 
        }


    };
    function confirm() {
        history.push('/article')
    }


    return (
        <div>
            <header className={style.header}>
                <div>
                    <Popconfirm
                        placement="rightTop"
                        title={'确认关闭？如果有内容变更，请先保存。'}
                        onConfirm={confirm}
                        okText="确定"
                        cancelText="取消"
                    >
                        <Button><CloseOutlined /></Button>
                    </Popconfirm>

                    <input className={style.Input} placeholder='请输入页面名称' style={{ border: 'none', borderBottom: '1px solid #ccc', outline: 'none', width: '300px' }} onChange={e => setTitle(e.target.value)} />
                </div>
                <div>
                    <Button type='primary' onClick={() => submit('publish')}>发布</Button>
                    <Dropdown overlay={menu}>
                        <EllipsisOutlined style={{ marginLeft: '16px' }} />
                    </Dropdown>
                </div>

            </header>
            <section className={style.editor}>
                <ForEditor
                    height='100%'
                    value={content}
                    toolbar={toolbar}
                    onChange={value => setContent(value)}
                />
            </section>
            <Drawer
                title="文章设置"
                placement={'right'}
                closable={true}
                onClose={onClose}
                visible={visible}
                key='id'
                width={'480px'}
                footer={
                    [<Button type='primary' onClick={() => {
                        setVisible(false)
                        console.log('form..', form.getFieldsValue())
                    }} htmlType="submit">确认</Button>]
                }
                footerStyle={{ textAlign: 'right' }}
            >

                <Form
                    key='from'
                    form={form}
                >
                    <Form.Item name="summary" label="文章摘要" key='1'>
                        <Input.TextArea style={{ height: '142px' }}></Input.TextArea>
                    </Form.Item>
                    <Form.Item name="password" label="访问密码" key='2'>
                        <Input.Password placeholder="" />
                    </Form.Item>
                    <Form.Item name="totalAmount" label="付费查看" key='3'>
                        <Input.Password placeholder="" />
                    </Form.Item>
                    <Form.Item name="isCommentable" label="开启评论" key='4'>
                        <Switch checked={isCommentable} onChange={e => setIsCommentable(!isCommentable)} />
                    </Form.Item>
                    <Form.Item name="isRecommended" label="首页推荐" key='5'>
                        <Switch checked={isRecommended} onChange={e => setIsRecommended(!isRecommended)} />
                    </Form.Item>
                    <Form.Item name="category" label="选择分类" key='6'>
                        <Select key='100'>{
                            store.article.categoryList.map(item => {
                                return <Select.Option key={item.id} value={item.id}>{item.label}</Select.Option>
                            })
                        }</Select>
                    </Form.Item>
                    <Form.Item name="tags" label="选择标签" key='7'>
                        <Select
                            mode="tags"
                            size={'large'}
                            style={{ width: '100%' }}
                        >
                            {store.article.tagList.map(item => {
                                return <Option key={item.id} value={item.id}>{item.label}</Option>
                            })}
                        </Select>

                    </Form.Item>

                    <Form.Item name="cover" label="文章封面" key='8'>
                        <div>
                            <img src={cover} alt="" style={{
                                display: 'flex',
                                justifyContent: 'center',
                                alignItems: 'center',
                                height: '180px',
                                marginBottom: '16px',
                                color: '#888',
                                backgroundColor: '#f5f5f5',
                            }} />
                        </div>
                        <Input type="text" value={cover} placeholder="或输入外部链接" style={{ marginBottom: '16px', }} onChange={e => setCover(e.target.value)} />
                        <Button onClick={() => {
                            let values = form.getFieldsValue();
                            form.setFieldsValue({ ...values, cover: '' })
                            setCover('');
                        }}>移除</Button>
                    </Form.Item>

                </Form>
            </Drawer>


        </div >
    )
}
export default observer(Editor)
