import React, { useEffect, useRef, useState } from 'react'
import styles from './index.less'
import classNames from "classnames"
import io, { Socket } from 'socket.io-client'
import md5 from 'md5'
let time: NodeJS.Timeout
const TYPING_TIMER_LENGTH = 400;
const COLORS = [
    '#e21400',
    '#91580f',
    '#f8a700',
    '#f78b00',
    '#58dc00',
    '#287b00',
    '#a8f07a',
    '#4ae8c4',
    '#3b88eb',
    '#3824aa',
    '#a700ff',
    '#d300e7',
  ];
const ChatRoom = () => {
    const [show, setShow] = useState(true)
    const instance = useRef<Socket>();
    const [message, setMessage] = useState<any[]>([])
    const [user, setUser] = useState<string>('')
    const [current, setCurrent] = useState<string>('')
    const [isFeedIn, setIsFeedIn] = useState<boolean>(false)

    useEffect(() => {
        instance.current = io('ws://localhost:3000');
        instance.current.on('login', (data) => {
            const message = 'Welcome to Socket.IO Chat – ';
            setMessage([{
                title: message
            }, {
                title: `there's ${data.numUsers} participant`
            }])
        });
        // // Whenever the server emits 'new message', update the chat body
        instance.current.on('new message', (data) => {
            setMessage(message=>{
                message.push(data)
                return [...message]
            })
        });
        // Whenever the server emits 'user joined', log it in the chat body
        instance.current.on('user joined', (data) => {
            setMessage(message=>{
                message.push({title:`${data.username} joined`})
                return [...message]
            })
        });

        // // Whenever the server emits 'user left', log it in the chat body
        instance.current.on('user left', (data) => {
            setMessage((message)=>{
                message.push({
                    title: `${data.username} left`
                }, {
                    title: `there's ${data.numUsers} participant`
                })
                return [...message]
            })
        });

        // // Whenever the server emits 'typing', show the typing message
        instance.current.on('typing', (data) => {
            setMessage(message=>{
                message.push({
                    title: data.username,
                    message: `is typing`
                })
                return [...message]
            })
        });

        // // Whenever the server emits 'stop typing', kill the typing message
        instance.current.on('stop typing', (data) => {
            setMessage(message=>{
                let messages = message.filter(item=>{
                    return item.title!==data.username
                })
                return [...messages]
            })
        });

        instance.current.on('disconnect', () => {
            setMessage((message)=>{
                message.push({
                    title: 'you have been disconnected'
                })
                return [...message]
            })
        });

        instance.current.on('reconnect', () => {
            setMessage(messages=>[...messages, {
                message: 'you have been disconnected'
            }])
            if (user) {
                instance.current?.emit('add user', user);
            }
        });

        instance.current.on('reconnect_error', () => {
            setMessage((message)=>[...message,{
                title: 'attempt to reconnect has failed'
            }])
        });
        return ()=>{
            instance.current?.emit(`user left`)
        }
    }, [])
    const notarize = (e: React.KeyboardEvent<HTMLInputElement>) => {
        if (e.keyCode === 13) {
            setShow(false)
            setUser(e.currentTarget.value)
            instance.current?.emit('add user', e.currentTarget.value)
        }
    }
    const commitValue = (e: React.KeyboardEvent<HTMLInputElement>) => {
        if (e.keyCode === 13) {
            instance.current?.emit("new message", e.currentTarget.value)
            setMessage([...message, {
                username: user,
                message: current
            }])
            setCurrent('')
        }
    }
    const feedIn = (e: React.FormEvent<HTMLInputElement>) => {
        setCurrent(e.currentTarget.value)
        if(!isFeedIn){
            instance.current?.emit('stop typing')
            instance.current?.emit('typing')
            time = setTimeout(()=>{
                instance.current?.emit('stop typing')
                setIsFeedIn(true)
            },TYPING_TIMER_LENGTH)
        }else{
            clearTimeout(time)
            instance.current?.emit('stop typing')
            instance.current?.emit('typing')
            time = setTimeout(()=>{
                instance.current?.emit('stop typing')
                setIsFeedIn(false)
            },TYPING_TIMER_LENGTH)
        }
    }
    function getUserColor(username: string) {
        let str = md5(username);
        return parseInt(str.slice(-1), 16) % 12;
    }
    return (
        <ul className={styles.pages}>
            <li className={classNames(styles.page, styles.chat)} style={{ display: !show ? "block" : "" }}>
                <div className={styles.chatArea}>
                    <ul className={styles.messages}>
                        {
                            message.map((item, index) => {
                                if (item.title) {
                                    if(item.message){
                                        return <li className={classNames(styles.typing)} key={index}><span className={styles.title} style={{ color: COLORS[getUserColor(item.title)] }}>{item.title}</span><span className={styles.messageBody}>{item.message}</span></li>
                                    }
                                    return <li className={styles.log} key={index}>{item.title}</li>
                                } else {
                                    return <li className={styles.message} key={index}><span style={{ color: COLORS[getUserColor(item.username)] }}>{item.username}</span><span>{item.message}</span> </li>
                                }
                            })
                        }
                    </ul>
                </div>
                <input value={current} onInput={feedIn} className={styles.inputMessage} onKeyDown={commitValue} placeholder="Type here..." />
            </li>
            <li className={classNames(styles.login, styles.page)} style={{ display: show ? "" : "none" }}>
                <div className={styles.form}>
                    <h3 className={styles.title}>What's your nickname?</h3>
                    <input autoFocus onKeyDown={notarize} className={classNames(styles.usernameInput)} type="text" maxLength={14} />
                </div>
            </li>
        </ul>
    )
}
export default ChatRoom
