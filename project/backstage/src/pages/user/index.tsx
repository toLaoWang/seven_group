import React, { useEffect, useState, Key } from 'react';
import { Form, Row, Col, Input, Button, Table, Tooltip, Popconfirm, Space, Badge, Select, ConfigProvider } from 'antd';
import "./index.less";
import { IUserList, IUserParams } from "@/types/index";
import { observer } from 'mobx-react-lite';
import useStore from '@/context/useStore';
import zhCN from 'antd/es/locale/zh_CN';
import { fomatTime } from "@/utils/fomatTime";
const Option = Select.Option;
const User = () => {
    const [selectedRowKeys, setSelectedRowKeys] = useState<Key[]>([]);
    const [form] = Form.useForm();
    const [page, setPage] = useState(1);
    const [pageSize, setPageSize] = useState(12);
    const [params, setParams] = useState({});
    const store = useStore();

    useEffect(() => {
        store.user.getUserList(page, pageSize, params);
    }, [page, pageSize, params])

    const onGenderChange = (value: string) => {
        switch (value) {
            case 'admin':
                form.setFieldsValue({ role: value });
                return;
            case 'visitor':
                form.setFieldsValue({ role: value });
                return;
            case 'locked':
                form.setFieldsValue({ status: value });
                return;
            case 'active':
                form.setFieldsValue({ status: value });
                return;
        }
    };

    const children = [
        <Col span={6} key={1}>
            <Form.Item
                name='name'
                label='账号'
                rules={[
                    {
                        message: '请输入用户账户!',
                    },
                ]}
            >
                <Input placeholder="请输入用户账户" />
            </Form.Item>
        </Col>,
        <Col span={6} key={2}>
            <Form.Item
                name='email'
                label='邮箱'
                rules={[
                    {
                        message: '请输入账户邮箱!',
                    },
                ]}
            >
                <Input placeholder="请输入账户邮箱" />
            </Form.Item>
        </Col>,
        <Col span={6} key={3}>
            <Form.Item name="role" label="角色">
                <Select onChange={onGenderChange}>
                    <Select.Option value="admin">管理员</Select.Option>
                    <Select.Option value="visitor">访客</Select.Option>
                </Select>,
            </Form.Item>
        </Col>,
        <Col span={6} key={4}>
            <Form.Item name="status" label="状态">
                <Select onChange={onGenderChange}>
                    <Select.Option value="locked">锁定</Select.Option>
                    <Select.Option value="active">可用</Select.Option>
                </Select>,
            </Form.Item>
        </Col>
    ]


    const columns = [
        { title: '账户', dataIndex: 'name', key: 'name' },
        { title: '邮箱', dataIndex: 'email', key: 'email' },
        {
            title: '角色', dataIndex: 'role', key: 'role',
            render: (role: string) => { return role === "admin" ? '管理员' : '访客' }
        },
        {
            title: '状态', dataIndex: 'status', key: 'status',
            render: (status: string) => { return status === "active" ? <Badge color="#87d068" text="可用" /> : <Badge color="#f50" text="已锁定" /> }
        },
        { title: '注册日期', dataIndex: 'createAt', key: 'createAt' , render: (time: string) => fomatTime(time)},
        {
            title: '操作',
            dataIndex: '',
            key: 'x',
            render: (text: IUserList) => {
                return <>
                    <a style={{ margin: "0 10px" }} onClick={() => {
                        setSelectedRowKeys([text.id]);
                        changeBtn(text.status === "active" ? 1 : 0)
                    }}>{text.status === "active" ? "禁用" : "启用"}</a>
                    <a style={{ margin: "0 10px" }} onClick={() => {
                        setSelectedRowKeys([text.id]);
                        changeBtn(text.role === "admin" ? 2 : 3)
                    }}>{text.role === "admin" ? "解除授权" : "授权"}</a>
                </>
            }
        }
    ];

    //搜索
    function onFinish(values: IUserParams) {
        setParams(values);
        setPage(1);
    };


    //监听多选框选中状态
    function onSelectChange(selectedRowKeys: Key[]) {
        setSelectedRowKeys(selectedRowKeys)
    };

    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange,
    };
    //分页切换
    function changePage(pagination: any) {
        setPage(pagination.current);
        setPageSize(pagination.pageSize);
    }

    const changeBtn = (ind: number) => {
        let newArr: IUserList[] = [];
        store.user.userList.forEach((item) => {
            selectedRowKeys.forEach((id) => {
                if (item.id === id) {
                    switch (ind) {
                        case 0:
                            item.status = "active"
                            break;
                        case 1:
                            item.status = "locked"
                            break;
                        case 2:
                            item.role = "visitor"
                            break;
                        case 3:
                            item.role = "admin"
                            break;
                        default:
                            break;
                    }
                    newArr.push(item)
                }
            })
        })
        store.user.editUserList(JSON.parse(JSON.stringify(newArr)), page, pageSize, params)
    }

    return (
        <>
            <Form
                form={form}
                name="advanced_search"
                className="ant-advanced-search-form"
                onFinish={onFinish}
            >
                <Row gutter={20}>{children}</Row>
                <Row>
                    <Col
                        span={24}
                        style={{
                            textAlign: 'right',
                        }}
                    >
                        <Button type="primary" htmlType="submit">
                            搜索
                        </Button>
                        <Button
                            style={{
                                margin: '0 8px',
                            }}
                            onClick={() => {
                                form.resetFields();
                                setParams({});
                            }}
                        >
                            重置
                        </Button>
                    </Col>
                </Row>
            </Form>


            <div className="table">
                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                    {selectedRowKeys.length ?
                        <div>
                            {["启用", "禁用", "解除授权", "授权"].map((item, index) => {
                                return <Button style={{ marginRight: "10px" }}
                                    key={index}
                                    onClick={() => changeBtn(index)}
                                >
                                    {item}
                                </Button>
                            })}
                        </div>
                        : <div></div>}
                    <Tooltip placement="top" title="刷新">
                        <svg onClick={() => {
                            setPage(1);
                            setSelectedRowKeys([]);
                        }} style={{ cursor: 'pointer' }} viewBox="64 64 896 896" focusable="false" data-icon="reload" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M909.1 209.3l-56.4 44.1C775.8 155.1 656.2 92 521.9 92 290 92 102.3 279.5 102 511.5 101.7 743.7 289.8 932 521.9 932c181.3 0 335.8-115 394.6-276.1 1.5-4.2-.7-8.9-4.9-10.3l-56.7-19.5a8 8 0 00-10.1 4.8c-1.8 5-3.8 10-5.9 14.9-17.3 41-42.1 77.8-73.7 109.4A344.77 344.77 0 01655.9 829c-42.3 17.9-87.4 27-133.8 27-46.5 0-91.5-9.1-133.8-27A341.5 341.5 0 01279 755.2a342.16 342.16 0 01-73.7-109.4c-17.9-42.4-27-87.4-27-133.9s9.1-91.5 27-133.9c17.3-41 42.1-77.8 73.7-109.4 31.6-31.6 68.4-56.4 109.3-73.8 42.3-17.9 87.4-27 133.8-27 46.5 0 91.5 9.1 133.8 27a341.5 341.5 0 01109.3 73.8c9.9 9.9 19.2 20.4 27.8 31.4l-60.2 47a8 8 0 003 14.1l175.6 43c5 1.2 9.9-2.6 9.9-7.7l.8-180.9c-.1-6.6-7.8-10.3-13-6.2z"></path></svg>
                    </Tooltip>
                </div>
                <ConfigProvider locale={zhCN}>
                    <Table
                        columns={columns}
                        rowSelection={rowSelection}
                        onChange={changePage}
                        pagination={{
                            pageSizeOptions: ["8", "12", "24", "36"],
                            showSizeChanger: true,
                            current: page,
                            pageSize: pageSize,
                            total: store.user.total,
                            showTotal: () => `共${store.user.total}条`
                        }}
                        dataSource={store.user.userList.map(item => {
                            item.key = item.id;
                            return item
                        })
                        }
                    />
                </ConfigProvider>
            </div>
        </>
    );
};

export default observer(User);
