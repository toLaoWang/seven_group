import React, { useEffect, useState } from 'react';
import { Form, Row, Col, Input, Button, Select, Table, Radio, Divider, Badge, Space, ConfigProvider, Popconfirm, message } from 'antd';
import { SyncOutlined } from '@ant-design/icons';
import zhCN from 'antd/es/locale/zh_CN';
import { observer } from 'mobx-react-lite';
import './index.less'
import useStore from '@/context/useStore';
import moment from 'moment';
import { useHistory } from 'umi';
import share from '@/components/share';
import { IPageList } from '@/types';
import ConfimPage from '@/components/ConfimPage';
const { Option } = Select;

const AdvancedSearchForm = () => {
    const [form] = Form.useForm();
    const history = useHistory()
    const [page, setPage] = useState(1)
    const [pageSize, setPageSize] = useState(12)
    const store = useStore();
    const [show, setShow] = React.useState(false);
    const [selectArry, setSelectArry] = React.useState<React.Key[]>([]);
    const [confirmLoading, setConfirmLoading] = React.useState(false);

    // 下线 发布
    const OffLine = async (record?: IPageList) => {
        if (JSON.parse(localStorage.user).role === 'visitor') {
            message.warning('访客无权进行该操作');
        } else {
            if (record!.status === 'publish') {
                let result = await store.page.getOutPage(record!.id, 'draft');
                if (result.statusCode === 200) {
                    message.success('操作成功');
                    store.page.getPageList({ page, pageSize })
                }
            } else {
                let result = await store.page.getOutPage(record!.id, 'publish');
                if (result.statusCode === 200) {
                    message.success('操作成功');
                    store.page.getPageList({ page, pageSize })
                }
            }
        }
    }
    // 下线
    const OffLines = async (array: React.Key[],status:string) => {
        if (JSON.parse(localStorage.user).role === 'visitor') {
            message.warning('访客无权进行该操作');
        } else {
            await store.page.getOutPages(array,status);
        }
    }

    // 表头
    const columns:any = [
        {
            title: '名称',
            dataIndex: 'name',
        },
        {
            title: '路径',
            dataIndex: 'path',
        },
        {
            title: '顺序',
            dataIndex: 'order',
        }, {
            title: '阅读量',
            dataIndex: 'views',

            render: (text: string) => {
                return <Space>
                    <Badge
                        className="site-badge-count-109"
                        showZero={true}
                        count={text}
                        style={{ backgroundColor: '#52c41a' }}
                    />
                </Space>
            }
        }, {
            title: '状态',
            dataIndex: 'status',
            width: 65,
            render: (text: string) => {
                    return text === 'publish' ? <Badge status="success" text="已发布" /> : <Badge status="warning" text="草稿" />

            }
        }, {
            title: '发布时间',
            dataIndex: 'publishAt',
            render: (text: string) => {
                return <span>{moment(text).format('YYYY-MM-DD HH:mm:s')}</span>
            }
        }, {
            title: '操作',
            dataIndex: 'address',
            width: 200,
            render: (text: string, record: IPageList) => {
                return <div style={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
                    <a><Button type="link" onClick={() => history.push('/page/editor/' + record.id)}>编辑</Button></a><Divider type="vertical" />
                    <a><Button type="link" onClick={() => OffLine(record)}>{record.status === 'publish' ? '下线' : '发布'}</Button></a><Divider type="vertical" />
                    <a><Button type="link" onClick={() => share()}>查看访问</Button></a><Divider type="vertical" />
                    <ConfimPage title='确认删除这个文章？' id={record.id} page={page} pageSize={pageSize}><a><Button type="link">删除</Button></a></ConfimPage>
                </div>
            },
        },
    ];

    // 多选
    const rowSelection = {
        onChange: (selectedRowKeys: React.Key[]) => {
            selectedRowKeys.length ? setShow(true) : setShow(false);
            setSelectArry(selectedRowKeys)
        },
    };
    const getFields = () => {
        const children = [];
        children.push(
            <Col span={8} key={1}>
                <Form.Item
                    name={`name`}
                    label={`名称`}
                >
                    <Input placeholder="请输入页面名称" />
                </Form.Item>
            </Col>,
            <Col span={8} key={2}>
                <Form.Item
                    name={`path`}
                    label={`路径`}
                >
                    <Input placeholder="请输入页面路径" />
                </Form.Item>
            </Col>,
            <Col span={8} key={3}>
                <Form.Item
                    name="status"
                    label="状态"
                >
                    <Select >
                        <Option value="publish">已发布</Option>
                        <Option value="draft">草稿</Option>
                    </Select>
                </Form.Item>
            </Col>
        );
        return children;
    };

    useEffect(() => {
        store.page.getPageList({ page, pageSize })
    }, [])

    // 搜索
    const onFinish = (values: any) => {
        store.page.getPageList({ ...{ page, pageSize }, ...values })
    };
    // 切换每页条目数
    function onChange(page: number, pageSize?: number) {
        store.page.getPageList({ page, pageSize })
    }
    // 新建页面
    function newPage() {
        history.push('/page/editor')
    }
    // 刷新
    function refresh() {
        store.page.getPageList({ page, pageSize })
    }


    return (
        <div>
            <Form
                form={form}
                name="advanced_search"
                className="ant-advanced-search-form from_top"
                onFinish={onFinish}
            >
                <Row gutter={24} className='page_row'>{getFields()}</Row>
                <Row>
                    <Col span={24} style={{ textAlign: 'right' }}>
                        <Button type="primary" htmlType="submit"> 搜索</Button>
                        <Button
                            style={{ margin: '0 8px' }}
                            onClick={() => {
                                form.resetFields();
                            }}
                        >
                            重置
                        </Button>
                    </Col>
                </Row>
            </Form>
            <div className='table'>
                <div>
                    <div style={{ opacity: show ? '1' : '0' }}><Button  onClick={() => OffLines(selectArry,'publish')}>发布</Button>&ensp;<Button onClick={() => OffLines(selectArry,'draft')}>下线</Button>&ensp;<ConfimPage title='确认删除？' selectedRow={selectArry} page={page} pageSize={pageSize}><Button danger >删除</Button></ConfimPage>
                    </div>
                    <div>
                        <Button type="primary" onClick={() => newPage()}>+&nbsp;新建</Button>
                        <span onClick={() => refresh()}><SyncOutlined style={{ width: '16px', marginLeft: '12px' }} /></span>
                    </div>
                </div>
                <ConfigProvider locale={zhCN}>
                    <Table
                        rowSelection={{
                            ...rowSelection,
                        }}
                        rowKey='id'
                        columns={columns}
                        dataSource={store.page.pageLise}
                        pagination={{
                            defaultPageSize: 12,
                            total: store.page.pageCount,
                            pageSizeOptions: ['8', '12', '24', '36'],
                            defaultCurrent: page,
                            showSizeChanger: true,
                            showTotal: (total => `共 ${store.page.pageCount} 条`),
                            onChange: onChange
                        }}
                    />
                </ConfigProvider>
            </div>
        </div>
    );
};
export default observer(AdvancedSearchForm)
