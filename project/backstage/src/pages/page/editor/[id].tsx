import React, { useEffect, useState } from 'react'
import ForEditor from 'for-editor'
import { makeHtml, makeToc } from '@/utils/markdown'
import { Button, Modal, Input, Menu, Dropdown, message, Drawer, Form, Select, Switch, Popconfirm, InputNumber } from 'antd'
import { CloseOutlined, EllipsisOutlined, ExclamationCircleOutlined, FileImageOutlined } from '@ant-design/icons'
import style from './index.less'
import useStore from '@/context/useStore'
import { IArticleItem, ICategoryItem, IPageList } from '@/types'
import { useHistory } from 'umi'
import { observer } from 'mobx-react-lite';
const { Option } = Select;
const { confirm } = Modal;


const EditorID: React.FC = (props) => {

    const store = useStore()
    const [name, setName] = useState('')
    const [content, setContent] = useState('');
    const [form] = Form.useForm()
    const history = useHistory()
    const [visible, setVisible] = useState(false)
    let [count, setCount] = useState(0)

    useEffect(() => {
        store.article.getPageDetail(location.pathname.split('/')[location.pathname.split('/').length - 1])
    }, [])
    useEffect(() => {
        setName(store.article.pageDetail?.name!)
        setContent(store.article.pageDetail?.content!)
    }, [store.article.pageDetail])
    useEffect(() => {
        if (visible && count === 1) {
            form.setFieldsValue(JSON.parse(JSON.stringify(store.article.pageDetail)))
        }
        setCount(count += 1)
    }, [visible])

    const toolbar = {
        h1: true, // h1
        h2: true, // h2
        h3: true, // h3
        h4: true, // h4
        img: true, // 图片
        link: true, // 链接
        code: true, // 代码块
        preview: true, // 预览
        expand: true, // 全屏
        /* v0.0.9 */
        undo: true, // 撤销
        redo: true, // 重做
        save: true, // 保存
        /* v0.2.3 */
        subfield: true, // 单双栏模式
    }
    // 设置
    const showDrawer = () => {
        if (name) {
            setVisible(true)
        } else {
            message.warning('请输入页面标题')
        }
    };

    const onClose = () => {
        setVisible(false)
    };
    // 删除
    const getDelPage = async () => {
        console.log('del');
        if (localStorage.user.rule !== "visitor") {
            let result = await store.page.getDelPage(location.pathname.split('/')[location.pathname.split('/').length - 1]);
            if (result.statusCode === 200) {
                message.warning('页面删除成功');
                history.push('/page')
            }
        } else {
            message.warning("访客无权进行该操作")
        }

    }
    // 弹出确认
    function confirmDel() {
        Modal.confirm({
            title: '确认删除？',
            icon: <ExclamationCircleOutlined />,
            content: '删除内容后，无法恢复。',
            okText: '确认',
            cancelText: '取消',
            onOk() {
                getDelPage()
            },
        });
    }
    const menu = (
        <Menu>
            <Menu.Item key="31" disabled={history.location.pathname === '/article/editor'}>查看</Menu.Item>
            <Menu.Item key="21" onClick={showDrawer}>设置</Menu.Item>
            <Menu.Item key="33" onClick={() => submit('draft')}>保存草稿</Menu.Item>
            <Menu.Item key="46" disabled={history.location.pathname === '/article/editor'} onClick={confirmDel}>删除</Menu.Item>
        </Menu>
    );

    // 点击发布按钮
    async function submit(status: string) {
        let values: { [key: string]: string | boolean | null } = form.getFieldsValue()
        let obj: IPageList | null = null;
        if (!name) {
            message.warning('请输入页面标题');
            return
        }
        // md
        values.content = content!;
        // html
        values.html = makeHtml(content!)
        // toc
        values.toc = JSON.stringify(makeToc(makeHtml(content!) as string))
        values.name = name;
        values.status = status;
        obj = ({ ...store.article.pageDetail, ...values }) as IPageList;
        console.log(obj);


        if (!content) {
            message.warning('请输入页面内容')
        } else if (!values.path) {
            message.warning('请输入页面路径')
            setVisible(true)
        } else {
            let result = await store.article.getEditPage(location.pathname.split('/')[location.pathname.split('/').length - 1], obj)
            if (result) {
                if (status === 'publish') {
                    message.warning('页面发布成功')
                } else {
                    message.warning('页面保存草稿成功')
                }
                history.push(`/page/editor/${result.data.id}`)
            }
        }


    };
    function confirmChu() {
        history.push('/page')
    }
    return (
        <div>
            <header className={style.header}>
                <div>
                    <Popconfirm
                        placement="rightTop"
                        title={'确认关闭？如果有内容变更，请先保存。'}
                        onConfirm={confirmChu}
                        okText="确定"
                        cancelText="取消"
                    >
                        <Button><CloseOutlined /></Button>
                    </Popconfirm>

                    <input className={style.Input} defaultValue={name} placeholder='请输入页面名称' style={{ border: 'none', borderBottom: '1px solid #ccc', outline: 'none', width: '300px' }} onChange={e => setName(e.target.value)} />
                </div>
                <div>
                    <Button type='primary' onClick={() => submit('publish')}>发布</Button>
                    <Dropdown overlay={menu}>
                        <EllipsisOutlined style={{ marginLeft: '16px' }} />
                    </Dropdown>
                </div>

            </header>
            <section className={style.editor}>
                <ForEditor
                    height='100%'
                    value={content}
                    toolbar={toolbar}
                    onChange={value => setContent(value)}
                />
            </section>
            <Drawer
                title="页面设置"
                placement={'right'}
                closable={true}
                onClose={onClose}
                visible={visible}
                width={'480px'}
                key='id'
                footer={
                    <Button type='primary' onClick={() => {
                        setVisible(false)
                    }} htmlType="submit">确认</Button>
                }
                footerStyle={{ textAlign: 'right' }}
            >

                <Form
                    key='from'
                    form={form}
                >
                    <Form.Item name="cover" label="封面" key='1'>
                        <Input addonAfter={<FileImageOutlined />} placeholder='请输入页面封面' />
                    </Form.Item>
                    <Form.Item name="path" label="路径" key='2'>
                        <Input placeholder="请配置页面路径" />
                    </Form.Item>
                    <Form.Item name="order" label="顺序" key='3'>
                        <InputNumber min={0} defaultValue={0} />
                    </Form.Item>

                </Form>
            </Drawer>



        </div >
    )


}
export default observer(EditorID)
