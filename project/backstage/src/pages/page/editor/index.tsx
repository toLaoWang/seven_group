import React, { useEffect, useState } from 'react'
import ForEditor from 'for-editor'
import { makeHtml, makeToc } from '@/utils/markdown'
import { Button, Input, Menu, Dropdown, message, Drawer, Form, Select, Popconfirm, InputNumber } from 'antd'
import { CloseOutlined, EllipsisOutlined, FileImageOutlined } from '@ant-design/icons'
import style from './index.less'
import useStore from '@/context/useStore'
import { ICategoryItem, ITagItem } from '@/types'
import { useHistory } from 'umi'
import { observer } from 'mobx-react-lite';

const { Option } = Select;
const Editor: React.FC = (props) => {
    const [name, setName] = useState('')
    const [content, setContent] = useState("");
    const [cover, setCover] = useState('');
    const [form] = Form.useForm()
    const store = useStore()
    const history = useHistory()


    const toolbar = {
        h1: true, // h1
        h2: true, // h2
        h3: true, // h3
        h4: true, // h4
        img: true, // 图片
        link: true, // 链接
        code: true, // 代码块
        preview: true, // 预览
        expand: true, // 全屏
        /* v0.0.9 */
        undo: true, // 撤销
        redo: true, // 重做
        save: true, // 保存
        /* v0.2.3 */
        subfield: true, // 单双栏模式
    }
    const [visible, setVisible] = useState(false)
    useEffect(() => {
        store.article.getTagList()
        store.article.getCategoryList()
    }, [])
    // 设置
    const showDrawer = () => {
        if (name) {
            setVisible(true)
        } else {
            message.warning('请输入页面名称')
        }
    };

    const onClose = () => {
        setVisible(false)
    };

    const menu = (
        <Menu>
            <Menu.Item key="11" disabled={history.location.pathname === '/page/editor'}>查看</Menu.Item>
            <Menu.Item key="21" onClick={showDrawer}>设置</Menu.Item>
            <Menu.Item key="31" onClick={() => submit('draft')}>保存草稿</Menu.Item>
            <Menu.Item key="46" disabled={history.location.pathname === '/page/editor'}>删除</Menu.Item>
        </Menu>
    );

    // 点击发布按钮
    async function submit(status: string) {
        let values: { [key: string]: string | boolean | null } = form.getFieldsValue()

        if (!name) {
            message.warning('请输入页面名称');
            return
        }
        // md
        values.content = content;
        // html
        values.html = makeHtml(content)
        // toc
        values.toc = JSON.stringify(makeToc(makeHtml(content) as string))
        values.name = name;
        values.status = status;
        if (cover) {
            values.cover = cover;
        } else {
            values.cover = null;
        }
        console.log(values);


        if (!content) {
            message.warning('请输入文章内容')
        } else if(!values.path){
            message.warning('请输入文章路径')
            setVisible(true)
        }else {
            let result = await store.article.getPubPage(values)
            if (result) {
                if (status === 'publish') {
                    message.success('文章发布成功')
                } else {
                    message.success('页面已保存草稿成功')
                }
                history.push(`/page/editor/${result.data.id}`)
            }
        }


    };
    function confirm() {
        history.push('/article')
    }


    return (
        <div>
            <header className={style.header}>
                <div>
                    <Popconfirm
                        placement="rightTop"
                        title={'确认关闭？如果有内容变更，请先保存。'}
                        onConfirm={confirm}
                        okText="确定"
                        cancelText="取消"
                    >
                        <Button><CloseOutlined /></Button>
                    </Popconfirm>

                    <input className={style.Input} placeholder='请输入页面名称' style={{ border: 'none', borderBottom: '1px solid #ccc', outline: 'none', width: '300px' }} onChange={e => setName(e.target.value)} />
                </div>
                <div>
                    <Button type='primary' onClick={() => submit('publish')}>发布</Button>
                    <Dropdown overlay={menu}>
                        <EllipsisOutlined style={{ marginLeft: '16px' }} />
                    </Dropdown>
                </div>

            </header>
            <section className={style.editor}>
                <ForEditor
                    height='100%'
                    value={content}
                    toolbar={toolbar}
                    onChange={value => setContent(value)}
                />
            </section>
            <Drawer
                title="文章设置"
                placement={'right'}
                closable={true}
                onClose={onClose}
                visible={visible}
                key='id'
                width={'480px'}
                footer={
                    [<Button type='primary' onClick={() => {
                        setVisible(false)
                        // console.log('form..', form.getFieldsValue())
                    }} htmlType="submit">确认</Button>]
                }
                footerStyle={{ textAlign: 'right' }}
            >

                <Form
                    key='from'
                    form={form}
                >
                    <Form.Item name="cover" label="封面" key='1'>
                        <Input addonAfter={<FileImageOutlined />} placeholder='请输入页面封面' />
                    </Form.Item>
                    <Form.Item name="path" label="路径" key='2'>
                        <Input placeholder="请配置页面路径" />
                    </Form.Item>
                    <Form.Item name="order" label="顺序" key='3'>
                        <InputNumber min={0} defaultValue={0} />
                    </Form.Item>
                </Form>
            </Drawer>


        </div >
    )
}
export default observer(Editor)
