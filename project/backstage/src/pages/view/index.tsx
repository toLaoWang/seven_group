import React, { useEffect, useState, Key } from 'react';
import { Form, Row, Col, Input, Button, Table, Tooltip, Popconfirm, Space, Badge, ConfigProvider } from 'antd';
import "./index.less";
import { IViewParams, IViewList } from "@/types/index";
import { observer } from 'mobx-react-lite';
import useStore from '@/context/useStore';
import zhCN from 'antd/es/locale/zh_CN';
import { fomatTime } from '@/utils/fomatTime';
import { NavLink } from 'react-router-dom';
const View: React.FC = () => {
    const [selectedRowKeys, setSelectedRowKeys] = useState<Key[]>([]);
    const [form] = Form.useForm();
    const [page, setPage] = useState(1);
    const [params, setParams] = useState({});
    const [pageSize, setPageSize] = useState(12);
    const store = useStore();

    useEffect(() => {
        store.view.getViewList(page, pageSize, params);
    }, [page, pageSize, params])

    const children = [
        <Col span={6} key={1}>
            <Form.Item
                name='ip'
                label='IP'
            >
                <Input placeholder="请输入IP地址" />
            </Form.Item>
        </Col>,
        <Col span={6} key={2}>
            <Form.Item
                name='userAgent'
                label='UA'
            >
                <Input placeholder="请输入 User Agent" />
            </Form.Item>
        </Col>,
        <Col span={6} key={3}>
            <Form.Item
                name='url'
                label='URL'
            >
                <Input placeholder="请输入URL" />
            </Form.Item>
        </Col>,
        <Col span={6} key={4}>
            <Form.Item
                name='address'
                label='地址'
            >
                <Input placeholder="请输入地址" />
            </Form.Item>
        </Col>,
        <Col span={6} key={5}>
            <Form.Item
                name='browser'
                label='浏览器'
            >
                <Input placeholder="请输入浏览器" />
            </Form.Item>
        </Col>,
        <Col span={6} key={6}>
            <Form.Item
                name='engine'
                label='内核'
            >
                <Input placeholder="请输入内核" />
            </Form.Item>
        </Col>,
        <Col span={6} key={7}>
            <Form.Item
                name='count'
                label='os'
            >
                <Input placeholder="请输入OS" />
            </Form.Item>
        </Col>,
        <Col span={6} key={8}>
            <Form.Item
                name='device'
                label='设备'
            >
                <Input placeholder="请输入设备" />
            </Form.Item>
        </Col>,
    ]

    const columns = [
        { title: 'URL', dataIndex: 'url', key: 'url', fixed: 'left', width: 200 },
        { title: 'IP', dataIndex: 'ip', key: 'ip', width: 126 },
        { title: '浏览器', dataIndex: 'browser', key: 'browser', width: 126 },
        { title: '内核', dataIndex: 'engine', key: 'engine', width: 126 },
        { title: '操作系统', dataIndex: 'os', key: 'os', width: 126 },
        { title: '设备', dataIndex: 'device', key: 'device', width: 126 },
        { title: '地址', dataIndex: 'address', key: 'address', width: 126 },
        {
            title: '访问量', dataIndex: 'count', key: 'count', width: 126,
            render: (text: IViewList) => {
                return <Space>
                    <Badge
                        className="site-badge-count-109"
                        showZero={true}
                        count={text}
                        style={{ backgroundColor: '#52c41a' }}
                    />
                </Space>
            }
        },
        { title: '访问时间', dataIndex: 'updateAt', key: 'updateAt', width: 126, render: (time: string) => fomatTime(time) },
        {
            title: '操作',
            dataIndex: '',
            key: 'x',
            width: 126,
            fixed: 'right',
            render: (text: IViewList) => <Popconfirm placement="top" title="确认删除这个搜索记录？"
                onConfirm={() => {
                    store.view.delViewList([text.id] as string[], page, pageSize, params);
                    setSelectedRowKeys([]);
                }} okText="确定" cancelText="取消">
                <a>删除</a>
            </Popconfirm>
        },
    ];

    //搜索
    const onFinish = (values: IViewParams) => {
        setParams(values);
        setPage(1);
    };

    //监听多选框选中状态
    const onSelectChange = (selectedRowKeys: Key[]) => {
        setSelectedRowKeys(selectedRowKeys)
    };

    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange,
    };

    function changePage(pagination: any) {
        setPage(pagination.current);
        setPageSize(pagination.pageSize);
    }

    return (
        <>
            <Form
                form={form}
                name="advanced_search"
                className="ant-advanced-search-form"
                onFinish={onFinish}
            >
                <Row gutter={20}>{children}</Row>
                <Row>
                    <Col
                        span={24}
                        style={{
                            textAlign: 'right',
                        }}
                    >
                        <Button type="primary" htmlType="submit">
                            搜索
                        </Button>
                        <Button
                            style={{
                                margin: '0 8px',
                            }}
                            onClick={() => {
                                form.resetFields();
                                setParams({});
                            }}
                        >
                            重置
                        </Button>
                    </Col>
                </Row>
            </Form>


            <div className="table">
                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                    {selectedRowKeys.length ?
                        <Popconfirm placement="top" title="确认删除？"
                            onConfirm={() => {
                                store.view.delViewList(selectedRowKeys as string[], page, pageSize, params);
                                setSelectedRowKeys([]);
                            }} okText="确定" cancelText="取消">
                            <Button danger>删除</Button>
                        </Popconfirm> : <div></div>}
                    <Tooltip placement="top" title="刷新">
                        <svg onClick={() => {
                            store.view.getViewList(page, pageSize)
                            setSelectedRowKeys([]);
                        }} style={{ cursor: 'pointer' }} viewBox="64 64 896 896" focusable="false" data-icon="reload" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M909.1 209.3l-56.4 44.1C775.8 155.1 656.2 92 521.9 92 290 92 102.3 279.5 102 511.5 101.7 743.7 289.8 932 521.9 932c181.3 0 335.8-115 394.6-276.1 1.5-4.2-.7-8.9-4.9-10.3l-56.7-19.5a8 8 0 00-10.1 4.8c-1.8 5-3.8 10-5.9 14.9-17.3 41-42.1 77.8-73.7 109.4A344.77 344.77 0 01655.9 829c-42.3 17.9-87.4 27-133.8 27-46.5 0-91.5-9.1-133.8-27A341.5 341.5 0 01279 755.2a342.16 342.16 0 01-73.7-109.4c-17.9-42.4-27-87.4-27-133.9s9.1-91.5 27-133.9c17.3-41 42.1-77.8 73.7-109.4 31.6-31.6 68.4-56.4 109.3-73.8 42.3-17.9 87.4-27 133.8-27 46.5 0 91.5 9.1 133.8 27a341.5 341.5 0 01109.3 73.8c9.9 9.9 19.2 20.4 27.8 31.4l-60.2 47a8 8 0 003 14.1l175.6 43c5 1.2 9.9-2.6 9.9-7.7l.8-180.9c-.1-6.6-7.8-10.3-13-6.2z"></path></svg>
                    </Tooltip>
                </div>
                <ConfigProvider locale={zhCN}>
                    <Table
                        columns={columns as any}
                        rowSelection={rowSelection}
                        scroll={{ x: 1300 }}
                        onChange={changePage}
                        pagination={{
                            pageSizeOptions: ["8", "12", "24", "36"],
                            current: page,
                            pageSize: pageSize,
                            total: store.view.total,
                            showTotal: () => `共${store.view.total}条`
                        }}
                        dataSource={store.view.viewList.map(item => {
                            item.key = item.id;
                            return item
                        })
                        }
                    />
                </ConfigProvider>
            </div>
        </>
    );
};

export default observer(View);