import News from '@/components/news'
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import InfiniteList from '@/components/infiniteList';
import { IRootState } from '@/types';
import styles from './index.less'
import MsgBoards from '@/components/msgboards'
import { msgboardItem } from '@/types/msgboard';
const About: React.FC = (props) => {
    let [page, setPage] = useState(1);
    const dispatch = useDispatch()
    const { recommend } = useSelector((state: IRootState) => state.article)
    const { aboutList, getCommentList, CommentNum } = useSelector((state: IRootState) => state.about)
    useEffect(() => {
        if (!recommend.length) {
            dispatch({
                type: 'article/getRecommend'
            })
        }
        dispatch({
            type: "about/getAboutList"
        })
    }, [])
    useEffect(() => {
        dispatch({
            type: 'about/getCommentLists',
            payload: page
        })
    }, [page])
    function changePage(page: number) {
        setPage(page)
    }
    return <div id={styles.about}>
        <div className={styles.container}>
            <div className={styles.box}>
                <img className={styles.img} src={aboutList.cover} alt="文章封面" />
                <div className={styles.text}>
                    <div>
                        <h2>这世界只有一种英雄主义，就是在看清了生活的真相之后，依然热爱生活。</h2>
                    </div>
                </div>
            </div>
        </div>
        <p className={styles.title}>评论</p>
        <MsgBoards changePage={changePage} msgboardList={getCommentList} msgboardNum={CommentNum} />
        <p className={styles.tjyd}>推荐阅读</p>
        <InfiniteList recommend={recommend} />
    </div>
}

export default About;