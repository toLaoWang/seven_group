import { msgboardItem } from '@/types/msgboard';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'umi';
import styles from './msgboard.less';
import Msgboards from '@/components/msgboards'
import News from '@/components/news'
const MsgBoard: React.FC = (props) => {
    let [page, setPage] = useState(1);
    const dispatch = useDispatch();
    const { msgboardList, msgboardNum } = useSelector((state: msgboardItem) => state.msgboard);

    useEffect(() => {
        dispatch({
            type: 'msgboard/getMsgBoardList',
            payload: page
        })
    }, [page])
    function changePage(page: number) {
        setPage(page)
    }


    return <div className={styles.MsgBoard}>
        <div className={styles.container}>
            <div className={styles.container_cen}>
                <h2>留言板</h2>
                <p><strong>[ 请勿灌水 🤖 ]</strong></p>
            </div>
        </div>
        <div className={styles.cenBox}>
            <div className={styles.topBox}>
                <p className={styles.commentBox}>评论</p>
                <Msgboards msgboardList={msgboardList} msgboardNum={msgboardNum} changePage={changePage} />
            </div>
            <div className={styles._3ZUDOCREZiU7HtQwpEsqZZ}>
                <p className={styles._1hXJ8FkJwAyuM2}>
                    推荐阅读
                </p>
                <div className={styles.ammbB8tpRTlzwGu9Q8GNE}>
                    <News />
                </div>
            </div>
        </div>
    </div >
}
export default MsgBoard;