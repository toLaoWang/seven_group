import { IRootArchivesState } from '@/types/model';
import React, { useEffect, useState } from 'react';
import { RouteComponentProps, useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'umi';
import classNames from 'classnames';
import style from './archives.less';
import Push from '@/components/push';
import Msgboards from '@/components/msgboards';
import { fomatTime } from '@/utils';
import HighLight from '@/components/highlight';
import ImageView from '@/components/imageView';
import Level from '@/components/level';
import Like from '@/components/like';
import { Modal, Button } from 'antd'
import ReactDOM from 'react-dom';

const archives: React.FC<RouteComponentProps<{ id: string }>> = (props) => {
  const dom = React.createRef<HTMLDivElement>();
  const dispatch = useDispatch();
  const history = useHistory()
  useEffect(() => {
    // window._hmt.push(['ClickEvent', "首页", "点击列表", "id", props.match.params.id]);
    dispatch({
      type: 'archives/getArchivesDetail',
      payload: props.match.params.id,
    });
  }, []);
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(6);
  useEffect(() => {
    dispatch({
      type: 'archives/getCommentList',
      payload: {
        id: props.match.params.id,
        page,
        pageSize,
      },
    });
    React.createRef<HTMLDivElement>();
  }, []);
  const { archivesIObj, commentList, num } = useSelector(
    (state: IRootArchivesState) => state.archives,
  );
  function changePage(page: number) {
    setPage(page);
  }
  console.log(archivesIObj);
  const [visible, setVisible] = React.useState(false);

  // const showModal = () => {
  //   setVisible(true);
  // };
  // 支付
  const handleOk = async () => {

    let result = await fetch('http://127.0.0.1:7001/pay', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ totalAmount: archivesIObj.totalAmount, id: archivesIObj.id })
    }).then(res => res.json());
    if (result.url) {
      // window.location.href = result.url;
      window.open(result.url)
    }

  };

  const handleCancel = () => {
    history.push('/')
  };


  return (
    <div className={classNames(style.container)}>
      <div id='share-Pay'>
        <Modal
          title="确认以下收费信息"
          width='400px'
          visible={archivesIObj.totalAmount}
          onOk={handleOk}
          onCancel={handleCancel}
          footer={
            <p>
              <Button onClick={() => handleCancel()}>取消</Button>
              <Button type="primary" danger onClick={() => handleOk()}>
                立即支付
              </Button>
            </p>
          }
        >
          <p style={{ textAlign: 'center' }}>支付金额：￥{archivesIObj.totalAmount}</p>
        </Modal>
      </div>
      <div className={classNames(style.left)}>
        <div className={classNames(style.box)}>
          <div className={style.Like}>
            <Like item={archivesIObj} id={props.match.params.id!} num={archivesIObj.likes!} lei={'article'} cover={archivesIObj.cover} summary={archivesIObj.summary} url={`/article/${archivesIObj.id}`} title={archivesIObj.title!} />
          </div>

          <ImageView>
            {archivesIObj.cover && (
              <div>
                <img src={archivesIObj.cover} alt="" />
              </div>
            )}
          </ImageView>

          <div className={classNames(style.top)}>
            <h1>{archivesIObj.title}</h1>
            <p>
              {archivesIObj.publishAt && (
                <span>发布于{fomatTime(archivesIObj.publishAt!)}</span>
              )}
              <span> • </span>
              {archivesIObj.views && <span>阅读量 {archivesIObj.views}</span>}
            </p>
          </div>
          <HighLight>
            <div
              ref={dom}
              dangerouslySetInnerHTML={{ __html: archivesIObj.html! }}
            ></div>
          </HighLight>
          <div className={classNames(style._1F8nZG_3e_sxtkzObh163H)}>
            <div className={classNames(style._2qMEw2QUMeyQzgM1G1J4P)}>
              发布于<span>{archivesIObj.publishAt}</span>|版权信息{' '}
              <span>非商用-署名-自由转载</span>
              发布于<span>{fomatTime(archivesIObj.publishAt!)}</span> |
              版权信息:{' '}
              <span style={{ cursor: 'pointer' }}>非商用-署名-自由转载</span>
            </div>
          </div>
        </div>
        <div className={style.cenBox}>
          <div className={style.topBox}>
            <p className={style.commentBox}>评论</p>
            <Msgboards
              msgboardList={commentList}
              msgboardNum={num}
              changePage={changePage}
            />
          </div>
        </div>
      </div>
      <div className={classNames(style.right)}>
        <div className={classNames(style.sticky)}>
          <Push></Push>
          <Level archivesIObj={archivesIObj} dom={dom} />
        </div>
      </div>
    </div>
  );
};

export default archives;
