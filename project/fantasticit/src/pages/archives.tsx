import React, { ReactElement } from 'react';
import Classify from '@/components/classify';
import Push from '@/components/push';
import { IRootArchivesState, July2 } from '@/types';

import moment from 'moment';
import { useEffect } from 'react';
import { NavLink, RouteComponentProps } from 'react-router-dom';
import { useDispatch, useSelector } from 'umi';
import style from './archives.less';

const Archives: React.FC<RouteComponentProps> = (props): ReactElement => {
  const classNames = require('classnames');

  const dispatch = useDispatch();
  const { archivesObj } = useSelector(
    (state: IRootArchivesState) => state.archives,
  );

  useEffect(() => {
    dispatch({
      type: 'archives/getArchivesList',
    });
  }, []);

  return (
    <div className={classNames(style.container)}>
      <div className={classNames(style.box)}>
        <div className={classNames(style.left)}>
          <div className={classNames(style.leftTop)}>
            <p>
              <span>归档</span>
            </p>
            <p>
              共计
              <span>
                {Object.keys(archivesObj).length > 0
                  ? Object.keys(archivesObj)
                      .map((item) =>
                        Object.keys(archivesObj[item])
                          .map((item2) => archivesObj[item][item2])
                          .reduce((p, v) => (p += v.length), 0),
                      )
                      .reduce((p, v) => p + v)
                  : 0}
              </span>
              篇
            </p>
            <p></p>
          </div>
          <div className={classNames(style.leftBottom)}>
            {Object.keys(archivesObj)
              .sort((a, b) => {
                return Number(b) - Number(a);
              })
              .map((item, index) => {
                return (
                  <div className={classNames(style.leftContent)} key={index}>
                    <h2>{item}</h2>
                    {Object.keys(archivesObj[item]).map((item2, index2) => {
                      return (
                        <div key={index2}>
                          <h3>{item2}</h3>
                          <ul>
                            {Object.values(
                              archivesObj[item][item2] as July2,
                            ).map((item3) => {
                              return (
                                <li key={item3.id}>
                                  <NavLink to={`/archives/${item3.id}`}>
                                    <span>
                                      {moment(item3.createAt).format('MM-DD')}
                                    </span>
                                    <span>{item3.title}</span>
                                  </NavLink>
                                </li>
                              );
                            })}
                          </ul>
                        </div>
                      );
                    })}
                  </div>
                );
              })}
          </div>
        </div>
        <div className={classNames(style.right)}>
          <div className={classNames(style.sticky)}>
            <Push></Push>
            <Classify></Classify>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Archives;
