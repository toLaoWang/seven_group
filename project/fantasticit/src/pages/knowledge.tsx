import styles from './knowledge.less';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'umi';
import { IRootState } from '@/types';
import Push from '../components/push'
import KnowledgeItem from "@/components/knowledgeItem";
import { RouteComponentProps } from 'react-router-dom';
import Classify from '@/components/classify';
import InfiniteScroll from 'react-infinite-scroll-component';
const KnowLedge: React.FC<RouteComponentProps> = (props) => {
    let [page, setPage] = useState(1);
    const dispatch = useDispatch()
    const { knowledge, knowledgeCount } = useSelector((state: IRootState) => state.knowledge);

    useEffect(() => {
        dispatch({
            type: 'knowledge/getKnowledge',
            payload: page
        })
    }, [page])

    function pullupLoader() {
        setPage(page => page + 1)
    }

    const goKnowledgeDetail = (id: string) => {
        props.history.push(`/knowledge/${id}`)
    }

    return <div className={styles.knowledge}>
        <div className={styles.knowledge_left}>
            <InfiniteScroll
                hasMore={knowledgeCount > page * 12}
                loader={<h4>Loading...</h4>}
                dataLength={knowledge.length}
                next={pullupLoader}
            >
                {
                    knowledge.length ? knowledge.map(item => {
                        return <KnowledgeItem key={item.id} item={item} goKnowledgeDetail={goKnowledgeDetail} />
                    }) : null
                }
            </InfiniteScroll>
        </div>
        <div className={styles.knowledge_right}>
            <div className={styles.sticky}>
                <Push></Push>
                <Classify></Classify>
            </div>
        </div>
    </div>
}

export default KnowLedge;