import Backtop from '@/components/backTop';
import Label from '@/components/label';
import Classify from '@/components/classify';
import Push from '@/components/push';
import { IRootState, IArticleItem } from '@/types';
import { useEffect, useState } from 'react';
import { RouteComponentProps } from 'react-router-dom'
import { NavLink, useDispatch, useSelector } from 'umi';
import styles from './index.less'
import New from '@/components/infiniteList';
const Tag: React.FC<RouteComponentProps<{ id: string }, {}, { qian: string }>> = (props) => {
    let { id } = props.match.params;
    const { tagDetail, tagTotal, tags } = useSelector((state: IRootState) => state.article)
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch({
            type: 'article/getTagDeta',
            payload: id
        })
    }, [id])
    return <div id={styles._index}>
        <div className={styles.content}>
            <div className={styles.contentLeft}>
                <div className={styles.banner}>
                    <div className={styles.logo}>
                        <p>与 <span>{tagDetail[0] && tagDetail[0].tags![0].label}</span> 标签有关的文章</p>
                        <p>共搜索到 <span>{tagTotal}</span> 篇</p>
                    </div>
                </div>
                <div className={styles.tag}>
                    <div>
                        <span>文章标签</span>
                    </div>
                    <ul>
                        {
                            tags.map(item => <li key={item.id}><NavLink activeStyle={{color:'var(--primary-color)'}} to={`/tag/${item.value}`}>{item.label}[{item.articleCount}]</NavLink></li>)
                        }
                    </ul>
                </div>
                <div className={styles.list}>
                    <div className={styles.newList}>
                        <New recommend={tagDetail} />
                    </div>
                </div>
            </div>
            <div className={styles.contentRight}>
                <Push></Push>
                <Classify/>
            </div>
        </div>
        <Backtop />
    </div>
}
export default Tag

