import styles from "./detail.less";
import { RouteComponentProps } from "react-router-dom"
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'umi';
import { IRootState } from "@/types/model";
import Like from "@/components/like";
import { fomatTime } from "@/utils";
import Level from '@/components/level';
import React from "react";
import Msgboards from '@/components/msgboards';
import HighLight from "@/components/highlight";

interface Id {
    id: string,
    endId: string
}

const Detail: React.FC<RouteComponentProps<Id>> = (props) => {
    const dispatch = useDispatch();
    const { knowledgeDetail, detailList, knowLedgemsgList, knowLedgemsgNum } = useSelector((state: IRootState) => state.knowledge);
    const [Listind, setListInd] = useState(0);
    const dom = React.createRef<HTMLDivElement>();
    const [page, setPage] = useState(1);
    const [pageSize, setPageSize] = useState(6);
    const { id, endId } = props.match.params;

    useEffect(() => {
        dispatch({
            type: 'knowledge/getViews',
            payload: endId
        });
    }, [endId]);

    useEffect(() => {
        dispatch({
            type: 'knowledge/getKnowledgeDetail',
            payload: id
        });
    }, []);

    useEffect(() => {
        const { id, endId } = props.match.params;
        dispatch({
            type: 'knowledge/getkonwLedgeMsg',
            payload: {
                id: endId,
                page,
                pageSize
            }
        })
    }, [page])

    function goDetail(endId: string, index: number) {
        props.history.push(`/knowledge/${props.match.params.id}/${endId}`);
        setListInd(index);
    };
    
    //改编页码数
    function changePage(page: number) {
        setPage(page);
    }

    const ind: number | undefined = knowledgeDetail?.children && knowledgeDetail?.children.findIndex(item => item.id === props.match.params.endId);
    const ListLength: number | undefined = knowledgeDetail?.children && knowledgeDetail?.children.length - 1;

    return (
        <div className={styles.detail}>
            <div className={styles.detailHeader}>
                <span onClick={() => props.history.push("/knowledge")}>知识小册</span>/
                <span onClick={() => props.history.push(`/knowledge/${props.match.params.id}`)}>{knowledgeDetail.title}</span>/<span>{detailList.title}</span>
            </div>
            <div className={styles.detailContent}>

                <div className={styles.detailLeft}>
                    {detailList ? <div className={styles.bg}>
                        <div className={styles._1sHvTwlFk1qI5GYNQQYAOA}>
                            <h1 className={styles._1gyFGCKvmuw_FCv8gpxCM}>{detailList && detailList.title}</h1>
                            <p className={styles._3kL5VVYs3ZqoG6F75pb9FZ}>
                                <span>发布于{detailList && detailList.title}<span>
                                </span>{detailList && fomatTime(detailList.createAt!)}</span>
                                <span> • </span>
                                <span>阅读量 {detailList && detailList.views}</span>
                            </p>
                        </div>
                        {
                            <HighLight>
                                <div className={styles.markdown} ref={dom} dangerouslySetInnerHTML={{ __html: detailList.html! }} ></div>
                            </HighLight>
                        }
                        <div className={styles.WsHfV3fGKFel9niUnWSq3}>
                            发布于{detailList && fomatTime(detailList.createAt!)} | 版权信息：<a href="https://creativecommons.org/licenses/by-nc/3.0/cn/deed.zh" target="_blank" rel="noreferrer">非商用-署名-自由转载</a>
                        </div>

                        <div className={styles.oHrkPFOCJ0FoSxx3WN}>
                            {
                                ind === 0 && <div className={styles._1tv35f3f1_0IbnKHej4MDB}
                                    style={{ width: "100%" }}>
                                    <a href={`/knowledge/${knowledgeDetail.children! && knowledgeDetail.children![ind].parentId}/${knowledgeDetail.children! && knowledgeDetail.children![ind + 1].id}`}
                                        style={{ justifyContent: "flex-end" }}>
                                        <span>{knowledgeDetail.children! && knowledgeDetail.children![ind! + 1].title}</span><span role="img" aria-label="right" className="anticon anticon-right">
                                            <svg viewBox="64 64 896 896" focusable="false" data-icon="right" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M765.7 486.8L314.9 134.7A7.97 7.97 0 00302 141v77.3c0 4.9 2.3 9.6 6.1 12.6l360 281.1-360 281.1c-3.9 3-6.1 7.7-6.1 12.6V883c0 6.7 7.7 10.4 12.9 6.3l450.8-352.1a31.96 31.96 0 000-50.4z"></path></svg>
                                        </span>
                                    </a>
                                </div>
                            }
                            {
                                ind === ListLength && <div className={styles._1tv35f3f1_0IbnKHej4MDB}
                                    style={{ width: "100%" }}>
                                    <a href={`/knowledge/${knowledgeDetail.children! && knowledgeDetail.children![ind!].parentId}/${knowledgeDetail.children! && knowledgeDetail.children![ind! - 1].id}`}>
                                        <svg viewBox="64 64 896 896" focusable="false" data-icon="left" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M724 218.3V141c0-6.7-7.7-10.4-12.9-6.3L260.3 486.8a31.86 31.86 0 000 50.3l450.8 352.1c5.3 4.1 12.9.4 12.9-6.3v-77.3c0-4.9-2.3-9.6-6.1-12.6l-360-281 360-281.1c3.8-3 6.1-7.7 6.1-12.6z"></path></svg>
                                        <span>{knowledgeDetail.children! && knowledgeDetail.children![(ind as number) - 1].title}</span><span role="img" aria-label="right" className="anticon anticon-right">
                                        </span>
                                    </a>
                                </div>
                            }
                            {ind !== ListLength && ind !== 0 && <>
                                <div className={styles._1tv35f3f1_0IbnKHej4MDB}
                                    style={{ width: "45%" }}>
                                    <a href={`/knowledge/${knowledgeDetail.children! && knowledgeDetail.children![ind!].parentId}/${knowledgeDetail.children! && knowledgeDetail.children![ind! - 1].id}`}>
                                        <svg viewBox="64 64 896 896" focusable="false" data-icon="left" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M724 218.3V141c0-6.7-7.7-10.4-12.9-6.3L260.3 486.8a31.86 31.86 0 000 50.3l450.8 352.1c5.3 4.1 12.9.4 12.9-6.3v-77.3c0-4.9-2.3-9.6-6.1-12.6l-360-281 360-281.1c3.8-3 6.1-7.7 6.1-12.6z"></path></svg>
                                        <span>{knowledgeDetail.children! && knowledgeDetail.children![(ind as number) - 1].title}</span><span role="img" aria-label="right" className="anticon anticon-right">
                                        </span>
                                    </a>
                                </div>
                                <div className={styles._1tv35f3f1_0IbnKHej4MDB}
                                    style={{ width: "45%" }}>
                                    <a href={`/knowledge/${knowledgeDetail.children! && knowledgeDetail.children![ind!].parentId}/${knowledgeDetail.children! && knowledgeDetail.children![ind! + 1].id}`}
                                        style={{ justifyContent: "flex-end" }}>
                                        <span>{knowledgeDetail.children! && knowledgeDetail.children![(ind as number) + 1].title}</span><span role="img" aria-label="right" className="anticon anticon-right">
                                        </span>
                                        <svg viewBox="64 64 896 896" focusable="false" data-icon="right" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M765.7 486.8L314.9 134.7A7.97 7.97 0 00302 141v77.3c0 4.9 2.3 9.6 6.1 12.6l360 281.1-360 281.1c-3.9 3-6.1 7.7-6.1 12.6V883c0 6.7 7.7 10.4 12.9 6.3l450.8-352.1a31.96 31.96 0 000-50.4z"></path></svg>
                                    </a>
                                </div>
                            </>
                            }
                        </div>
                    </div> : null}
                    <p className={styles.comment}>评论</p>
                    <Msgboards msgboardList={knowLedgemsgList} msgboardNum={knowLedgemsgNum} changePage={changePage} />
                </div>

                <div className={styles.detailRight}>
                    <div className={styles.sticky}>
                        <ul className={styles.titleBox}>
                            <h3>{knowledgeDetail && knowledgeDetail.title}</h3>
                            <div className={styles.title}>
                                {
                                    knowledgeDetail.children! && knowledgeDetail.children.map((item, index) => {
                                        return <li key={item.id} onClick={() => goDetail(item.id, index)}
                                            className={props.match.params.endId === item.id ? styles.active : ""}
                                        >{item.title}</li>
                                    })
                                }
                            </div>
                        </ul>
                        <Level archivesIObj={detailList} dom={dom} />
                    </div>

                </div>
            </div>

            <div className={styles.Like}>
                <Like id={props.match.params.endId} num={detailList.likes!} lei={"knowledge"} cover={knowledgeDetail.cover!} summary={knowledgeDetail.summary!} title={knowledgeDetail.title!} item={knowledgeDetail} url={`/knowledge/${knowledgeDetail.id}`} />
            </div>

        </div>
    )
}
export default Detail;
