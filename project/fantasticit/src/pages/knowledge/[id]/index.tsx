import styles from './index.less';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'umi';
import { IRootState } from '@/types';
import { RouteComponentProps } from "react-router-dom"
import moment from 'moment';
import { fomatTime } from "@/utils";
import share from '@/components/share';
moment.locale('zh-cn');
interface Id {
    id: string
}
const KnowledgeDetail: React.FC<RouteComponentProps<Id>> = (props) => {
    const dispatch = useDispatch()
    const { knowledgeDetail, knowledge } = useSelector((state: IRootState) => state.knowledge);
    useEffect(() => {
        dispatch({
            type: 'knowledge/getKnowledge',
            payload: 1
        })
    }, [])
    useEffect(() => {
        dispatch({
            type: 'knowledge/getKnowledgeDetail',
            payload: props.match.params.id
        })
    }, [props.match.params.id])

    const goKnowledgeDetail = (id: string) => {
        props.history.push(`/knowledge/${id}`)
        dispatch({
            type: 'knowledge/getKnowledgeDetail',
            payload: id
        })
    }

    function goKnowledgeList(prevId: string, endId: String) {
        props.history.push(`/knowledge/${prevId}/${endId}`)
    }

    return <div className={styles.KnowledgeDetailBox}>
        <div className={styles.KnowledgeDetailBg} style={{ backgroundImage: `url(${knowledgeDetail.cover})` }}></div>

        <div className={styles.KnowledgeDetail}>
            <div className={styles.KnowledgeDetailHeader}> <span onClick={() => props.history.push("/knowledge")}>知识小册</span>/<span>{knowledgeDetail.title}</span> </div>
            <div className={styles.KnowledgeDetailContent}>
                <div className={styles.KnowledgeDetailContentLeft}>
                    <div className={styles.header}><span>{knowledgeDetail.title}</span></div>
                    <div className={styles.left}>
                        <div className={styles.detailPic}><img src={knowledgeDetail.cover} alt="" /></div>
                        <h2>{knowledgeDetail.title}</h2>
                         <p>{knowledgeDetail.summary}</p>
                        <p>
                            <span>{knowledgeDetail.views ? knowledgeDetail.views + '次阅读' : null}</span> · <span>{knowledgeDetail.views ? moment(knowledgeDetail.createAt).format('lll').replace('年', "-").replace('月', "-").replace('日', "") : null}</span>
                        </p>
                        <p>{knowledgeDetail.views ? <button onClick={() => goKnowledgeList(knowledgeDetail.id!, knowledgeDetail.children![0].id)}>开始阅读</button> : null}</p>
                        {
                            knowledgeDetail?.children && knowledgeDetail.children.map(item => {
                                return <div className={styles.title}
                                    key={item.id}
                                    onClick={() => goKnowledgeList(knowledgeDetail.id!, item.id)}
                                >
                                    <span>{item.title}</span>
                                    <span>{fomatTime(knowledgeDetail.createAt!)}</span></div>
                            })
                        }
                    </div>
                </div>
                <div className={styles.KnowledgeDetailContentRight}>
                    <div className={styles.sticky}>
                        <div className={styles.header}>其他知识笔记</div>
                        <div className={styles.right}>
                            {knowledge.length ? knowledge.filter(item => item.id !== knowledgeDetail.id).map((item, index) => {
                                return <div key={item.id} className={styles.content}>
                                    <div className={styles.XifRPckEIhR} onClick={() => { goKnowledgeDetail(item.id) }}>
                                        <div className={styles.knowledge_header}>
                                            <span className={styles.knowledge_header_title}>{item.title}</span>
                                            <span>{moment(item.createAt).startOf('day').fromNow()}</span></div>
                                        <div className={styles.knowledge_main}>
                                            <div className={styles.knowledge_pic}> <img src={item.cover} alt="" /></div>
                                            <div className={styles.knowledge_content}>
                                                <p>{item.summary}</p>
                                                <p>
                                                    <span>
                                                        <i> <svg viewBox="64 64 896 896" focusable="false" data-icon="eye" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M942.2 486.2C847.4 286.5 704.1 186 512 186c-192.2 0-335.4 100.5-430.2 300.3a60.3 60.3 0 000 51.5C176.6 737.5 319.9 838 512 838c192.2 0 335.4-100.5 430.2-300.3 7.7-16.2 7.7-35 0-51.5zM512 766c-161.3 0-279.4-81.8-362.7-254C232.6 339.8 350.7 258 512 258c161.3 0 279.4 81.8 362.7 254C791.5 684.2 673.4 766 512 766zm-4-430c-97.2 0-176 78.8-176 176s78.8 176 176 176 176-78.8 176-176-78.8-176-176-176zm0 288c-61.9 0-112-50.1-112-112s50.1-112 112-112 112 50.1 112 112-50.1 112-112 112z"></path></svg></i>
                                                        {item.views}
                                                    </span>
                                                    <span onClick={(e) => {
                                                        // showModal()
                                                        e.stopPropagation();
                                                        // share(item, 'knowledge')
                                                        share(item)
                                                    }}>
                                                        <i> <svg viewBox="64 64 896 896" focusable="false" data-icon="share-alt" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M752 664c-28.5 0-54.8 10-75.4 26.7L469.4 540.8a160.68 160.68 0 000-57.6l207.2-149.9C697.2 350 723.5 360 752 360c66.2 0 120-53.8 120-120s-53.8-120-120-120-120 53.8-120 120c0 11.6 1.6 22.7 4.7 33.3L439.9 415.8C410.7 377.1 364.3 352 312 352c-88.4 0-160 71.6-160 160s71.6 160 160 160c52.3 0 98.7-25.1 127.9-63.8l196.8 142.5c-3.1 10.6-4.7 21.8-4.7 33.3 0 66.2 53.8 120 120 120s120-53.8 120-120-53.8-120-120-120zm0-476c28.7 0 52 23.3 52 52s-23.3 52-52 52-52-23.3-52-52 23.3-52 52-52zM312 600c-48.5 0-88-39.5-88-88s39.5-88 88-88 88 39.5 88 88-39.5 88-88 88zm440 236c-28.7 0-52-23.3-52-52s23.3-52 52-52 52 23.3 52 52-23.3 52-52 52z"></path></svg>
                                                        </i>
                                                        分享
                                                    </span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className={styles.hr_border}><div className={styles.hr}></div></div>
                                </div>
                            }) : null
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
}

export default KnowledgeDetail;