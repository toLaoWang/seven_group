import Backtop from '@/components/backTop';
import Label from '@/components/label';
import Nav from '@/components/nav';
import Push from '@/components/push';
import { IRootState } from '@/types';
import { useEffect, useState } from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
import { RouteComponentProps } from 'react-router-dom'
import { useDispatch, useSelector } from 'umi';
import styles from './index.less'
import New from '@/components/infiniteList';
const categoryClass: React.FC<RouteComponentProps<{ id: string }>> = (props) => {
  const dispatch = useDispatch()
  const [page, setPage] = useState(1)
  const { categoryList, categoryTotal } = useSelector((state: IRootState) => state.article)
  let { id } = props.match.params;
  useEffect(() => {
    setPage(1)
  }, [props.match.params.id])
  useEffect(() => {
    dispatch({
      type: 'article/getCategoryList',
      payload: { type: id, page }
    })
  }, [page, props.match.params.id])
  const fetchMoreData = () => {
    setPage(page + 1)
  }
  return <div id={styles._index}>
    <div className={styles.content}>
      <div className={styles.contentLeft}>
        <div className={styles.banner}>
          <div className={styles.logo}>
            <p>
              <span>{categoryList[0] && categoryList[0].category?.label}</span> 分类文章</p>
            <p>共搜索到 <span>{categoryTotal}</span> 篇</p>
          </div>
        </div>
        <div className={styles.list}>
          <Nav />
          <InfiniteScroll
            dataLength={categoryList.length}
            next={fetchMoreData}
            hasMore={categoryTotal > page * 12}
            loader={<h4>Loading...</h4>}
          >
            <div className={styles.newList}>
              <New recommend={categoryList} />
            </div>
          </InfiniteScroll>
        </div>
      </div>
      <div className={styles.contentRight}>
        <Push></Push>
        <Label></Label>
      </div>
    </div>
    <Backtop />
  </div>
}
export default categoryClass