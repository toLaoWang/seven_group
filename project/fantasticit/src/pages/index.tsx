import { IRootState } from '@/types';
import { Affix } from 'antd'
import { useEffect, useState, FC } from 'react';
import { useDispatch, useSelector } from 'umi';
import Swiper from '../components/swiper';
import Label from '../components/label';
import Push from '../components/push';
import styles from "./index.less"
import { RouterProps } from 'react-router-dom'
import InfiniteScroll from 'react-infinite-scroll-component';
import New from "@/components/infiniteList"
import Nav from '@/components/nav'
export default function IndexPage(props: RouterProps) {
  const dispatch = useDispatch()
  const { articleList, total } = useSelector((state: IRootState) => state.article);
  const [page, setPage] = useState(1)
  const [top, setTop] = useState(10)
  useEffect(() => {
    dispatch({
      type: "article/getArticleList",
      payload: page
    })
  }, [page])
  function fetchMoreData() {
    setPage(page + 1)
  }
  return (
    <div id={styles._index}>
      <div className={styles.content}>
        <div className={styles.contentLeft}>
          <div className={styles.banner}>
            <Swiper />
          </div>
          <div className={styles.list}>
            <Nav />
            <InfiniteScroll
              dataLength={articleList.length}
              next={fetchMoreData}
              hasMore={total > page * 12}
              loader={<h4>Loading...</h4>}
            >
              <div className={styles.newList}>
                <New recommend={articleList} />
              </div>
            </InfiniteScroll>
          </div>
        </div>
        <div className={styles.contentRight}>
          <Affix offsetTop={top}>
            <div className={styles.sticky}>
              <Push></Push>
              <Label></Label>
            </div>
          </Affix>
        </div>
      </div>
    </div>
  );
}
