import { getArchivesDetail, getArchivesList, getCommentList } from '@/services';
import { RootObject, RootsObject } from '@/types';
import { msgboardItem } from '@/types/msgboard';
import { Effect, Reducer } from 'umi';

export interface ArchivesModelState {
  archivesObj: RootObject;
  archivesIObj: RootsObject;
  commentList: msgboardItem[];
  num: number;
}

export interface ArchivesModelType {
  namespace: 'archives';
  state: ArchivesModelState;
  effects: {
    getArchivesList: Effect;
    getArchivesDetail: Effect;
    getCommentList: Effect;
  };
  reducers: {
    save: Reducer<ArchivesModelState>;
  };
}

const IndexModel: ArchivesModelType = {
  namespace: 'archives',

  state: {
    archivesObj: ({} as RootObject),
    archivesIObj: {},
    commentList: [],
    num: 0,
  },

  effects: {
    *getArchivesList({ payload }, { call, put }) {
      let result = yield call(getArchivesList);
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { archivesObj: result.data },
        });
      }
    },
    *getArchivesDetail({ payload }, { call, put }) {
      let result = yield call(getArchivesDetail, payload);

      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { archivesIObj: result.data },
        });
      }
    },
    *getCommentList({ payload }, { call, put }) {
      let result = yield call(getCommentList, payload);
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { commentList: result.data[0], num: result.data[1] },
        });
      }
    },
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
};

export default IndexModel;
