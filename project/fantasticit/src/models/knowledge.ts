import { getKnowledge, getKnowledgeDetail, getkonwLedgeMsg, getViews } from '@/services';
import { IRootState, KnowledgeObject } from "@/types";
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import { IDetailListObject, IKnowledgeDetailObject } from "@/types/knowledge"
import { msgboardItem } from '@/types/msgboard';
export interface KnowledgeState {
    [x: string]: any;
    knowledge: Array<KnowledgeObject>;
    knowledgeDetail: Partial<IKnowledgeDetailObject>;
    knowledgeCount: number;
    detailList: Partial<IDetailListObject>;
    knowLedgemsgList: msgboardItem[];
    knowLedgemsgNum: number;
}
export interface KnowledgeModelType {
    namespace: 'knowledge';
    state: KnowledgeState;
    effects: {
        getKnowledge: Effect;
        getKnowledgeDetail: Effect;
        getViews: Effect;
        getkonwLedgeMsg: Effect;
    };
    reducers: {
        save: Reducer<KnowledgeState>;
    };
}
const IndexModel: KnowledgeModelType = {
    namespace: 'knowledge',
    state: {
        knowledge: [],
        knowledgeCount: 0,
        knowledgeDetail: {},
        detailList: {},
        knowLedgemsgList: [],
        knowLedgemsgNum: 0
    },
    effects: {
        *getKnowledge({ payload }, { call, put, select }) {
            let knowledge = yield select((state: IRootState) => state.knowledge.knowledge);
            let result = yield call(getKnowledge, payload);
            knowledge = payload == 1 ? result.data[0] : [...knowledge, ...result.data[0]];
            if (result.statusCode === 200) {
                yield put({
                    type: 'save',
                    payload: { knowledge, knowledgeCount: result.data[1] }
                })
            }
        },
        *getKnowledgeDetail({ payload }, { call, put }) {
            const result = yield call(getKnowledgeDetail, payload);
            if (result.statusCode === 200) {
                yield put({
                    type: 'save',
                    payload: { knowledgeDetail: result.data }
                })
            }
        },

        *getViews({ payload }, { call, put }) {
            const result = yield call(getViews, payload);
            console.log(result);
            if (result.statusCode === 200) {
                yield put({
                    type: 'save',
                    payload: { detailList: result.data }
                })
            }
        },

        *getkonwLedgeMsg({ payload }, { call, put }) {
            let result = yield call(getkonwLedgeMsg, payload);
            if (result.statusCode === 200) {
                yield put({
                    type: 'save',
                    payload: {
                        knowLedgemsgList: result.data[0],
                        knowLedgemsgNum: result.data[1]
                    }
                })
            }
        },

    },
    reducers: {
        save(state, action) {
            // console.log();
            return {
                ...state,
                ...action.payload,
            };
        }
    }
};
export default IndexModel;