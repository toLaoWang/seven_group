import { getMsgBoardList, sendChildrenMsg, sendMsgBoard } from '@/services';
import { msgboardItem } from '@/types/msgboard';
import { sendmsgboard } from '@/types/sendmsgboard';
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import { message } from 'antd';
export interface MsgBoardModelState {
    msgboardList: msgboardItem[],
    msgboardNum: number,
    sendmsgboard: sendmsgboard | null
}
export interface MsgBoardModelType {
    namespace: 'msgboard';
    state: MsgBoardModelState;
    effects: {
        getMsgBoardList: Effect;
        sendMsgBoard: Effect;
        sendChildrenMsg: Effect;
    };
    reducers: {
        save: Reducer<MsgBoardModelState>;
    };
}

const IndexModel: MsgBoardModelType = {
    namespace: 'msgboard',

    state: {
        msgboardList: [],
        msgboardNum: 0,
        sendmsgboard: null,
    },

    effects: {
        *getMsgBoardList({ payload }, { call, put }) {
            let result = yield call(getMsgBoardList, payload);
            if (result.statusCode === 200) {
                yield put({
                    type: 'save',
                    payload: {
                        msgboardList: result.data[0],
                        msgboardNum: result.data[1]
                    }
                })
            }
        },
        *sendMsgBoard({ payload }, { call, put }) {
            let result = yield call(sendMsgBoard, payload)
            if (result.statusCode === 201) {
                message.success('评论成功，已提交审核');
            }
        },
        *sendChildrenMsg({ payload }, { call, put }) {
            let result = yield call(sendChildrenMsg, payload)
            if (result.statusCode === 201) {
                message.success('评论成功，已提交审核');
            }
        }
    },
    reducers: {
        save(state, action) {
            return {
                ...state,
                ...action.payload,
            };
        },
    }
};

export default IndexModel;