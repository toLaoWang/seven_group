import { getRecommend, getCategory, getTag, getArticleList, getArticleDetail, getRecommends, getTagDeta, getSearch, getLike, getCategoryList } from '@/services';
import { IArticleItem, ISearchList, ITagDetail } from '@/types/article';
import { ICategory } from '@/types/category'
import { ITag } from '@/types/tag'
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import { IRootState, ArticleDetail } from "@/types"
export interface ArticleModelState {
  recommend: IArticleItem[];
  recommends: IArticleItem[];
  category: ICategory[],
  tags: ITag[]
  articleList: IArticleItem[];
  total: number;
  articleDetail: null | ArticleDetail,
  tagDetail: Array<IArticleItem>;
  categoryList: IArticleItem[];
  categoryTotal: number,
  tagTotal: number
  searchList: ISearchList[];
  likeState: IArticleItem | null;
}
export interface ArticleModelType {
  namespace: 'article';
  state: ArticleModelState;
  effects: {
    getRecommend: Effect;
    getRecommends: Effect;
    getCategory: Effect;
    getTag: Effect;
    getArticleList: Effect;
    getArticleDetail: Effect;
    getTagDeta: Effect;
    getCategoryList: Effect;
    getSearch: Effect;
    getLike: Effect;
  };
  reducers: {
    save: Reducer<ArticleModelState>;
  };
}
const IndexModel: ArticleModelType = {
  namespace: 'article',
  state: {
    recommend: [],
    recommends: [],
    category: [],
    tags: [],
    articleList: [],
    total: -1,
    articleDetail: null,
    tagDetail: [],
    tagTotal: -1,
    categoryList: [],
    categoryTotal: -1,
    searchList: [],
    likeState: null
  },
  effects: {
    *getRecommend({ payload }, { call, put }) {
      let result = yield call(getRecommend);
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { recommend: result.data }
        })
      }
    },
    *getRecommends({ payload }, { call, put }) {
      let result = yield call(getRecommends, payload);
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { recommends: result.data.splice(0, 6) }
        })
      }
    },
    *getCategory({ payload }, { call, put }) {
      let result = yield call(getCategory);
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { category: result.data }
        })
      }
    },
    // 标签
    *getTag({ payload }, { call, put }) {
      let result = yield call(getTag);
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { tags: result.data }
        })
      }
    },
    *getArticleList({ payload }, { call, put, select }) {
      let articleList = yield select((state: IRootState) => state.article.articleList)
      let result = yield call(getArticleList, payload)
      articleList = payload == 1 ? result.data[0] : [...articleList, ...result.data[0]]
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { articleList, total: result.data[1] }
        })
      }
    },
    *getArticleDetail({ payload }, { call, put }) {
      let result = yield call(getArticleDetail, payload)
      if (result.statusCode === 200) {
        console.log(result);
        yield put({
          type: "save",
          payload: { articleDetail: result.data }
        })
      }
    },
    // 标签详情页
    *getTagDeta({ payload }, { call, put }) {
      let result = yield call(getTagDeta, payload)
      if (result.statusCode === 200) {
        yield put({
          type: "save",
          payload: { tagDetail: result.data[0], tagTotal: result.data[1] }
        })
      }
    },
    *getCategoryList({ payload, type }, { call, put, select }) {
      let categoryList = yield select((state: IRootState) => state.article.categoryList)
      let result = yield call(getCategoryList, payload.type, payload.page)
      categoryList = [...categoryList, ...result.data[0]].length > result.data[1] ? result.data[0] : [...categoryList, ...result.data[0]]
      if (result.statusCode === 200) {
        yield put({
          type: "save",
          payload: { categoryList, categoryTotal: result.data[1] }
        })
      }
    },

    // 搜索
    *getSearch({ payload }, { call, put }) {
      let result = yield call(getSearch, payload);
      if (result.statusCode === 200) {
        yield put({
          type: "save",
          payload: { searchList: result.data }
        })
      }
    },
    //喜欢
    *getLike({ payload }, { call, put }) {
      let result = yield call(getLike, payload);
      if (result.statusCode === 200) {
        yield put({
          type: "save",
          payload: { likeState: result.data }
        })
      }
    },
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  }
};

export default IndexModel;