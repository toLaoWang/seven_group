import {  Effect, Reducer } from 'umi';
import {getAboutList, getCommentLists} from '@/services'
import {About} from '@/types'
import { msgboardItem } from '@/types/msgboard';
export interface AboutState {
    aboutList:Partial<About>;
    getCommentList:msgboardItem[]
    CommentNum:number
}
export interface AboutModelType {
    namespace: 'about';
    state: AboutState;
    effects: {
        getAboutList:Effect;
        getCommentLists:Effect;
    };
    reducers: {
        save: Reducer<AboutState>;
    };
}
const IndexModel: AboutModelType = {
    namespace: 'about',
    state: {
        aboutList:{},
        getCommentList:[],
        CommentNum:0
    },
    effects: {
        *getAboutList({},{call,put}){
            let result = yield call(getAboutList)
            if(result.statusCode){
                yield put({
                    type:"save",
                    payload:{aboutList:result.data}
                })
            }
        },
        *getCommentLists({payload},{call,put}){
            let result = yield call(getCommentLists,payload)
            console.log(result);
            
            if(result.statusCode){
                yield put({
                    type:"save",
                    payload:{getCommentList:result.data[0],CommentNum:result.data[1]}
                })
            }
        }
    },
    reducers: {
        save(state, action) {
            return {
                ...state,
                ...action.payload,
            };
        }
    }
};
export default IndexModel;