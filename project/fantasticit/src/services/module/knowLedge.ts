import { ParamsItem } from '@/types';
import { request } from 'umi';
// 获取知识小册列表数据
export function getKnowledge(page: number = 1, pageSize: number = 12, status = 'publish') {
    return request('/api/knowledge', {
        params: {
            page,
            pageSize,
            status,
        },
    })
}
//获取知识小册详情页
export function getKnowledgeDetail(id: string) {
    return request(`/api/knowledge/${id}`, { method: 'get' })
}
export function getViews(id: string) {
    return request(`/api/knowledge/${id}/views`, { method: 'post' })
}
//获取详情页评论
export function getkonwLedgeMsg(params: ParamsItem) {
    return request(`/api/comment/host/${params.id}?${params.page}&${params.pageSize}`)
}