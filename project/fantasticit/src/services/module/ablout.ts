import {request} from 'umi'
export function getAboutList(){
    return request(`/api/page/a5e81ffe-0ad0-4be9-acca-c0462b1b98a1/views`,{method:"post"})
}
export function getCommentLists(page: number, pageSize = 6) {
    return request('/api/comment/host/a5e81ffe-0ad0-4be9-acca-c0462b1b98a1', {
        params: {
            page,
            pageSize
        }
    })
}