import { childrenMsg } from '@/types/childrenMsg';
import { sendmsgboard } from '@/types/sendmsgboard';
import { request } from 'umi';
//获取留言板数据 分页
export function getMsgBoardList(page: number, pageSize = 6) {
    return request('/api/comment/host/8d8b6492-32e5-44e5-b38b-9a479d1a94bd', {
        params: {
            page,
            pageSize
        }
    })
}
//留言板发送评论
export function sendMsgBoard(params: sendmsgboard) {
    return request('/api/comment', { data: params, method: 'POST' })
}
//评论发送子评论
export function sendChildrenMsg(params: childrenMsg) {
    return request('/api/comment', { data: params, method: 'POST' })
}
