import { request } from 'umi';
// 获取文章列表
export function getArticleList(page: number, pageSize = 12, status = 'publish') {
    return request('/api/article', {
        params: {
            page,
            pageSize,
            status,
        }
    })
}
//获取文章详情页
export function getArticleDetail(id: string) {
    return request(`/api/article/${id}/views`, { method: 'post' })
}
// 获取推荐文章
export function getRecommend() {
    return request('/api/article/recommend')
}
// 动态获取推荐文章
export function getRecommends(articleId: string) {
    return request('/api/article/recommend', {
        params: {
            articleId
        }
    })
}
// 获取文章分类导航
export function getCategory(articleStatus = 'publish') {
    return request('/api/category', {
        articleStatus
    })
}
// 获取文章标签
export function getTag(articleStatus = 'publish') {
    return request('/api/tag', {
        params: {
            articleStatus
        }
    })
}
// 获取标签详情
export function getTagDeta(id: string, status = 'publish', page = 1, pageSize = 12) {
    return request(`/api/article/tag/${id}`, {
        params: {
            page, pageSize, status
        }
    })
}
// 获取分类页面数据
export function getCategoryList(type: string, page = 1, pageSize = 12, status = "publish") {
    return request(`/api/article/category/${type}?page=${page}&pageSize=${pageSize}&status=${status}`)
}

// 搜索
export function getSearch(keyword: string) {
    return request(`/api/search/article`, {
        params: {
            keyword
        }
    })
}

interface Like {
    id: string,
    type: string,
    lei: string
}
// 喜欢
export function getLike(payload: Like) {
    return request(`/api/${payload.lei}/${payload.id}/likes`, { method: 'post', data: { type: payload.type } })
}
