import { IObj } from '@/types';
import { request } from 'umi';
//获取列表数据
export function getArchivesList() {
   return request('/api/article/archives')
}
//获取详情数据
export function getArchivesDetail(id: string) {
   return request(`/api/article/${id}/views`, { method: 'post' })
}
//获取评论数据
export function getCommentList(obj: IObj) {
   return request(`/api/comment/host/${obj.id}?${obj.page}&${obj.pageSize}`)
}

