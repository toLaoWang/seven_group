import moment from 'moment'
moment.updateLocale('en', {
    relativeTime: {
        future: "in %s",
        past: "%s",
        s: '秒',
        ss: '几秒',
        m: "分钟",
        mm: "几分钟",
        h: "小时",
        hh: "几小时",
        d: "天",
        dd: "几天",
        M: "月",
        MM: "几月",
        y: "年",
        yy: "几年"
    }
});
export default moment