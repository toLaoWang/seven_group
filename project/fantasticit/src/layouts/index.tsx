import Header from "@/components/header";
import './index.less'
import Footer from "@/components/footer";
import Backtop from "@/components/backTop";

const Layouts: React.FC = (props) => {
    
    return <div id='div'>
        <div className="header">
            <Header />
        </div>
        <section>
            {props.children}
        </section>
        <Backtop />
        <Footer />
    </div>
}

export default Layouts;


