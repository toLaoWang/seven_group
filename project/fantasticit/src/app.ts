import { RequestConfig } from 'umi';
import { message } from 'antd';
import * as Sentry from "@sentry/react";
import { Integrations } from "@sentry/tracing";
import Nprogress from 'nprogress';
import 'nprogress/nprogress.css';
import { createLogger } from 'redux-logger';
Nprogress.configure({ showSpinner: false });
// 禁掉线上的console
// process.env.NODE_ENV === 'production'?console.log = ()=>{}: null;

// dva的日志配置
export const dva = process.env.NODE_ENV === 'production' ? {} : {
  config: {
    // onAction: createLogger(),
    onError(e: Error) {
      message.error(e.message, 3);
    },
  },
};
//qiaojiye
Sentry.init({
  dsn: "https://34bb700e4c7a443b9fa4dc8b5faecb8e@o974362.ingest.sentry.io/5930120",
  integrations: [new Integrations.BrowserTracing()],

  // Set tracesSampleRate to 1.0 to capture 100%
  // of transactions for performance monitoring.
  // We recommend adjusting this value in production
  tracesSampleRate: 1.0,
});

// 禁掉线上的console
process.env.NODE_ENV === 'production' ? console.log = () => { } : null;
// export const dva = {
//   config: {
//     onAction: createLogger(),
//     onError(e: Error) {
//       message.error(e.message, 3);
//     },
//   },
// };
// 全局路由切换配置
export function onRouteChange({ matchedRoutes }: any) {
  Nprogress.start();
  setTimeout(() => {
    Nprogress.done();
  }, 2000);
}
const baseUrl = 'https://creationapi.shbwyz.com';
export const request: RequestConfig = {
  timeout: 100000,
  errorConfig: {},
  middlewares: [],
  requestInterceptors: [
    (url, options) => {
      return {
        url: `${baseUrl}${url}`,
        options,
      };
    },
  ],
  responseInterceptors: [
    (response) => {
      const codeMaps: { [key: number]: string } = {
        400: '错误的请求',
        403: '禁止访问',
        404: '找不到资源',
        500: '服务器内部错误',
        502: '网关错误。',
        503: '服务不可用，服务器暂时过载或维护。',
        504: '网关超时。',
      };
      if (Object.keys(codeMaps).indexOf(String(response.status)) !== -1) {
        // message.error(codeMaps[response.status]);
      }
      return response;
    },
  ],
};
