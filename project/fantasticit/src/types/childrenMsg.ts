export interface childrenMsg {
    hostId: string;
    name: string;
    email: string;
    content: string;
    url: string;
    parentCommentId: string;
    replyUserName: string;
    replyUserEmail: string;
}