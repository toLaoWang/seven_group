export interface IPoster{
    width:number,
    height:number,
    html:string,
    name:string,
    pageUrl:string
}