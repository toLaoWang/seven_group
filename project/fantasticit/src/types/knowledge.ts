export interface KnowledgeObject {
    id: string;
    parentId?: any;
    order: number;
    title: string;
    cover: string;
    summary: string;
    content?: any;
    html?: any;
    toc?: any;
    status: string;
    views: number;
    likes: number;
    isCommentable: boolean;
    publishAt: string;
    createAt: string;
    updateAt: string;
}
export interface IKnowledgeDetailObject {
    id: string;
    parentId?: any;
    order: number;
    title: string;
    cover: string;
    summary: string;
    content?: any;
    html?: any;
    toc?: any;
    status: string;
    views: number;
    likes: number;
    isCommentable: boolean;
    publishAt: string;
    createAt: string;
    updateAt: string;
    children: IKnowledgeDetailChild[];
}
export interface IKnowledgeDetailChild {
    id: string;
    parentId: string;
    order: number;
    title: string;
    cover?: any;
    summary?: any;
    content: string;
    html: string;
    toc: string;
    status: string;
    views: number;
    likes: number;
    isCommentable: boolean;
    publishAt: string;
    createAt: string;
    updateAt: string;
}
export interface IDetailListObject {
    id: string;
    parentId: string;
    order: number;
    title: string;
    cover?: any;
    summary?: any;
    content: string;
    html: string;
    toc: string;
    status: string;
    views: number;
    likes: number;
    isCommentable: boolean;
    publishAt: string;
    createAt: string;
    updateAt: string;
}
export interface ParamsItem {
    id: string,
    page: number,
    pageSize: number
}