export interface sendmsgboard {
    hostId: string;
    name: string;
    email: string;
    content: string;
    url: string;
}