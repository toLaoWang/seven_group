import { ArchivesModelState } from "@/models/archives";
import { ArticleModelState } from "@/models/article";
import { KnowledgeState } from "@/models/knowledge";
import { AboutState } from '@/models/about'
import { LanguageModelState } from "@/models/language";
import { MsgBoardModelState } from '@/models/msgboard'
export interface IRootState {
    loading: { global: boolean },
    article: ArticleModelState;
    knowledge: KnowledgeState;
    about: AboutState;
    language: LanguageModelState;
    msgboard: MsgBoardModelState;
}
export interface IRootArchivesState {
    archives: ArchivesModelState;
}
