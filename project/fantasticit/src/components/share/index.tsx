import { Modal, Button } from 'antd'
import { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import { IArticleItem, IKnowledgeDetailObject, KnowledgeObject } from "@/types"
import QRCode from 'qrcode'
import { getPoster } from '@/services/module/poster';
// import 
export const Share: React.FC<{ item: IArticleItem | KnowledgeObject |Partial<IKnowledgeDetailObject>}> = ({ item}) => {

    const [qrSrc, setqrSrc] = useState(``);
    const [downloadSrc, setDownloadSrc] = useState(``);
    const url = `https://creationapi.shbwyz.com/archives/${item.id}`

    useEffect(() => {
        if (Object.keys(item)) {
            QRCode.toDataURL(url)
                .then(url => {
                    setqrSrc(url)
                })
                .catch(err => {
                    console.error(err)
                })
        }
    }, [item])

    useEffect(() => {
        if (Object.keys(item).length && qrSrc) {
            getPoster({
                height: 861,
                width: 400,
                name: item.title!,
                html: document.querySelector('.ant-modal-root')?.innerHTML!,
                pageUrl: `/archives/${item.id}`,
            }).then(res => {
                console.log(res);
                setDownloadSrc(res.data.url)
            })
        }
        
    }, [item, qrSrc])

    
    function handleCancel() {
        ReactDOM.unmountComponentAtNode(document.querySelector('#share-dislog')!)
    };

    return <Modal
        // width={400}
        visible={true}
        title="分享海报"
        onCancel={handleCancel}
        footer={[
            <Button key="back" onClick={handleCancel}>
                关闭
            </Button>,
            <Button key="submit" type="primary" onClick={() => {
                ReactDOM.unmountComponentAtNode(document.querySelector('#share-dislog')!)
                window.location.href = (downloadSrc)
            }}>
                下载
            </Button>
        ]}
    >
        {item.cover && <img style={{width:'100%',height:'100%'}} src={item.cover} alt="" />}
        <h3>{item.title}</h3>
        <p>{item.summary}</p>
        <div style={{ display: 'flex' }}>
            <div>
                <img src={qrSrc} alt="" style={{ width: '100px', height: '100px' }} />
            </div>
            <div style={{ position: 'relative', flex: 1, height: '100px' }}>
                <p style={{ position: 'absolute', top: 0 }}>识别二维码打开文章</p>
                <p style={{ position: 'absolute', bottom: 0, marginBottom: 0 }}>原文分享自<a style={{ color: 'red' }} href={url}>小楼又清风</a></p>
            </div>
        </div>
    </Modal>
}

export default function share(item: IArticleItem | KnowledgeObject |Partial<IKnowledgeDetailObject>) {
    let shareDislog = document.querySelector('#share-dislog');
    if (!shareDislog) {
        shareDislog = document.createElement('div');
        shareDislog.id = 'share-dislog';
        document.body.appendChild(shareDislog);
    }
    ReactDOM.render(<Share item={item}  />, shareDislog)
}