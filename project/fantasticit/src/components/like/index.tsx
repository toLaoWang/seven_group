import { IRootArchivesState, IRootState } from '@/types/model';
import { Modal, Button } from 'antd';
import React, { useEffect, useState } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { useDispatch, useSelector } from 'umi';
import style from './index.less';
import { Badge } from 'antd';
import QRCode from 'qrcode.react'
import share from '../share';
import { IArticleItem, IKnowledgeDetailObject } from '@/types';
interface Props{
    id: string, 
    num: number,
    lei: string,
    cover:string,
    summary:string,
    url:string,
    title:string,
    item:Partial<IKnowledgeDetailObject>
}

const Like: React.FC<Props> = (props) => {
    const [count, setCount] = useState(0);
    let { id, num, lei } = props;
    const dispatch = useDispatch();
    const [likes, setlikes] = useState<string[]>(localStorage.LIKES === undefined ? [] : JSON.parse(localStorage.LIKES));
    const { likeState } = useSelector((state: IRootState) => state.article);
    const [ind, setInd] = useState(-2);
    const [visible, setVisible] = useState(false);
    useEffect(() => {
        localStorage.LIKES = JSON.stringify(likes);
        setInd(JSON.stringify(likes).indexOf(id));
    }, [likes])

    function getLike() {
        if (ind == -1) {
            setlikes([...likes, id]);
            dispatch({
                type: 'article/getLike',
                payload: { id, lei, type: 'like' }
            })
        } else if (ind !== -2 && ind !== -1) {
            dispatch({
                type: 'article/getLike',
                payload: { id, lei, type: 'dislike' }
            })
            setlikes(likes.slice(ind, 1))
        }
    }
    function showModal() {
        setVisible(true);
    };

    function handleOk() {
        setVisible(false);

    };

    function handleCancel() {
        setVisible(false);
    };


    return (
        <div className={style.like}>
            <div onClick={() => getLike()}>
                <Badge offset={[2, -2]} size='small' count={likeState?.likes ? likeState?.likes : num}>
                    <span style={ind == -1 ? { color: 'var(--disable-text-color)' } : { color: 'var(--primary-color)' }}><svg viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em"><path d="M859.8 191.2c-80.8-84.2-212-84.2-292.8 0L512 248.2l-55-57.2c-81-84.2-212-84.2-292.8 0-91 94.6-91 248.2 0 342.8L512 896l347.8-362C950.8 439.4 950.8 285.8 859.8 191.2z" fill="currentColor"></path></svg></span>
                </Badge>
            </div>
            <div onClick={() => {
                document.documentElement.scrollTo(0, window.document.body.scrollHeight)
            }}>
                <span>
                    <svg viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em"><path d="M988.8 512a348.8 348.8 0 0 1-144.96 278.72v208.32l-187.84-131.52a387.2 387.2 0 0 1-56 4.8A408.64 408.64 0 0 1 384 811.84l-36.8-23.04a493.76 493.76 0 0 0 52.8 3.2 493.44 493.44 0 0 0 51.2-2.88c221.44-23.04 394.24-192 394.24-400a365.12 365.12 0 0 0-4.16-51.84 373.44 373.44 0 0 0-48.96-138.56l18.88 11.2A353.6 353.6 0 0 1 988.8 512z m-198.72-128c0-192-169.6-349.76-378.24-349.76h-24C192 47.04 33.92 198.72 33.92 384a334.08 334.08 0 0 0 118.4 253.12v187.52l86.08-60.48 66.24-46.4a396.16 396.16 0 0 0 107.52 16C620.48 734.08 790.08 576 790.08 384z" fill="currentColor"></path></svg>
                </span>
            </div>
            <div onClick={(e) => {
                            // showModal()
                            e.stopPropagation();
                            share(props.item)
                        }}>
                <span>
                    <svg viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em"><path d="M753.607 584.7c-48.519 0-91.596 23.298-118.66 59.315l-233.123-116.96c3.684-12.936 5.657-26.591 5.657-40.71 0-15.465-2.369-30.374-6.76-44.391l232.241-116.52c26.916 37.549 70.919 62.017 120.644 62.017 81.926 0 148.34-66.412 148.34-148.34 0-81.926-66.413-148.34-148.34-148.34-81.927 0-148.34 66.413-148.34 148.34 0 5.668 0.33 11.258 0.948 16.762l-244.945 122.892c-26.598-25.259-62.553-40.762-102.129-40.762-81.926 0-148.34 66.412-148.34 148.34s66.413 148.34 148.34 148.34c41.018 0 78.144-16.648 104.997-43.555l242.509 121.668c-0.904 6.621-1.382 13.374-1.382 20.242 0 81.927 66.412 148.34 148.34 148.34s148.34-66.413 148.34-148.34c-0.001-81.925-66.409-148.339-148.336-148.339l0 0z" fill="currentColor"></path></svg>
                </span>
            </div>
            {/* <Modal
                width={400}
                visible={visible}
                title="分享海报"
                onOk={handleOk}
                onCancel={handleCancel}
                footer={[
                    <Button key="back" onClick={handleCancel}>
                        关闭
                    </Button>,
                    <Button key="submit" type="primary" onClick={handleOk}>
                        下载
                    </Button>
                ]}
            >
                {props.cover&&<img src={props.cover} alt="" />}
                <h3>{props.title}</h3>
                <p>{props.summary}</p>
                <div style={{display:'flex'}}>
                    <div>
                        <QRCode style={{width:'80px',height:'80px'}} value={`https://blog.wipi.tech${props.url}`} />
                    </div>
                    <div style={{marginLeft:'1rem',position:'relative',flex:1,height:'80px'}}>
                        <p style={{position:'absolute',top:0}}>识别二维码打开文章</p>
                        <p  style={{position:'absolute',bottom:0,marginBottom:0}}>原文分享自<a style={{color:'red'}} href={`https://blog.wipi.tech${props.url}`}>小楼又清风</a></p>
                    </div>
                </div>
            </Modal> */}
        </div>

    );
};

export default Like;
