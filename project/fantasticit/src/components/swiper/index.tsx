import { Carousel } from 'antd'
import style from './index.less'
import moment from '@/utils/moment';
import { IRootState } from '@/types';
import { useEffect } from 'react';
import { useDispatch, useHistory, useSelector } from 'umi';
const Swiper: React.FC = (props) => {
    const dispatch = useDispatch()
    const { recommend } = useSelector((state: IRootState) => state.article);
    const history=useHistory();
    useEffect(() => {
        dispatch({
            type: 'article/getRecommend'
        })
    }, [])

    function toDetail(id: string) {
        history.push("/archives/" + id);
        document.documentElement.scrollTop = document.body.scrollTop = 0;
    }

    return <div className={style.Carousel}>
        <Carousel autoplay>
            {
                recommend[4] && <div className={style.carousel} onClick={() => toDetail(recommend[4].id)}>
                    <img src={recommend[4].cover} alt="" />
                    <div>
                        <h2>{recommend[4].title}</h2>
                        <p>
                            <span>{moment(recommend[4].publishAt).fromNow()}</span> · <span>{recommend[4].views}次阅读</span>
                        </p>
                    </div>
                </div>
            }
            {
                recommend[5] && <div className={style.carousel} onClick={() => toDetail(recommend[5].id)}>
                    <img src={recommend[5].cover} alt="" />
                    <div>
                        <h2>{recommend[5].title}</h2>
                        <p>

                            <span>{moment(recommend[5].publishAt).fromNow()}</span> ·
                            <span>{recommend[5].views}次阅读</span>
                        </p>
                    </div>
                </div>
            }

        </Carousel>
    </div>
}

export default Swiper;

