import { BackTop } from 'antd';
import {ArrowUpOutlined } from '@ant-design/icons'
import style from './index.less'

const backtop:React.FC=(props)=>{
    return <BackTop>
    <div className={style.backtop}><ArrowUpOutlined/></div>
  </BackTop>
}
export default backtop;

