import { RootsObject, Toc } from '@/types';
import React, { RefObject, useEffect, useState } from 'react';
import style from './index.less';
// import BetterScroll from 'better-scroll'

interface Props {
  archivesIObj: RootsObject;
  dom: RefObject<HTMLDivElement>;
}

const Level: React.FC<Props> = (props) => {
  const box = React.createRef<HTMLDivElement>();
  const list = React.createRef<HTMLDivElement>();
  const { dom, archivesIObj } = props;
  const [ind, setInd] = useState(0);

  //右边点击滚动高亮
  function handleChangeIndex(ind: number) {
    setInd(ind);
    document.documentElement.scrollTop =
      [...dom.current!.querySelectorAll('h2,h3')] &&
      ([...dom.current!.querySelectorAll('h2,h3')][ind] as HTMLElement)
        .offsetTop;
  }



  //滚动高亮
  window.onscroll = function () {
    dom.current &&
      [...dom.current!.querySelectorAll('h2,h3')].forEach((item, index) => {
        if (
          document.documentElement.scrollTop >= (item as HTMLElement).offsetTop
        ) {
          setInd(index);
        }
      });
  };

  useEffect(() => {
    if (ind === 0) {
      box.current!.scrollTop = 0;
    } else {
      // let bs = new BetterScroll(box.current!, {
      //   probeType: 3,
      //   momentumLimitDistance: 5,
      //   scrollY: true,
      //   scrollbar: {
      //     fade: true,
      //     interactive: false // 1.8.0 新增
      //   },
      //   mouseWheel: true,
      // })
      // bs.scrollToElement(([...box.current!.children[0].children!][ind] as HTMLElement),1000,0,0);
      // bs.refresh();
      box.current!.scrollTop =
        ([...box.current!.children[0].children!][ind] as HTMLElement)
          .offsetHeight *
        ind +
        10;
    }
  }, [ind]);

  return (
    <div className={style.List} ref={list}>
      <p className={style.list}>目录</p>
      <div className={style.box} ref={box}>
        <ul className={style.Detail_Right_Ul}>
          {archivesIObj?.toc &&
            JSON.parse(archivesIObj?.toc as string).map(
              (item: Toc, index: number) => {
                return (
                  <li
                    onClick={() => handleChangeIndex(index)}
                    className={ind === index ? style.active : ''}
                    style={
                      item.level == 2
                        ? { marginLeft: '12px' }
                        : { marginLeft: '24px' }
                    }
                    key={item.id}
                  >
                    {item.level == 2 ? (
                      <span className={style.two}></span>
                    ) : (
                      <span className={style.three}></span>
                    )}
                    <span>{item.text}</span>
                  </li>
                );
              },
            )}
        </ul>
      </div>
    </div>
  );
};

export default Level;
