import style from './index.less'
import { Input, Space } from 'antd';
import { withRouter, RouteComponentProps, NavLink } from 'react-router-dom'
import { useDispatch, useSelector, } from 'umi';
import { useEffect, useState } from 'react';
import { IRootState } from '@/types';


const { Search } = Input;
interface Search extends RouteComponentProps {
    bool: boolean,
    onBool(bool: boolean): void
}

const SearcH: React.FC<Search> = (props) => {
    const dispatch = useDispatch();
    const { searchList } = useSelector((state: IRootState) => state.article)
    const [value, setValue] = useState('')
    // useEffect(() => {

    //     console.log(searchList);

    // }, [value])

    function onSearch(value: string) {
        setValue(value);
        if (value) {
            dispatch({
                type: 'article/getSearch',
                payload: value
            })
        }

    }

    function getDetail(id: string) {
        props.history.push('/archives/' + id);
        // window.location.reload()
    }
    return <div className={style.search}>
        <div className={style.header}>
            <span>文章搜索</span>
            <span onClick={() => props.onBool(false)}>
                <span>
                    <svg viewBox="64 64 896 896" focusable="false" data-icon="close" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M563.8 512l262.5-312.9c4.4-5.2.7-13.1-6.1-13.1h-79.8c-4.7 0-9.2 2.1-12.3 5.7L511.6 449.8 295.1 191.7c-3-3.6-7.5-5.7-12.3-5.7H203c-6.8 0-10.5 7.9-6.1 13.1L459.4 512 196.9 824.9A7.95 7.95 0 00203 838h79.8c4.7 0 9.2-2.1 12.3-5.7l216.5-258.1 216.5 258.1c3 3.6 7.5 5.7 12.3 5.7h79.8c6.8 0 10.5-7.9 6.1-13.1L563.8 512z"></path></svg>
                </span>
                <span>esc</span>
            </span>
        </div>

        <div className={style.content}>
            <Space direction="vertical" style={{width:'100%'}}>
                <Search placeholder="输入关键字，搜索文章" value={value} onChange={(e) => { setValue(e.target.value) }} allowClear size='large' onSearch={() => onSearch(value)} style={{ width: '100%' }} />
            </Space>
            <ul className={style.ul}>
                {
                    searchList && searchList.map(item => {
                        return <NavLink key={item.id} onClick={() => {
                            document.documentElement.scrollTop = document.body.scrollTop = 0;
                            props.onBool(false)
                        }} to={`/archives/${item.id}`} exact>
                            <li
                            // onClick={()=>{getDetail(item.id)}}
                            >
                                {item.title}
                            </li>
                        </NavLink>
                    })
                }
            </ul>
        </div>
    </div>
}
export default withRouter(SearcH)