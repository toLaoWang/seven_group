import { IRootState } from "@/types";
import { useEffect, useState } from "react";
import { NavLink, useDispatch, useSelector, withRouter } from "umi";
import { RouterProps } from 'react-router-dom'
import styles from "./index.less"
const Nav: React.FC<RouterProps> = (props) => {
    const dispatch = useDispatch()
    const { category } = useSelector((state: IRootState) => state.article);


    useEffect(() => {
        if (!category.length) {
            dispatch({
                type: 'article/getCategory'
            })
        }
    }, [])


    return <div className={styles.categoryNav}>
        <span><NavLink activeStyle={{color:"var(--primary-color)"}} to="/" exact>所有</NavLink></span>
        {
            category.map((item, index) => {
                return <span
                    key={item.id}
                >
                    <NavLink activeStyle={{color:"var(--primary-color)"}}  to={`/category/${item.value}`}>{item.label}</NavLink></span>
            })
        }
    </div>
}
export default withRouter(Nav)