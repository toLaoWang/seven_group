import { msgboardItem } from '@/types/msgboard';
import React, { useRef, useState } from 'react'
import classNames from 'classnames';
import moment from 'moment';
import styles from './index.less'
import { Pagination, Popover, Button } from 'antd';
import { useDispatch } from 'umi';
import { ChangeEvent } from '@umijs/renderer-react/node_modules/@types/react';
import ChildrenMsg from '@/components/childrenMsg'
import Login from '../login';
import emo from '@/public/emojis'
moment.locale('zh-cn')

interface Props {
    msgboardList: msgboardItem[],
    msgboardNum: number,
    changePage(page: number): void,
}
const msgboards: React.FC<Props> = (props) => {
    let [current, setCurrent] = useState(1);
    let { msgboardList, msgboardNum, changePage } = props;
    let [val, setVal] = useState('');
    const [bool, setBool] = useState(false);
    const refs: any = useRef('')
    const dispatch = useDispatch();

    function getPage(page: number) {
        props.changePage(page)
        setCurrent(page)
    }

    function changeValue(value: string) {
        setVal(value)
    }

    function sendmsg() {
        dispatch({
            type: 'msgboard/sendMsgBoard',
            payload: {
                content: val,
                email: '691887089@qq.com',
                hostId: '8d8b6492-32e5-44e5-b38b-9a479d1a94bd',
                name: 'YJ',
                url: '/page/msgboard'
            }
        })
        refs.current.value = ''
        setVal('')
    }


    //表情
    function clickbq(e: React.MouseEvent<HTMLUListElement, MouseEvent>) {
        if ((e.target as Element).nodeName === 'LI') {
            setVal(val + (e.target as Element).innerHTML)
        }
    }
    const content = (
        <ul className={styles.SO1DW6R4gpGKoWYBL5g62} onClick={(e) => clickbq(e)}>
            {
                Object.keys(emo).map((item, index) => {
                    return <li key={index}>{emo[item]}</li>
                })
            }
        </ul>
    );
    //点击回复显示回复框
    function changeFlag(e: React.MouseEvent<HTMLSpanElement, MouseEvent>) {
        console.log(((e.target as Element).nextSibling as unknown as ElementCSSInlineStyle).style.display);

        if (((e.target as Element).nextSibling as unknown as ElementCSSInlineStyle).style.display === 'none') {
            ((e.target as Element).nextSibling as unknown as ElementCSSInlineStyle).style.display = 'block';
        } else {
            ((e.target as Element).nextSibling as unknown as ElementCSSInlineStyle).style.display = 'none';
        }
    }
    function getFous() {
        setBool(!bool);
    }
    function getBool() {
        setBool(false);
    }

    return (
        <>
            <div className={styles.textInputBox}>
                {!localStorage.user && <Login bool={bool} getBool={() => getBool()} />}{/*判断是否登录 */}
                <textarea onClick={() => getFous()} placeholder='请输入评论内容（支持 Markdown）' ref={refs} value={val} onInput={(e: ChangeEvent<HTMLTextAreaElement>) => changeValue(e.target.value)}></textarea>
                <div className={styles.btnBox} onClick={() => getFous()} >
                    <div>
                        {!localStorage.user ?
                            <Button style={{ border: 'none', background: 'none', display: 'flex', alignItems: 'center', color: 'var(--disable-text-color)' }}>
                                <svg viewBox="0 0 1024 1024" width="18px" height="18px"><path d="M288.92672 400.45568c0 30.80192 24.97024 55.77216 55.77216 55.77216s55.77216-24.97024 55.77216-55.77216c0-30.7968-24.97024-55.76704-55.77216-55.76704s-55.77216 24.97024-55.77216 55.76704z m334.60224 0c0 30.80192 24.97024 55.77216 55.77216 55.77216s55.77216-24.97024 55.77216-55.77216c0-30.7968-24.97024-55.76704-55.77216-55.76704s-55.77216 24.97024-55.77216 55.76704z m-111.5392 362.4704c-78.05952 0-156.13952-39.08096-200.75008-100.3776-16.77312-22.31296-27.84256-50.15552-39.08096-72.45824-5.53472-16.77312 5.5296-33.4592 16.77312-39.08096 16.77312-5.53472 27.84256 5.53472 33.46432 16.768 5.53472 22.30784 16.77312 39.08608 27.84256 55.77728 44.61568 55.76704 100.38272 83.69664 161.664 83.69664 61.30176 0 122.7008-27.84256 156.16-78.07488 11.15136-16.77824 22.30784-38.99904 27.84256-55.77728 5.62176-16.768 22.30784-22.30272 33.4592-16.768 16.768 5.53472 22.30784 22.30272 16.768 33.4592-5.61152 27.84256-22.2976 50.14528-39.08096 72.45824-38.912 61.37856-116.98176 100.3776-195.06176 100.3776z m0 194.51392C268.4928 957.44 66.56 755.52256 66.56 511.99488 66.56 268.48256 268.4928 66.56 511.98976 66.56 755.50208 66.56 957.44 268.48256 957.44 511.99488 957.44 755.52256 755.50208 957.44 511.98976 957.44z m0-831.45728c-213.78048 0-386.00192 172.21632-386.00192 386.01216 0 213.8112 172.22144 386.0224 386.00192 386.0224 213.80096 0 386.0224-172.2112 386.0224-386.0224 0-213.79584-172.22144-386.01216-386.0224-386.01216z" fill="currentColor"></path></svg>
                                <span >表情</span>
                            </Button>
                            :
                            <div style={{ clear: 'both', whiteSpace: 'nowrap' }}>
                                <Popover placement="bottomLeft" content={content} trigger="click"  >
                                    <Button style={{ border: 'none', background: 'none', display: 'flex', alignItems: 'center', color: 'var(--disable-text-color)' }}>
                                        <svg viewBox="0 0 1024 1024" width="18px" height="18px"><path d="M288.92672 400.45568c0 30.80192 24.97024 55.77216 55.77216 55.77216s55.77216-24.97024 55.77216-55.77216c0-30.7968-24.97024-55.76704-55.77216-55.76704s-55.77216 24.97024-55.77216 55.76704z m334.60224 0c0 30.80192 24.97024 55.77216 55.77216 55.77216s55.77216-24.97024 55.77216-55.77216c0-30.7968-24.97024-55.76704-55.77216-55.76704s-55.77216 24.97024-55.77216 55.76704z m-111.5392 362.4704c-78.05952 0-156.13952-39.08096-200.75008-100.3776-16.77312-22.31296-27.84256-50.15552-39.08096-72.45824-5.53472-16.77312 5.5296-33.4592 16.77312-39.08096 16.77312-5.53472 27.84256 5.53472 33.46432 16.768 5.53472 22.30784 16.77312 39.08608 27.84256 55.77728 44.61568 55.76704 100.38272 83.69664 161.664 83.69664 61.30176 0 122.7008-27.84256 156.16-78.07488 11.15136-16.77824 22.30784-38.99904 27.84256-55.77728 5.62176-16.768 22.30784-22.30272 33.4592-16.768 16.768 5.53472 22.30784 22.30272 16.768 33.4592-5.61152 27.84256-22.2976 50.14528-39.08096 72.45824-38.912 61.37856-116.98176 100.3776-195.06176 100.3776z m0 194.51392C268.4928 957.44 66.56 755.52256 66.56 511.99488 66.56 268.48256 268.4928 66.56 511.98976 66.56 755.50208 66.56 957.44 268.48256 957.44 511.99488 957.44 755.52256 755.50208 957.44 511.98976 957.44z m0-831.45728c-213.78048 0-386.00192 172.21632-386.00192 386.01216 0 213.8112 172.22144 386.0224 386.00192 386.0224 213.80096 0 386.0224-172.2112 386.0224-386.0224 0-213.79584-172.22144-386.01216-386.0224-386.01216z" fill="currentColor"></path></svg>
                                        <span >表情</span>
                                    </Button>
                                </Popover>
                            </div>
                        }{/*判断是否登录 */}
                    </div>
                    <button className={val ? styles.changeBtn : ''} disabled={val ? false : true} style={{ cursor: val ? 'pointer' : '' }} onClick={sendmsg}>发布</button>
                </div>
            </div>
            {
                msgboardList.map((item: msgboardItem, index) => {
                    return <div key={index} className={classNames(styles.msgBox, styles._3G7_JYLS4EIxTgILiJm_A5)}>
                        <div className={styles.header}>
                            <span className={styles.ant_avatar} style={{ width: '32px', height: '32px', lineHeight: '32px', fontSize: '18px', backgroundColor: 'rgb(255, 0, 100)' }}>
                                <span className={styles.ant_avatar_string} style={{ lineHeight: '32px', transform: 'scale(1) translateX(-50%)' }}>{item.name.substr(0, 1)}</span>
                            </span>
                            <span className={styles.nameBox}>
                                <strong>{item.name}</strong>
                            </span>
                        </div>
                        <div className={styles.main} style={{ padding: '12px 0px 12px 40px' }}>
                            <div dangerouslySetInnerHTML={{ __html: item.content }}>
                            </div>
                        </div>
                        <div className={styles.footer} style={{ paddingLeft: '40px' }}>
                            <span>{item.userAgent}</span>
                            <time dateTime={item.createAt}>
                                {
                                    moment(+new Date(item.createAt)).fromNow()
                                }
                            </time>
                            <span className={classNames(styles.backBox)} onClick={(e) => changeFlag(e)}>
                                <span role="img" aria-label="message" className={styles.action} style={{ marginRight: '4px' }}><svg viewBox="64 64 896 896" focusable="false" data-icon="message" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M464 512a48 48 0 1096 0 48 48 0 10-96 0zm200 0a48 48 0 1096 0 48 48 0 10-96 0zm-400 0a48 48 0 1096 0 48 48 0 10-96 0zm661.2-173.6c-22.6-53.7-55-101.9-96.3-143.3a444.35 444.35 0 00-143.3-96.3C630.6 75.7 572.2 64 512 64h-2c-60.6.3-119.3 12.3-174.5 35.9a445.35 445.35 0 00-142 96.5c-40.9 41.3-73 89.3-95.2 142.8-23 55.4-34.6 114.3-34.3 174.9A449.4 449.4 0 00112 714v152a46 46 0 0046 46h152.1A449.4 449.4 0 00510 960h2.1c59.9 0 118-11.6 172.7-34.3a444.48 444.48 0 00142.8-95.2c41.3-40.9 73.8-88.7 96.5-142 23.6-55.2 35.6-113.9 35.9-174.5.3-60.9-11.5-120-34.8-175.6zm-151.1 438C704 845.8 611 884 512 884h-1.7c-60.3-.3-120.2-15.3-173.1-43.5l-8.4-4.5H188V695.2l-4.5-8.4C155.3 633.9 140.3 574 140 513.7c-.4-99.7 37.7-193.3 107.6-263.8 69.8-70.5 163.1-109.5 262.8-109.9h1.7c50 0 98.5 9.7 144.2 28.9 44.6 18.7 84.6 45.6 119 80 34.3 34.3 61.3 74.4 80 119 19.4 46.2 29.1 95.2 28.9 145.8-.6 99.6-39.7 192.9-110.1 262.7z"></path></svg></span>
                                回复
                            </span>
                            <ChildrenMsg name={item.name} item={item} />
                            {
                                item.children && item.children.map((item2, index2) => {
                                    return <div key={index2} className={styles.smallBox}>
                                        <div className={styles.children_footer}>
                                            <span className={styles.antAavatar} style={{ width: '24px', height: '24px', lineHeight: '24px', fontSize: '18px', backgroundColor: 'rgb(250, 173, 20)' }}>
                                                <span className={styles.ant_avatar_string} style={{ lineHeight: '24px', transform: 'scale(1) translateX(-50%)' }}>{item2.name.substr(0, 1)}</span>
                                            </span>
                                            <span className={styles.smallnameBox}>
                                                <strong>{item2.name}</strong>
                                                <span style={{ margin: '0px 8px' }}>回复</span>
                                                <strong>{item2.replyUserName}</strong>
                                            </span>
                                        </div>
                                        <div className={styles.children_main} style={{ padding: '12px 0px 12px 32px' }}>
                                            <div dangerouslySetInnerHTML={{ __html: item2.content }}>
                                            </div>
                                        </div>
                                        <div className={styles.children_footer} style={{ paddingLeft: '32px' }}>
                                            <span>{item2.userAgent}</span>
                                            <time dateTime={item.createAt}>
                                                {
                                                    moment(+new Date(item2.createAt)).fromNow()
                                                }
                                            </time>
                                            <span className={classNames(styles.backBox, styles.backBoxTwo)} onClick={(e) => changeFlag(e)}>
                                                <span role="img" aria-label="message" className={styles.action} style={{ marginRight: '4px' }}><svg viewBox="64 64 896 896" focusable="false" data-icon="message" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M464 512a48 48 0 1096 0 48 48 0 10-96 0zm200 0a48 48 0 1096 0 48 48 0 10-96 0zm-400 0a48 48 0 1096 0 48 48 0 10-96 0zm661.2-173.6c-22.6-53.7-55-101.9-96.3-143.3a444.35 444.35 0 00-143.3-96.3C630.6 75.7 572.2 64 512 64h-2c-60.6.3-119.3 12.3-174.5 35.9a445.35 445.35 0 00-142 96.5c-40.9 41.3-73 89.3-95.2 142.8-23 55.4-34.6 114.3-34.3 174.9A449.4 449.4 0 00112 714v152a46 46 0 0046 46h152.1A449.4 449.4 0 00510 960h2.1c59.9 0 118-11.6 172.7-34.3a444.48 444.48 0 00142.8-95.2c41.3-40.9 73.8-88.7 96.5-142 23.6-55.2 35.6-113.9 35.9-174.5.3-60.9-11.5-120-34.8-175.6zm-151.1 438C704 845.8 611 884 512 884h-1.7c-60.3-.3-120.2-15.3-173.1-43.5l-8.4-4.5H188V695.2l-4.5-8.4C155.3 633.9 140.3 574 140 513.7c-.4-99.7 37.7-193.3 107.6-263.8 69.8-70.5 163.1-109.5 262.8-109.9h1.7c50 0 98.5 9.7 144.2 28.9 44.6 18.7 84.6 45.6 119 80 34.3 34.3 61.3 74.4 80 119 19.4 46.2 29.1 95.2 28.9 145.8-.6 99.6-39.7 192.9-110.1 262.7z"></path></svg></span>
                                                回复
                                            </span>
                                            <ChildrenMsg name={item2.name} item={item} item2={item2} />
                                        </div>

                                    </div>
                                })
                            }
                        </div>
                    </div>
                })
            }
            <div style={{ opacity: 1, borderRadius: "var(--border-radius)", backgroundColor: "var(--bg-second)", boxShadow: "var(--box-shadow)" }}>
                <div className={styles._3k5_guVzUUtXzu2skrNnws} style={{ padding: '16px 0px' }}>
                    <Pagination size="small" total={msgboardNum} pageSize={6} current={current} onChange={getPage} />
                </div>
            </div>
        </>
    )
}

export default msgboards;
