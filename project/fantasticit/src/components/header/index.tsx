import { useState } from 'react';
import { withRouter, RouterProps } from 'react-router-dom';
import Search from '@/components/search'
import './index.less'
import { NavLink, useDispatch, useSelector, setLocale, useIntl } from 'umi';
import { useEffect } from 'react';
import { IRootState } from '@/types';
import { Spin, Select } from 'antd';
const Header: React.FC<RouterProps> = (props) => {
    let nav = [
        {
            url: "/",
            title: "menu.article"
        },
        {
            url: "/archives",
            title: "menu.archive"
        },
        {
            url: "/knowledge",
            title: "menu.knowledge"
        },
        {
            url: "/page/msgboard",
            title: "menu.message"
        },
        {
            url: "/page/about",
            title: "menu.about"
        },
    ]
    const intl = useIntl();
    const dispatch = useDispatch();
    const { language } = useSelector((state: IRootState) => {
        return {
            global: state.loading.global,
            language: state.language
        }
    })
    useEffect(() => {
        setLocale(language.locale, false);
    }, [language.locale]);

    function changeLanguage(locale: string) {
        dispatch({
            type: 'language/save',
            payload: { locale }
        })
    }

    const [flag, setflag] = useState(false)
    const [bool, setBool] = useState(false);
    const [isShow, setShow] = useState(false);
    useEffect(() => {
        isShow ? document.body.classList.add('dark') : document.body.classList.remove('dark')
    }, [isShow])
    function getSearch() {
        setBool(true)
    }
    function getBool(bool: boolean) {
        setBool(bool)
    }
    function show() {
        setShow(!isShow)
    }
    return (
        <header>
            <div className="container">
                {/* 全局loading */}
                {/* {global && <div className='loadding'><Spin size="large" /></div>} */}

                <div className='img_box'>
                    <a href="/">
                        <img src="https://wipi.oss-cn-shanghai.aliyuncs.com/2021-02-20/wipi-logo.png" alt="" />
                    </a>
                </div>
                <nav className={flag ? '_nav' : ''}>
                    <ul className='title_list'>
                        {
                            nav.map((item, index) => {
                                return <li key={index}><span><NavLink activeStyle={{ color: "var(--primary-color)" }} exact to={item.url}>{intl.formatMessage({ id: item.title })}</NavLink></span></li>
                            })
                        }
                        <li>
                            <Select value={language.locale} onChange={val => changeLanguage(val)}>{
                                language.locales.map(item => {
                                    return <Select.Option key={item.value} value={item.value}>{intl.formatMessage({ id: item.label, defaultMessage: '' })}</Select.Option>
                                })
                            }</Select>
                        </li>
                        <li className="_38OmGXMfjYo_xu-5O1rJEK" onClick={show}><div className={isShow ? "_2BHtRLIf7EUPHvdL0oIKeR _1M2d1ahyt0GjdRDlVxVqCN" : "_2BHtRLIf7EUPHvdL0oIKeR"}><span className="_2DxxB4r6HYBEDcLqnvTKMg"></span><span className="_3FbZOtsOBAPa2UFFqIaC_z"></span><small className="_3Gvr4LAEinc-IIBjwFSMH_"></small><small className="_3Gvr4LAEinc-IIBjwFSMH_"></small><small className="_3Gvr4LAEinc-IIBjwFSMH_"></small><small className="_3Gvr4LAEinc-IIBjwFSMH_"></small><small className="_3Gvr4LAEinc-IIBjwFSMH_"></small><small className="_3Gvr4LAEinc-IIBjwFSMH_"></small></div></li>
                        <li onClick={() => getSearch()}>
                            <span role="img" aria-label="search" className="anticon anticon-search"><svg viewBox="64 64 896 896" focusable="false" data-icon="search" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M909.6 854.5L649.9 594.8C690.2 542.7 712 479 712 412c0-80.2-31.3-155.4-87.9-212.1-56.6-56.7-132-87.9-212.1-87.9s-155.5 31.3-212.1 87.9C143.2 256.5 112 331.8 112 412c0 80.1 31.3 155.5 87.9 212.1C256.5 680.8 331.8 712 412 712c67 0 130.6-21.8 182.7-62l259.7 259.6a8.2 8.2 0 0011.6 0l43.6-43.5a8.2 8.2 0 000-11.6zM570.4 570.4C528 612.7 471.8 636 412 636s-116-23.3-158.4-65.6C211.3 528 188 471.8 188 412s23.3-116.1 65.6-158.4C296 211.3 352.2 188 412 188s116.1 23.2 158.4 65.6S636 352.2 636 412s-23.3 116.1-65.6 158.4z"></path></svg></span>
                        </li>
                    </ul>
                </nav>
                <div className={flag ? "btn active" : 'btn'}
                    onClick={() => setflag(!flag)}
                >
                    <div className="wire"></div>
                    <div className="wire"></div>
                    <div className="wire"></div>
                </div>
            </div>
            {bool && <Search bool={bool} onBool={getBool}></Search>}
        </header>
    )
}

export default withRouter(Header);
