import style from './index.less'
import { IRootState } from '@/types';
import { useEffect } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom'
import { useDispatch, useSelector } from 'umi';
interface biaoqian {
    biao: string,
    qian: string
}
const Label: React.FC<RouteComponentProps> = (props) => {
    const dispatch = useDispatch()
    const { tags } = useSelector((state: IRootState) => state.article);
    useEffect(() => {
        dispatch({
            type: 'article/getTag'
        })
    }, [])

    function getTagDeta({ biao, qian }: biaoqian) {
        props.history.push({
            pathname: '/tag/' + biao,
            state: {
                qian
            }
        })
    }


    return <div className={style.tag}>
        <div className={style.title}>
            <span>文章标签</span>
        </div>
        <ul>
            {
                tags && tags.map(item => {
                    return <li key={item.id} onClick={() => { getTagDeta({ biao: item.value, qian: item.label }) }}>
                        <span>{item.label} [{item.articleCount}]</span>
                    </li>
                })
            }
        </ul>

    </div>
}

export default withRouter(Label);

