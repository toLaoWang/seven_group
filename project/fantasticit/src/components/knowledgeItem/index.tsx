import { KnowledgeObject } from "@/types";
import styles from '@/pages/knowledge.less';
import moment from "moment";
import { Modal, Button } from 'antd'
import QRCode from 'qrcode.react'
import { useState } from "react";
import share from "../share";
moment.locale('zh-cn')
interface Props {
    item: KnowledgeObject,
    goKnowledgeDetail(id: string): void
}
const KnowledgeItem: React.FC<Props> = (props) => {
    let { item, goKnowledgeDetail } = props;
    const [visible, setVisible] = useState(false);

    function showModal() {
        setVisible(true);
    };

    function handleOk() {
        setVisible(false);

    };

    function handleCancel() {
        setVisible(false);
    };

    return <>
        <div className={styles.XifRPckEIhR} onClick={() => { goKnowledgeDetail(item.id) }}>
            <div className={styles.knowledge_header}>
                <span className={styles.knowledge_header_title}>{item.title}</span>
                <span>{moment(item.createAt).startOf('day').fromNow()}</span></div>
            <div className={styles.knowledge_main}>
                <div className={styles.knowledge_pic}> <img src={item.cover} alt="" /></div>
                <div className={styles.knowledge_content}>
                    <p>{item.summary}</p>
                    <p>
                        <span>
                            <i> <svg viewBox="64 64 896 896" focusable="false" data-icon="eye" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M942.2 486.2C847.4 286.5 704.1 186 512 186c-192.2 0-335.4 100.5-430.2 300.3a60.3 60.3 0 000 51.5C176.6 737.5 319.9 838 512 838c192.2 0 335.4-100.5 430.2-300.3 7.7-16.2 7.7-35 0-51.5zM512 766c-161.3 0-279.4-81.8-362.7-254C232.6 339.8 350.7 258 512 258c161.3 0 279.4 81.8 362.7 254C791.5 684.2 673.4 766 512 766zm-4-430c-97.2 0-176 78.8-176 176s78.8 176 176 176 176-78.8 176-176-78.8-176-176-176zm0 288c-61.9 0-112-50.1-112-112s50.1-112 112-112 112 50.1 112 112-50.1 112-112 112z"></path></svg></i>
                            {item.views}
                        </span>
                        <span onClick={(e) => {
                            // showModal()
                            e.stopPropagation();
                            // share(item, 'knowledge')
                            share(item)
                        }}>
                            <i> <svg viewBox="64 64 896 896" focusable="false" data-icon="share-alt" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M752 664c-28.5 0-54.8 10-75.4 26.7L469.4 540.8a160.68 160.68 0 000-57.6l207.2-149.9C697.2 350 723.5 360 752 360c66.2 0 120-53.8 120-120s-53.8-120-120-120-120 53.8-120 120c0 11.6 1.6 22.7 4.7 33.3L439.9 415.8C410.7 377.1 364.3 352 312 352c-88.4 0-160 71.6-160 160s71.6 160 160 160c52.3 0 98.7-25.1 127.9-63.8l196.8 142.5c-3.1 10.6-4.7 21.8-4.7 33.3 0 66.2 53.8 120 120 120s120-53.8 120-120-53.8-120-120-120zm0-476c28.7 0 52 23.3 52 52s-23.3 52-52 52-52-23.3-52-52 23.3-52 52-52zM312 600c-48.5 0-88-39.5-88-88s39.5-88 88-88 88 39.5 88 88-39.5 88-88 88zm440 236c-28.7 0-52-23.3-52-52s23.3-52 52-52 52 23.3 52 52-23.3 52-52 52z"></path></svg>
                            </i>
                            分享
                        </span>
                    </p>
                </div>
            </div>
        </div>
        <div className={styles.hr_border}><div className={styles.hr}></div></div>
        <Modal
            width={400}
            visible={visible}
            title="分享海报"
            onOk={handleOk}
            onCancel={handleCancel}
            footer={[
                <Button key="back" onClick={handleCancel}>
                    关闭
                </Button>,
                <Button key="submit" type="primary" onClick={handleOk}>
                    下载
                </Button>
            ]}
        >
            {item.cover && <img src={item.cover} alt="" />}
            <h3>{item.title}</h3>
            <p>{item.summary}</p>
            <div style={{ display: 'flex' }}>
                <div>
                    <QRCode style={{ width: '80px', height: '80px' }} value={`https://blog.wipi.tech/knowledge/${item.id}`} />
                </div>
                <div style={{ marginLeft: '1rem', position: 'relative', flex: 1, height: '80px' }}>
                    <p style={{ position: 'absolute', top: 0 }}>识别二维码打开文章</p>
                    <p style={{ position: 'absolute', bottom: 0, marginBottom: 0 }}>原文分享自<a style={{ color: 'red' }} href={`https://blog.wipi.tech/knowledge/${item.id}`}>小楼又清风</a></p>
                </div>
            </div>
        </Modal>
    </>

}


export default KnowledgeItem;


