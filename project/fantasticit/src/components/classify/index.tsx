import style from './index.less'
import { IRootState } from '@/types';
import { useEffect } from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom'
import { useDispatch, useSelector } from 'umi';
const Classify: React.FC<RouteComponentProps> = (props) => {
    const dispatch = useDispatch()
    const { category } = useSelector((state: IRootState) => state.article);
    useEffect(() => {
        dispatch({
            type: 'article/getCategory'
        })
    }, [])
    function getClassify(lei: string) {
        props.history.push('/category/' + lei)
    }
    return <div className={style.classify}>
        <div className={style.title}>
            <span>文章分类</span>
        </div>
        <ul>
            {
                category && category.map(item => {
                    return <li key={item.id} onClick={() => getClassify(item.value)}>
                        <span>{item.label}</span>
                        <span>共计{item.articleCount}篇文章</span>
                    </li>
                })
            }
        </ul>

    </div>
}

export default withRouter(Classify);

