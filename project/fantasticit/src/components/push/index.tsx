import style from './index.less'
import { IRootState } from '@/types';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'umi';
import { RouteComponentProps, withRouter } from 'react-router-dom'
import moment from 'moment';
interface ID {
    id: string
}
const Push: React.FC<RouteComponentProps<ID>> = (props) => {
    const dispatch = useDispatch()
    const { recommends } = useSelector((state: IRootState) => state.article);
    useEffect(() => {
        dispatch({
            type: 'article/getRecommends',
            payload: props.match.params.id
        })
    }, [])
    moment.locale('zh-cn')

    function toDetail(id: string) {
        props.history.push("/archives/" + id)
    }

    return <div className={style.push}>
        <div className={style.title}>
            <span>推荐阅读</span>
        </div>
        <ul>
            {
                recommends && recommends.map(item => {
                    return <li key={item.id}
                        onClick={() => toDetail(item.id)}
                    >
                        <a href="">
                            <span>{item.title}</span> ·
                            <span>
                                <time dateTime={item.createAt}>
                                    {
                                        moment(+new Date(item.createAt)).fromNow()
                                    }
                                </time>
                            </span>
                        </a>
                    </li>
                })
            }
        </ul>

    </div>
}

export default withRouter(Push);

