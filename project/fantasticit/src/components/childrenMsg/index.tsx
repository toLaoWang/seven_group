//封装评论列表回复框组件
import React, { ChangeEvent, useEffect, useState } from 'react';
import styles from './index.less'
import { Popover, Button } from 'antd';
import classNames from 'classnames';
import { useDispatch } from 'umi';
import { Child, msgboardItem } from '@/types/msgboard';
import emo from '@/public/emojis'

interface Props {
    name: string,
    item: msgboardItem,
    item2?: Child,
}
const ChildrenMsg: React.FC<Props> = (props) => {

    const dispatch = useDispatch();
    const { name, item } = props


    let [val, setVal] = useState('')
    const inp = React.createRef<HTMLTextAreaElement>()
    const bigBox = React.createRef<HTMLDivElement>()
    let [flag, setFlag] = useState(item.pass)

    //改变val的内容
    function changeValue(value: string) {
        setVal(value)
    }
    //发送评论，发送请求
    function sendmsg() {
        dispatch({
            type: 'msgboard/sendChildrenMsg',
            payload: {
                content: val,
                email: '691887089@qq.com',
                hostId: "8d8b6492-32e5-44e5-b38b-9a479d1a94bd",
                name: "YJ",
                parentCommentId: props.item.id,
                replyUserEmail: props.item2 ? props.item2.email : props.item.email,
                replyUserName: name,
                url: "/page/msgboard"
            }
        })
        inp.current!.value = ''
        setVal('')
    }

    function changeFlag() {
        if (flag) {
            bigBox.current!.style.display = 'none';
        }
    }
    //表情
    function clickbq(e: any) {
        setVal(val + e.target.innerHTML)
    }
    const content = (
        <ul className={styles.SO1DW6R4gpGKoWYBL5g62} onClick={(e: any) => clickbq(e)}>
            {
                Object.keys(emo).map((item, index) => {
                    return <li key={index}>{emo[item]}</li>
                })
            }
        </ul>
    );
    return <div className={classNames(styles._1oweEwxuizpZY8B)} ref={bigBox} style={{ display: 'none' }}>
        <textarea className={styles.childrenInput} ref={inp} value={val} onInput={(e: ChangeEvent<HTMLTextAreaElement>) => changeValue(e.target.value)} placeholder={'回复' + ' ' + name} style={{ width: '100%', height: '98px', minHeight: '98px', maxHeight: '186px', overflowY: 'hidden', resize: 'none' }}></textarea>
        <div className={styles.children_footer}>
            <div style={{ clear: 'both', whiteSpace: 'nowrap' }}>
                <Popover placement="bottomLeft" content={content} trigger="click">
                    <Button style={{ border: 'none', background: 'none', display: 'flex', alignItems: 'center', color: 'var(--disable-text-color)' }}>
                        <svg viewBox="0 0 1024 1024" width="18px" height="18px"><path d="M288.92672 400.45568c0 30.80192 24.97024 55.77216 55.77216 55.77216s55.77216-24.97024 55.77216-55.77216c0-30.7968-24.97024-55.76704-55.77216-55.76704s-55.77216 24.97024-55.77216 55.76704z m334.60224 0c0 30.80192 24.97024 55.77216 55.77216 55.77216s55.77216-24.97024 55.77216-55.77216c0-30.7968-24.97024-55.76704-55.77216-55.76704s-55.77216 24.97024-55.77216 55.76704z m-111.5392 362.4704c-78.05952 0-156.13952-39.08096-200.75008-100.3776-16.77312-22.31296-27.84256-50.15552-39.08096-72.45824-5.53472-16.77312 5.5296-33.4592 16.77312-39.08096 16.77312-5.53472 27.84256 5.53472 33.46432 16.768 5.53472 22.30784 16.77312 39.08608 27.84256 55.77728 44.61568 55.76704 100.38272 83.69664 161.664 83.69664 61.30176 0 122.7008-27.84256 156.16-78.07488 11.15136-16.77824 22.30784-38.99904 27.84256-55.77728 5.62176-16.768 22.30784-22.30272 33.4592-16.768 16.768 5.53472 22.30784 22.30272 16.768 33.4592-5.61152 27.84256-22.2976 50.14528-39.08096 72.45824-38.912 61.37856-116.98176 100.3776-195.06176 100.3776z m0 194.51392C268.4928 957.44 66.56 755.52256 66.56 511.99488 66.56 268.48256 268.4928 66.56 511.98976 66.56 755.50208 66.56 957.44 268.48256 957.44 511.99488 957.44 755.52256 755.50208 957.44 511.98976 957.44z m0-831.45728c-213.78048 0-386.00192 172.21632-386.00192 386.01216 0 213.8112 172.22144 386.0224 386.00192 386.0224 213.80096 0 386.0224-172.2112 386.0224-386.0224 0-213.79584-172.22144-386.01216-386.0224-386.01216z" fill="currentColor"></path></svg>
                        <span>表情</span>
                    </Button>
                </Popover>
            </div>
            <div>
                <Button style={{ marginRight: '16px' }} className={styles.ant_btn_sm} onClick={changeFlag}>收起</Button>
                {
                    val ? <Button type="primary" onClick={sendmsg} className={classNames(styles.ant_btn_sm, styles.ant_btn_primary)}>发送</Button>
                        : <Button type="primary" disabled className={classNames(styles.ant_btn_sm, styles.ant_btn_diable)}>
                            发送
                        </Button>
                }
            </div>
        </div>
    </div >
}

export default ChildrenMsg;