import { defineConfig } from 'umi';
export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  fastRefresh: {},
  antd: {},
  dva: {
    immer: true,
    hmr: true,
  },
  // favicon: "//avatars.githubusercontent.com/u/26452939?v=4",
  title: "小楼又清风",
  locale: {},
    //配置线上打包路径
  publicPath: process.env.NODE_ENV === 'production' ? '/1812A/wangning/fantasticit/' : '/',
   //配置线上路由前缀
  base:process.env.NODE_ENV === 'production' ? '/1812A/wangning/fantasticit/' : '/',
  hash:true,
  dynamicImport: {
    loading: '@/components/loadding',
  },
  analytics:{
    baidu:"e9c37a6a4d4bd5c8437e4d0dc8e94fba"
  },
  // 配置路由懒加载
  // dynamicImport: {
  //   loading: '@/components/loadding'
  // },
  // 配置文件hash后缀，使用增量发布策略
  // hash: true,
  //增加数据埋点，百度统计
  // analytics: {
  //   baidu: '7d69c156050d3bf15c0375aa4dcdf79b'
  // }
});
