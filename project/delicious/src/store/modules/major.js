import { getMajorBanner,getMajorGoods , getMajorTabs ,MajorCityList} from '@/services'

const state = {
    banners: [],
    navList: [],
    products: [],
    worthList: [],
    navtab: [],
    goodsList: [],
    goodsCount: 0,
    goodsQuery: {
        tabId: 328,
        showPage: 1,
        showSort: 1,
        showSortMode: 1,
        pageNo: 1,
        pageSize: 10,
    },
    citylist:[]
};


const mutations = {
    update(state, payload) {
        for (let key in payload) {
            state[key] = payload[key];
        }
    }
};

const actions = {
    // 获取
    async getMajorBanner({ commit }, payload) {
        let result = await getMajorBanner();
        console.log("大牌优惠数据", result);
        if (result.errno === 0) {
            commit('update', {
                banners: result.data.page[1].params.data,
                navList: result.data.page[2].params.data,
                products: result.data.page[3].params,
                worthList: result.data.page[4].params,
            })
        }
    },
    // 导航tab切换
    async getMajorTabs({ commit }, payload) {
        let result = await getMajorTabs();
        if (result.errNo === 0) {
            commit('update', {
                navtab: result.data,
            })
        }
    },
    // 列表数据
    async getMajorGoods({ commit,state }, payload) {
        let result = await getMajorGoods(state.goodsQuery);
        if (result.errNo === 0) {
            let list=result.data.data
            if(state.goodsQuery.pageNo>1){
                list=[...state.goodsList,...list]
            }
            commit('update', {
                goodsList: list,
                goodsCount:result.data.totalCount
            })
        }
    },
    // 城市选择列表
    async MajorCityList({ commit }, payload) {
        let result = await MajorCityList();
        if (result.errNo === 0) {
            let arr=result.data.dataList.map((item,index)=>{
                return {...item,label:item.name,value:item.id}
            })
            arr.unshift({label:'全城',value:0})
            commit('update', {
                citylist: arr,
            })
        }
    },
};

export default {
    namespaced: true,
    state,
    mutations,
    actions
}