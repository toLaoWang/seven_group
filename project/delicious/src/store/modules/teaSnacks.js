import { getTeaSnacksClassifyList, getTeaSnacksShopList } from "@/services";
const state = { teaSnacksShopList: [], shotClassify: [], sname: "" };
const mutations = {
    upData(state, payload) {
        for (const key in payload) {
            state[key] = payload[key]
        }
    }
};
const actions = {
    async getTeaSnacksClassifyList({ commit }, payload) {
        let result = await getTeaSnacksClassifyList()
        console.log(result);
    },
    async getTeaSnacksShopList({ commit }, payload) {
        let result = await getTeaSnacksShopList()
        console.log(result);
        if (result.errno === 0) {
            result.data.sort.unshift({id:"01",name:"全部"})
            commit('upData', { teaSnacksShopList: result.data.list, shotClassify:result.data.sort,sname:result.data.sname })
        }
    }
};
export default {
    namespaced: true,
    state,
    mutations,
    actions,
};
