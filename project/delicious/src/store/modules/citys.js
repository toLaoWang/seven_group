import { getCityList } from '@/services'

const state = {
    citysList: [],  //全部城市数据
    searchCitys: []  //搜索城市数据
}

const mutations = {
    update(state, payload) {
        for (let key in payload) {
            state[key] = payload[key];
        }
    }
};

const actions = {
    async getCityList({ commit }, payload) {
        let result = await getCityList(payload);
        if (result.errNo === 0 && result.data.list.length) {
            commit('update', {
                searchCitys: result.data.list
            })
        } else {
            commit('update', {
                citysList: result.data
            })
        }
    },
};

export default {
    namespaced: true,
    state,
    mutations,
    actions
}