import {getUserId,getCoupon,getCouponDetail} from '@/services'


const state = {
  userId:'',
  couponList:[],
  couponDetailList:[]
};

const mutations = {
    update(state, payload){
        for (let key in payload) {
            state[key] = payload[key];
        }
    }
};

const actions = {
     // 获取用户id
     async getUserId({commit}){
        let result = await getUserId();
        if (result.errNo === 0) {
            commit('update', {
                userId: result.data
            })
        }
    },
     // 获取优惠卷
     async getCoupon({commit},payload){
        let result = await getCoupon(payload);
        if (result.errNo === 0) {
            commit('update', {
                couponList: result.data
            })
        }
    },
      // 获取优惠卷详情
      async getCouponDetail({commit},payload){
        let result = await getCouponDetail(payload);
        console.log(result,'2312453');
        if (result.errNo === 0) {
            commit('update', {
                couponDetailList: result.data
            })
        }
    },
   
};

export default {
    namespaced: true,
    state,
    mutations,
    actions
}