import Vuex from 'vuex';
import Vue from 'vue';
import createLogger from 'vuex/dist/logger';
// 引入子模块
import index from './modules/index'
import major from './modules/major'
import teaSnacks from './modules/teaSnacks'
import user from './modules/user';
import citys from './modules/citys'

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        index,major,teaSnacks,user,citys
    },
    plugins: [createLogger()]
});