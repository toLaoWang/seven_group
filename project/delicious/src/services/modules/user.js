import request from '@/utils/request';

//获取用户id
export function getUserId(){
    return request.post('/c_msh/mLife/users/userGet',{
        "checkMobile": 1
    })
}

//获取优惠卷

export function getCoupon(num){
    return request.post('/c_msh/mLife/coupon/user/list',{
       type:num,
       startid:0,
       size:20,
       pageNo:1
    })
}


//获取详情优惠卷数据


export function getCouponDetail(){
    return request.post('/c_msh/mLife/coupon/detail',{
        "couponId": 702,
    })
}