import request from '@/utils/request';

// 获取城市
export function getCityList(isFlag) {
    return request.post('/basic/app/getCityList', { isSearch: isFlag })
}
