import request from '@/utils/request';

// 首页轮播图
export function getBanner(){
    return request.get('/home/swiper')
}