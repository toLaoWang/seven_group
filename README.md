# 乔集业
## 2021.9.17

1. 文章阅读

- [轻量级滑动验证码插件](https://juejin.cn/post/7007615666609979400)
- [响应式原理](https://juejin.cn/post/7008799005480058911)

2. 源码阅读


3. leecode 刷题
- [数组中两元素的最大乘积](https://leetcode-cn.com/problems/maximum-product-of-two-elements-in-an-array/)
- [判断字符串的两半是否相似](https://leetcode-cn.com/problems/determine-if-string-halves-are-alike/)
- [在区间范围内统计奇数数目](https://leetcode-cn.com/problems/count-odd-numbers-in-an-interval-range/)

4. 项目进度
- [x] 大牌优惠筛选排序
## 2021.9.16

1. 文章阅读

- [vue插槽](https://juejin.cn/post/7008540545790246920)
- [302重定向绕过安全限制](https://juejin.cn/post/7009057607457439780)

2. 源码阅读


3. leecode 刷题
- [字符串的最大公因子](https://leetcode-cn.com/problems/greatest-common-divisor-of-strings/)
- [位1的个数](https://leetcode-cn.com/problems/number-of-1-bits/)
- [到目标元素的最小距离](https://leetcode-cn.com/problems/minimum-distance-to-the-target-element/)

4. 项目进度
- [x] 大牌优惠页面
## 2021.9.15

1. 文章阅读

- [css](https://juejin.cn/post/7007711498306846727)
- [js速记](https://juejin.cn/post/7007714885232492574)

2. 源码阅读


3. leecode 刷题
- [验证回文串](https://leetcode-cn.com/problems/valid-palindrome/)
- [一周中的第几天](https://leetcode-cn.com/problems/day-of-the-week/)
- [日期之间隔几天](https://leetcode-cn.com/problems/number-of-days-between-two-dates/)

4. 项目进度
## 2021.9.14

1. 文章阅读

- [vue响应式](https://juejin.cn/post/7007711364693098503)
- [小程序摄像头](https://juejin.cn/post/7007708654791032845)

2. 源码阅读


3. leecode 刷题
- [消失的数字](https://leetcode-cn.com/problems/missing-number-lcci/)
- [整数的各位积和之差](https://leetcode-cn.com/problems/subtract-the-product-and-sum-of-digits-of-an-integer/)
- [交替位二进制数](https://leetcode-cn.com/problems/binary-number-with-alternating-bits/)

4. 项目进度
## 2021.9.13

1. 文章阅读

- [深拷贝如何解决循环引用?](https://juejin.cn/post/7007702027564220423)
- [浅析URL](https://juejin.cn/post/7007706851307585549)

2. 源码阅读


3. leecode 刷题
- [字符串相乘](https://leetcode-cn.com/problems/multiply-strings/)
- [至少是其他数字两倍的最大数](https://leetcode-cn.com/problems/largest-number-at-least-twice-of-others/)
- [和为s的两个数字](https://leetcode-cn.com/problems/he-wei-sde-liang-ge-shu-zi-lcof/)

4. 项目进度
## 2021.9.12

1. 文章阅读

- [https简单介绍](https://juejin.cn/post/7006949449616326670)
- [装饰器模式](https://juejin.cn/post/7006973909857730568)

2. 源码阅读


3. leecode 刷题
- [最后一块石头的重量](https://leetcode-cn.com/problems/last-stone-weight/)
- [替换隐藏数字得到的最晚时间](https://leetcode-cn.com/problems/latest-time-by-replacing-hidden-digits/)
- [比较含退格的字符串](https://leetcode-cn.com/problems/backspace-string-compare/)

4. 项目进度
## 2021.9.10

1. 文章阅读

- [vue双向绑定](https://juejin.cn/post/7006226317481902116)
- [vue插槽](https://juejin.cn/post/7006209455763226660)

2. 源码阅读


3. leecode 刷题
- [最大数](https://leetcode-cn.com/problems/largest-number/)
- [最大数值](https://leetcode-cn.com/problems/maximum-lcci/)

4. 项目进度
## 2021.9.9

1. 文章阅读

- [for 循环不是目的，map 映射更有意义！](https://juejin.cn/post/7006077858338570270)
- [webpack热更新](https://juejin.cn/post/6844904008432222215)

2. 源码阅读


3. leecode 刷题
- [单调数列](https://leetcode-cn.com/problems/monotonic-array/)
- [赎金信](https://leetcode-cn.com/problems/ransom-note/)

4. 项目进度
- [x] 前端支付
## 2021.9.8

1. 文章阅读

- [cookie的弊端](https://juejin.cn/post/684490120270)
- [cookie、localStorage和sessionStorage 三者之间的区别](https://juejin.cn/post/6844903516826255373)

2. 源码阅读


3. leecode 刷题
- [ 删除字符串中的所有相邻重复项](https://leetcode-cn.com/problems/remove-all-adjacent-duplicates-in-string/)
- [作为子字符串出现在单词中的字符串数目](https://leetcode-cn.com/problems/number-of-strings-that-appear-as-substrings-in-word/)
- [2 的幂](https://leetcode-cn.com/problems/power-of-two/)

4. 项目进度
- [x] 页面编辑
## 2021.9.7

1. 文章阅读

- [svg](https://juejin.cn/post/684490393764077568799)
- [预加载和懒加载](https://juejin.cn/post/6844903614138286094)

2. 源码阅读


3. leecode 刷题
- [ 第一个只出现一次的字符](https://leetcode-cn.com/problems/di-yi-ge-zhi-chu-xian-yi-ci-de-zi-fu-lcof/)
- [检查两个字符串数组是否相等](https://leetcode-cn.com/problems/check-if-two-string-arrays-are-equivalent/)

4. 项目进度
- [x] 新建协同编辑

## 2021.9.6

1. 文章阅读

- [优雅降级和渐进降级](https://juejin.cn/post/6844903473700405262)
- [重绘和回流](https://juejin.cn/post/6844903569087266823)

2. 源码阅读


3. leecode 刷题
- [ 最大重复子字符串](https://leetcode-cn.com/problems/maximum-repeating-substring/)
- [最后一个单词的长度](https://leetcode-cn.com/problems/length-of-last-word/)

4. 项目进度
- [x] 新建页面

## 2021.9.5

1. 文章阅读

- [js严格模式](https://juejin.cn/post/6844903777539997710#heading-6)
- [事件代理](https://juejin.cn/post/6844904190280466440#heading-5)

2. 源码阅读


3. leecode 刷题
- [ 在排序数组中查找数字 I](https://leetcode-cn.com/problems/zai-pai-xu-shu-zu-zhong-cha-zhao-shu-zi-lcof/)
- [最大数值](https://leetcode-cn.com/problems/maximum-lcci/)

4. 项目进度
- [x] 编辑文章

## 2021.9.3

1. 文章阅读

- [函数的length是多少](https://juejin.cn/post/7003369591967596552)

- [继承方式有哪些](https://juejin.cn/post/7004019825924112391)

2. 源码阅读


3. leecode 刷题

- [“气球” 的最大数量](https://leetcode-cn.com/problems/maximum-number-of-balloons/)
- [找不同](https://leetcode-cn.com/problems/find-the-difference/)
- [检查整数及其两倍数是否存在](https://leetcode-cn.com/problems/check-if-n-and-its-double-exist/)

4. 项目进度

- [x] 新建文章
## 2021.9.2

1. 文章阅读

- [原型，原型链](https://juejin.cn/post/7003267384358207501)

- [位运算](https://juejin.cn/post/7003227296941211684)

2. 源码阅读


3. leecode 刷题

- [合并排序的数组](https://leetcode-cn.com/problems/sorted-merge-lcci/)
- [Fizz Buzz](https://leetcode-cn.com/problems/fizz-buzz/)
- [最富有客户的资产总量](https://leetcode-cn.com/problems/richest-customer-wealth/)

4. 项目进度

- [x] 分类管理
- [x] 标签管理
## 2021.9.1

1. 文章阅读

- [异步编程梳理](https://juejin.cn/post/7002456485674369054)

- [深拷贝和浅拷贝](https://juejin.cn/post/7002446425036423205)

2. 源码阅读


3. leecode 刷题

- [重新排列数组](https://leetcode-cn.com/problems/shuffle-the-array/)
- [删除回文子序列](https://leetcode-cn.com/problems/remove-palindromic-subsequences/)
- [字符串中的最大奇数](https://leetcode-cn.com/problems/largest-odd-number-in-string/)

4. 项目进度

- [x] 所有文章
## 2021.8.31

1. 文章阅读

- [JS垃圾回收机制](https://juejin.cn/post/7002475280577085477)

- [说说作用域](https://juejin.cn/post/7002425151014830088#heading-1)

2. 源码阅读


3. leecode 刷题

- [找出数组最大公约数](https://leetcode-cn.com/problems/find-greatest-common-divisor-of-array/)
- [有效的完全平方数](https://leetcode-cn.com/problems/valid-perfect-square/)
- [三个数的最大乘积](https://leetcode-cn.com/problems/maximum-product-of-three-numbers/)

4. 项目进度

- [x] 所有文章
## 2021.8.30

1. 文章阅读

- [JSON.stringify 的 “魅力” ](https://juejin.cn/post/7001454317450297380#heading-14)

- [js鼠标和键盘事件](https://juejin.cn/post/7001138780128149535)

2. 源码阅读


3. leecode 刷题

- [字符串中不同整数的数目](https://leetcode-cn.com/problems/number-of-different-integers-in-a-string/)
- [三除数](https://leetcode-cn.com/problems/three-divisors/)
- [反转字符串II](https://leetcode-cn.com/problems/reverse-string-ii/)

4. 项目进度

- [x] 页面管理
## 2021.8.29

1. 文章阅读

- [css-bem命名规范](https://juejin.cn/post/7001777832682586119#heading-1)

- [this指向判断](https://juejin.cn/post/7001769945713344525)

2. 源码阅读


3. leecode 刷题

- [将句子排序](https://leetcode-cn.com/problems/sorting-the-sentence/)
- [阶乘尾数](https://leetcode-cn.com/problems/factorial-zeros-lcci/)
- [学生出勤记录I](https://leetcode-cn.com/problems/student-attendance-record-i//)

4. 项目进度

- [x] 优化
## 2021.8.27

1. 文章阅读

- [缓存](https://juejin.cn/post/6993358764481085453)

- [那些年我不知道的typeScript中的问题！](https://juejin.cn/post/7001047557522604040)

2. 源码阅读


3. leecode 刷题

- [调整数组顺序使奇数位于偶数前面](https://leetcode-cn.com/problems/diao-zheng-shu-zu-shun-xu-shi-qi-shu-wei-yu-ou-shu-qian-mian-lcof/)
- [按既定顺序创建目标数组](https://leetcode-cn.com/problems/create-target-array-in-the-given-order/)
- [判断句子是否为全字母句](https://leetcode-cn.com/problems/check-if-the-sentence-is-pangram/)

4. 项目进度

- [x] 优化
## 2021.8.26

1. 文章阅读

- [大白话讲解js执行机制](https://www.jianshu.com/p/22641c97e351)

- [浅析10个HTTP常见的状态响应码！](https://zhuanlan.zhihu.com/p/145942853)

2. 源码阅读


3. leecode 刷题

- [最小的k个数](https://leetcode-cn.com/problems/zui-xiao-de-kge-shu-lcof/)
- [分割等和子集](https://leetcode-cn.com/problems/partition-equal-subset-sum/)
- [第k个缺失的正整数](https://leetcode-cn.com/problems/kth-missing-positive-number/)

4. 项目进度

- [x] 优化

## 2021.8.25

1. 文章阅读

- [Http常见请求头信息](https://zhuanlan.zhihu.com/p/77697467)

- [工作中常用的 JavaScript 函数片段](https://zhuanlan.zhihu.com/p/143862373)

2. 源码阅读


3. leecode 刷题

- [换酒问题](https://leetcode-cn.com/problems/water-bottles/)
- [不用加号的加法](https://leetcode-cn.com/problems/add-without-plus-lcci/)
- [有效的山脉数组](https://leetcode-cn.com/problems/valid-mountain-array/)

4. 项目进度

- [x] 路由懒加载
- [x] 优化

## 2021.8.24

1. 文章阅读

- [Event Loop事件循环机制](https://juejin.cn/post/6999914057771778056)

- [对象常用api](https://juejin.cn/post/6999887121775345701)

2. 源码阅读


3. leecode 刷题

- [通过翻转子数组使两个数组相等](https://leetcode-cn.com/problems/make-two-arrays-equal-by-reversing-sub-arrays/)
- [有序数组的平方](https://leetcode-cn.com/problems/squares-of-a-sorted-array/)
- [重复N次的元素](https://leetcode-cn.com/problems/n-repeated-element-in-size-2n-array/)

4. 项目进度

- [x] 打包上线
- [x] 功能完善

## 2021.8.23

1. 文章阅读

- [宏任务微任务](https://juejin.cn/post/6844904050543034376)

- [闭包](https://juejin.cn/post/6999510850709127182)

2. 源码阅读


3. leecode 刷题

- [汉明距离](https://leetcode-cn.com/problems/hamming-distance/)
- [数字的补数](https://leetcode-cn.com/problems/number-complement/)
- [字符串轮转](https://leetcode-cn.com/problems/string-rotation-lcci/)

4. 项目进度

- [x] 页面响应式排版
- [x] 功能完善
## 2021.8.22

1. 文章阅读

- [前端的深拷贝和浅拷贝](https://juejin.cn/post/6999096522973397022)

- [防抖和节流](https://blog.csdn.net/qq_41997592/article/details/119717669?utm_medium=distribute.pc_category.none-task-blog-hot-7.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-7.nonecase)

2. 源码阅读


3. leecode 刷题

- [最长连续递增序列](https://leetcode-cn.com/problems/longest-continuous-increasing-subsequence/)
- [解压缩编码列表](https://leetcode-cn.com/problems/decompress-run-length-encoded-list/)
- [判定字符是否唯一](https://leetcode-cn.com/problems/is-unique-lcci/)

4. 项目进度

- [x] 详情页分享、二维码、下载，api组件


## 2021.8.20

1. 文章阅读

- [TypeScript-类型系统深入](https://juejin.cn/post/6998040237217677325)

- [响应式布局](https://juejin.cn/post/6998355778943844365)

2. 源码阅读


3. leecode 刷题

- [阶乘后的0](https://leetcode-cn.com/problems/factorial-trailing-zeroes/)
- [两数之和II-输入有序数组](https://leetcode-cn.com/problems/two-sum-ii-input-array-is-sorted/)
- [Nim游戏](https://leetcode-cn.com/problems/nim-game/)

4. 项目进度

- [x] 详情页分享、二维码功能
- [x] 响应式布局


## 2021.8.19

1. 文章阅读

- [聊聊为什么 XMLHTTPRequest 不能跨域请求资源](https://juejin.cn/post/6997945638302253086)

- [ H5 移动端ios/Android兼容性技巧](https://juejin.cn/post/6997332019445235743?utm_source=gold_browser_extension)

2. 源码阅读


3. leecode 刷题

- [数组拆分l](https://leetcode-cn.com/problems/array-partition-i/)
- [第三大的数](https://leetcode-cn.com/problems/third-maximum-number/)
- [字符串中的单词数](https://leetcode-cn.com/problems/number-of-segments-in-a-string/)

4. 项目进度

- [x] 详情页喜欢功能
- [x] 详情页评论功能

## 2021.8.18

1. 文章阅读

- [ JS基础篇：12、搞懂防抖、节流](https://juejin.cn/post/6997697142353559560)

- [Vue的路由实现原理: hash模式和history模式](https://juejin.cn/post/6997143974339149854)

2. 源码阅读


3. leecode 刷题

- [转换小写字母](https://leetcode-cn.com/problems/to-lower-case/)
- [翻转字符串中的单词III](https://leetcode-cn.com/problems/reverse-words-in-a-string-iii/)
- [字符串中的第一个唯一字符](https://leetcode-cn.com/problems/first-unique-character-in-a-string/)

4. 项目进度

- [x] 搜索功能
- [x] 详情喜欢功能
- [x] 详情评论功能

## 2021.8.17

1. 文章阅读

- [ 开发利器 | 你真的会用 Chrome 吗 —— Console 篇（二）](https://juejin.cn/post/6997056968166735879)

- [React--9: 组件的三大核心属性2:props与构造器](https://juejin.cn/post/6997196252580413447)

2. 源码阅读


3. leecode 刷题

- [两个数组的交集](https://leetcode-cn.com/problems/intersection-of-two-arrays/)
- [两个数组的交集II](https://leetcode-cn.com/problems/intersection-of-two-arrays-ii/)
- [加一](https://leetcode-cn.com/problems/plus-one/)

4. 项目进度

- [x] 组件封装
- [x] 详情页面
- [x] 详情页目录 

## 2021.8.16

1. 文章阅读

- [ new 关键字](https://juejin.cn/post/6996958146270855199)

- [数组方法的总结与应用](https://juejin.cn/post/6996956498718883848)

2. 源码阅读


3. leecode 刷题

- [各位相加](https://leetcode-cn.com/problems/add-digits/)
- [4的幂](https://leetcode-cn.com/problems/power-of-four/)
- [反转字符串](https://leetcode-cn.com/problems/reverse-string/)

4. 项目进度
- [x] 右侧组件封装

- [x] 详情页渲染

## 2021.8.15

1. 文章阅读

- [ 前端模块化规范详细总结](https://juejin.cn/post/6996595779037036580)

- [BOM核心——window对象之窗口](https://juejin.cn/post/6996527739129823239)

2. 源码阅读


3. leecode 刷题

- [移除元素](https://leetcode-cn.com/problems/remove-element/)
- [合并两个有序数组](https://leetcode-cn.com/problems/merge-sorted-array/)
- [最后一个单词长度](https://leetcode-cn.com/problems/length-of-last-word/)

4. 项目进度

## 2021.8.13

1. 文章阅读

- [ this指向](https://juejin.cn/post/6994233558529212452#heading-1)

- [闭包](https://juejin.cn/post/6995710760928149535)

2. 源码阅读


3. leecode 刷题

- [x的平方根](https://leetcode-cn.com/problems/sqrtx/)

- [实现 strStr() 函数](https://leetcode-cn.com/problems/implement-strstr/)

- [回文数](https://leetcode-cn.com/problems/palindrome-number/)

4. 项目进度

## 2021.8.12

1. 文章阅读

- [Typescript之类型系统](https://juejin.cn/post/6995444589448396813)

- [TypeScript 函数](https://juejin.cn/post/6995466449766596621)

2. 源码阅读


3. leecode 刷题

- [旋转数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2skh7/)

- [移动0](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)

- [两数之和](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2jrse/)

4. 项目进度

## 2021.8.11

1. 文章阅读

- [类数组有没有forEach方法?](https://juejin.cn/post/6995092720322609188)

- [聊聊让人头疼的正则表达式](https://juejin.cn/post/6993898348168085534)

2. 源码阅读


3. leecode 刷题

- [只出现一次的数字](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/)

- [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)

- [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)


4. 项目进度



# 王宁

## 2021.9.14

1. 文章阅读
- [掌握JavaScript面试：什么是闭包？](https://juejin.cn/post/6874829017006997511#heading-5)
- [三张图搞懂JavaScript的原型对象与原型链](https://zhuanlan.zhihu.com/p/294808520)

1. 源码阅读
- [你不知道的js堆内存和栈内存](https://zhuanlan.zhihu.com/p/50206683)

3. leecode刷题
- [回文数](https://leetcode-cn.com/problems/palindrome-number/)
- [移动零](https://leetcode-cn.com/leetbook/read/all-about-array/x9rh8e/)
- [整数反转](https://leetcode-cn.com/leetbook/read/tencent/x5s2oj/)

4. 项目进度


## 2021.9.13

1. 文章阅读
- [掌握JavaScript面试：什么是闭包？](https://juejin.cn/post/6874829017006997511#heading-5)
- [三张图搞懂JavaScript的原型对象与原型链](https://zhuanlan.zhihu.com/p/294808520)

1. 源码阅读
- [你不知道的js堆内存和栈内存](https://zhuanlan.zhihu.com/p/50206683)

3. leecode刷题
- [回文数](https://leetcode-cn.com/problems/palindrome-number/)
- [移动零](https://leetcode-cn.com/leetbook/read/all-about-array/x9rh8e/)
- [整数反转](https://leetcode-cn.com/leetbook/read/tencent/x5s2oj/)

4. 项目进度
- [x] 评论排版细致
- [x] 个人中心排版完成

## 2021.9.12
1. 文章阅读

- [跨浏览器窗口通讯 ，7种方式](https://juejin.cn/post/7002012595200720927?utm_source=gold_browser_extension)
- [3个小工具](https://juejin.cn/post/7001998089938534437?utm_source=gold_browser_extension)

2. 源码阅读


3. leecode刷题
   
- [判定字符是否唯一](https://leetcode-cn.com/problems/is-unique-lcci/)
- [字符串压缩](https://leetcode-cn.com/problems/compress-string-lcci/)
  
4. 项目进度 
- [x] 评论页面细致排版
- [x] 评论功能完成

## 2021.9.10
1. 文章阅读

- [Pnpm: 最先进的包管理工具](https://juejin.cn/post/7001794162970361892?utm_source=gold_browser_extension)
- [Console 3000字完整指南，让你不只会用console.log !](https://juejin.cn/post/7001529656188862494?utm_source=gold_browser_extension)

2. 源码阅读


3. leecode刷题
   
- [回文数](https://leetcode-cn.com/problems/palindrome-number/submissions/)
- [实现 strStr()](https://leetcode-cn.com/problems/implement-strstr/)

4. 项目进度 
- [x] 评论页面细致排版
- [x] 评论功能完成


## 2021.9.9
1. 文章阅读

- [Pnpm: 最先进的包管理工具](https://juejin.cn/post/7001794162970361892?utm_source=gold_browser_extension)
- [Console 3000字完整指南，让你不只会用console.log !](https://juejin.cn/post/7001529656188862494?utm_source=gold_browser_extension)

2. 源码阅读


3. leecode刷题
   
- [回文数](https://leetcode-cn.com/problems/palindrome-number/submissions/)
- [实现 strStr()](https://leetcode-cn.com/problems/implement-strstr/)

4. 项目进度 


## 2021.9.8

1. 文章阅读
- [Vue：知道什么时候使用计算属性并不能提高性能吗](https://juejin.cn/post/7005336858049642527?utm_source=gold_browser_extension)
- [分割平衡字符串](https://juejin.cn/post/7004991104894402573?utm_source=gold_browser_extension)

2. 源码阅读


3. leecode刷题
- [重新排列数组](https://leetcode-cn.com/problems/shuffle-the-array/)
- [字符串中的最大奇数](https://leetcode-cn.com/problems/largest-odd-number-in-string/)

4. 项目进度
- [x] 新建部分


## 2021.9.7

1. 文章阅读
- [使用这些思路与技巧，我读懂了多个优秀的开源项目](https://juejin.cn/post/6887689159918485511)
- [使用mobx和mobx-react代替redux和react-redux](https://juejin.cn/post/6844903734384820232)

2. 源码阅读
- [jQuery](https://jquery.cuishifeng.cn/index.html)

3. leecode刷题
- [数组形式的整数加法](https://leetcode-cn.com/problems/add-to-array-form-of-integer/)
- [检查两个字符串数组是否相等](https://leetcode-cn.com/problems/check-if-two-string-arrays-are-equivalent/submissions/)

4. 项目进度
- [x] 规划后台管理功能
- [x] 路由搭配

## 2021.9.6

1. 文章阅读
- [【前端可视化】如何在React中优雅的使用ECharts](https://juejin.cn/post/7000551946029858830)
- [使用mobx和mobx-react代替redux和react-redux](https://juejin.cn/post/6844903734384820232)

2. 源码阅读
- [MobX](https://cn.mobx.js.org/)

3. leecode刷题
- [移除元素](https://leetcode-cn.com/problems/remove-element/)
- [移动零](https://leetcode-cn.com/leetbook/read/all-about-array/x9rh8e/)
- [整数反转](https://leetcode-cn.com/leetbook/read/tencent/x5s2oj/)

4. 项目进度
- [x] 规划后台管理功能
- [x] 路由搭配

## 2021.9.5

1. 文章阅读
- [JavaScript深浅拷贝](https://juejin.cn/post/6844903608010407944)
- [HTTP和HTTPS的区别](https://www.mahaixiang.cn/internet/1233.html)

2. 源码阅读
- [ES6有什么新特性](https://juejin.cn/post/6844903944104181767)

3. leecode刷题
- [字符串压缩](https://leetcode-cn.com/problems/compress-string-lcci/)
- [字符串轮转](https://leetcode-cn.com/problems/string-rotation-lcci/)
- [移除重复节点](https://leetcode-cn.com/problems/remove-duplicate-node-lcci/)

4. 项目进度
- [x] 规划后台管理功能
- [x] 路由搭配

## 2021.9.3

1. 文章阅读
- [掌握JavaScript面试：什么是闭包？](https://juejin.cn/post/6874829017006997511#heading-5)
- [三张图搞懂JavaScript的原型对象与原型链](https://zhuanlan.zhihu.com/p/294808520)

1. 源码阅读
- [你不知道的js堆内存和栈内存](https://zhuanlan.zhihu.com/p/50206683)

3. leecode刷题
- [回文数](https://leetcode-cn.com/problems/palindrome-number/)
- [移动零](https://leetcode-cn.com/leetbook/read/all-about-array/x9rh8e/)
- [整数反转](https://leetcode-cn.com/leetbook/read/tencent/x5s2oj/)

1. 项目进度
- [x] 留言板渲染完成
- [x] 留言板所有功能完成

## 2021.9.2
1. 文章阅读

- [使用这些思路与技巧，我读懂了多个优秀的开源项目](https://juejin.cn/post/6887689159918485511)
- [使用mobx和mobx-react代替redux和react-redux](https://juejin.cn/post/6844903734384820232)
  
2. 源码阅读


3. leecode刷题
   
- [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/tencent/xxynuj/)
- [字符串相乘](https://leetcode-cn.com/leetbook/read/tencent/xxbolr/)
  
4. 项目进度 
- [x] 折叠屏
- [x] 图片方大
- [x] 分页
- [x] 模糊搜索 

## 2021.9.1
1. 文章阅读

- [巧用CSS filter，让你的网站更加酷炫！](https://juejin.cn/post/7002829486806794276?utm_source=gold_browser_extension)
- [打包去掉 console.log](https://juejin.cn/post/7002867286205808670?utm_source=gold_browser_extension)

2. 源码阅读


3. leecode刷题
   
- [移动零](https://leetcode-cn.com/leetbook/read/all-about-array/x9rh8e/)
- [整数反转](https://leetcode-cn.com/leetbook/read/tencent/x5s2oj/)
  
4. 项目进度 
- [x] 删除


## 2021.8.31
1. 文章阅读

- [Promise 被玩出 48 种“花样”，深度解析10个常用模块](https://juejin.cn/post/6999804617320038408?utm_source=gold_browser_extension)
- [30 个有用的 Node.js NPM 包](https://juejin.cn/post/7002054481252728869?utm_source=gold_browser_extension)

2. 源码阅读


3. leecode刷题
   
- [三除数](https://leetcode-cn.com/problems/three-divisors/)
- [反转字符串II](https://leetcode-cn.com/problems/reverse-string-ii/)
  
4. 项目进度 
- [x] 分页
- [x] 遮罩层
- [x] 搜索

## 2021.8.30
1. 文章阅读

- [跨浏览器窗口通讯 ，7种方式](https://juejin.cn/post/7002012595200720927?utm_source=gold_browser_extension)
- [3个小工具](https://juejin.cn/post/7001998089938534437?utm_source=gold_browser_extension)

2. 源码阅读


3. leecode刷题
   
- [判定字符是否唯一](https://leetcode-cn.com/problems/is-unique-lcci/)
- [字符串压缩](https://leetcode-cn.com/problems/compress-string-lcci/)
  
4. 项目进度 
- [x] 渲染页面

## 2021.8.29
1. 文章阅读

- [Pnpm: 最先进的包管理工具](https://juejin.cn/post/7001794162970361892?utm_source=gold_browser_extension)
- [Console 3000字完整指南，让你不只会用console.log !](https://juejin.cn/post/7001529656188862494?utm_source=gold_browser_extension)

2. 源码阅读


3. leecode刷题
   
- [回文数](https://leetcode-cn.com/problems/palindrome-number/submissions/)
- [实现 strStr()](https://leetcode-cn.com/problems/implement-strstr/)

4. 项目进度 
- [x] 优化项目

## 2021.8.27
1. 文章阅读

- [深入浅出讲解 JS 的微任务与宏任务 | 8月更文挑战](https://juejin.cn/post/7001010325193506853?utm_source=gold_browser_extension)
- [HTTP发送多种数据的多部份对象集合](https://juejin.cn/post/7001046027872534559?utm_source=gold_browser_extension)

1. 源码阅读


2. leecode刷题
   
- [URL化](https://leetcode-cn.com/problems/string-to-url-lcci/)
- [字符串压缩](https://leetcode-cn.com/problems/compress-string-lcci/)

1. 项目进度 
- [x] 优化项目


## 2021.8.26
1. 文章阅读

- [我为什么坚持6点起床](https://juejin.cn/post/7000659708948250660?utm_source=gold_browser_extension)
- [前端中的领域驱动(DDD)](https://juejin.cn/post/7000675924106543118?utm_source=gold_browser_extension)

1. 源码阅读


2. leecode刷题
   
- [Fizz Buzz](https://leetcode-cn.com/problems/fizz-buzz/submissions/)
- [相对名次](https://leetcode-cn.com/problems/relative-ranks/)

1. 项目进度 
- [x] 优化项目

## 2021.8.25
1. 文章阅读

- [DIff算法看不懂就一起来砍我](https://juejin.cn/post/7000266544181674014?utm_source=gold_browser_extension)
- [前端9种图片格式基础知识](https://juejin.cn/post/7000154907156152327?utm_source=gold_browser_extension)

1. 源码阅读


2. leecode刷题
   
- [正则表达式匹配](https://leetcode-cn.com/problems/regular-expression-matching/)
- [ 三数之和](https://leetcode-cn.com/problems/3sum/)

4. 项目进度 
- [x] 优化项目

## 2021.8.24
1. 文章阅读

- [Vue中ref和$refs的用法与介绍](https://juejin.cn/post/6999901520057597960?utm_source=gold_browser_extension)
- [前端工程化实战 - 可配置的模板管理](https://juejin.cn/post/6999397309180182564?utm_source=gold_browser_extension)

2. 源码阅读


3. leecode刷题
   
- [数组形式的整数加法](https://leetcode-cn.com/problems/add-to-array-form-of-integer/)
- [检查两个字符串数组是否相等](https://leetcode-cn.com/problems/check-if-two-string-arrays-are-equivalent/submissions/)

4. 项目进度 
- [x] 打包上线


## 2021.8.23
1. 文章阅读

- [Vue 中不要用 index 作为 key](https://juejin.cn/post/6999552498411241479?utm_source=gold_browser_extension)
- [js中的闭包](https://blog.csdn.net/weixin_43558749/article/details/90905723)

2. 源码阅读
- [react-colorful](https://www.npmjs.com/package/react-colorful)

3. leecode刷题
   
- [最大数值](https://leetcode-cn.com/problems/maximum-lcci/)
- [翻转单词顺序](https://leetcode-cn.com/problems/fan-zhuan-dan-ci-shun-xu-lcof/)

4. 项目进度 
- [x] API组件
- [x] 排版

## 2021.8.22
1. 文章阅读

- [黑马头条：项目重点](https://juejin.cn/post/6999160779215732744?utm_source=gold_browser_extension)
- [整理JS一些常用的工具函数](https://juejin.cn/post/6999153377397456932?utm_source=gold_browser_extension)

2. 源码阅读
- [react-colorful](https://www.npmjs.com/package/react-colorful)

3. leecode刷题
   
- [各位相加](https://leetcode-cn.com/problems/add-digits/)
- [反转字符串](https://leetcode-cn.com/problems/reverse-string/)


4. 项目进度 
- [x] API组件
- [x] 排版



## 2021.8.20
1. 文章阅读


- [RESTful原则](https://juejin.cn/post/6998447641549864967?utm_source=gold_browser_extension#heading-6)
- [整理JS一些常用的工具函数](https://juejin.cn/post/6998438314210508807?utm_source=gold_browser_extension)

2. 源码阅读
- [自定义懒加载组件](https://juejin.cn/post/6998443880311947301?utm_source=gold_browser_extension)

3. leecode刷题
   
- [有效的括号](https://leetcode-cn.com/problems/valid-parentheses/)
- [只出现一次的数字](https://leetcode-cn.com/problems/single-number/)


4. 项目进度 
- [x] 楼层
- [x] 自适应


## 2021.8.19
1. 文章阅读

- [一文搞定闭包](https://juejin.cn/post/6998065046295314446?utm_source=gold_browser_extension)

2. 源码阅读
- [@umijs/plugin-access](https://umijs.org/zh-CN/plugins/plugin-access)

3. leecode刷题
   
- [移动零](https://leetcode-cn.com/leetbook/read/all-about-array/x9rh8e/)
- [整数反转](https://leetcode-cn.com/leetbook/read/tencent/x5s2oj/)


4. 项目进度 
- [x] 主题滑动右边高亮
- [x] 排版

## 2021.8.18
1. 文章阅读

- [居然不知道CSS能做3D？天空盒子了解一下，颠覆想象](https://juejin.cn/post/6997697496176820255?utm_source=gold_browser_extension)
- [面试官最喜欢问☞前端手撕代码](https://juejin.cn/post/6997682438373638157?utm_source=gold_browser_extension)

2. 源码阅读


3. leecode刷题
- [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/tencent/xxynuj/)
- [字符串相乘](https://leetcode-cn.com/leetbook/read/tencent/xxbolr/)
- [反转字符串](https://leetcode-cn.com/leetbook/read/tencent/xxj50s/)


1. 项目进度 
- [x] 右边模块的点击高亮
- [x] 右边模块的点击滑动


## 2021.8.17
1. 文章阅读

- [Typescript 类型的本质是什么](https://juejin.cn/post/6997465633432535047?utm_source=gold_browser_extension)
- [css乱七八糟实用但八成记不住的功能](https://juejin.cn/post/6997295012916510727?utm_source=gold_browser_extension)

2. 源码阅读


3. leecode刷题


4. 项目进度 
- [x] 用npm-highlight.js做页面的高亮
- [x] 页面排版

## 2021.8.16
1. 文章阅读

- [你不知道的 Chrome 调试技巧](https://juejin.cn/book/6844733783166418958)
- [前后端是如何交互的](https://jasonandjay.github.io/study/zh/standard/Cooperation.html#_1-前端请求数据url由谁来写)

2. 源码阅读

- [jQuery 2.0.3 源码分析 回调对象 - Callbacks](https://www.cnblogs.com/aaronjs/p/3342344.html)
3. leecode刷题

4. 项目进度 

- [x] 跳转详情页
- [x] 页面渲染
- [x] 页面排版

## 2021.8.15
1. 文章阅读

- [深入理解JS事件循环](https://juejin.cn/post/6996570687452217352?utm_source=gold_browser_extension)
- [TypeScript-接口](https://juejin.cn/post/6996559915732975630?utm_source=gold_browser_extension)

2. 源码阅读

- [自定义 Hook](https://react.docschina.org/docs/hooks-custom.html)
- [Hook API 索引](https://react.docschina.org/docs/hooks-reference.html)

3. leecode刷题

4. 项目进度 

- [x] 页面排版
- [x] 加hover



## 2021.8.13
1. 文章阅读

- [代码优化篇](https://juejin.cn/post/6995429777754570782?utm_source=gold_browser_extension)
- [React 性能优化：网络性能（自动化按需加载)](https://juejin.cn/post/6995000587620204574?utm_source=gold_browser_extension)

2. 源码阅读

- [umijs/plugin-dva](https://umijs.org/zh-CN/plugins/plugin-dva)
- [umijs/plugin-request](https://umijs.org/zh-CN/plugins/plugin-request)

3. leecode刷题

- [无重复字符的最长子串](https://leetcode-cn.com/explore/interview/card/bytedance/242/string/1012/)
- [最长公共前缀](https://leetcode-cn.com/explore/interview/card/bytedance/242/string/1014/)

4. 项目进度 

- [x] 搭建项目基本结构
- [x] 获取数据
- [x] 归档页面简单搭建 


## 2021.8.12

1. 文章阅读

- [函数式编程](https://juejin.cn/post/6995826553082871839?utm_source=gold_browser_extension)
- [关于作用域与作用域链](https://juejin.cn/post/6995824534821552159?utm_source=gold_browser_extension)

1. 源码阅读

- [react-native](https://reactnative.cn/docs/platform-specific-code)
- [react-router-config](https://www.npmjs.com/package/react-router-config)

1. leecode刷题

- [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)
- [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)
- [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)

4. 项目进度 


## 2021.8.11

1. 文章阅读

- [一文搞定vue3.0新特性-常用函数的使用与生命周期](https://juejin.cn/post/6994271464211873823?utm_source=gold_browser_extension)
- [从零学习微信小程序（一）——基础知识](https://juejin.cn/post/6994937563412693022?utm_source=gold_browser_extension)

2. 源码阅读

- [npm fs-extra](https://www.npmjs.com/package/fs-extra)
- [ReactElement](https://react.jokcy.me/book/api/react-element.html)

3. leecode刷题

- [两数相加](https://leetcode-cn.com/leetbook/read/tencent/xxqfy5/)
- [旋转数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2skh7/)
- [数组去重](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)
  
4. 项目进度 



# 葛志鹏
## 2021.9.17

1. 文章阅读

- [一文搞定vue3.0新特性-常用函数的使用与生命周期](https://juejin.cn/post/6994271464211873823?utm_source=gold_browser_extension)
- [从零学习微信小程序（一）——基础知识](https://juejin.cn/post/6994937563412693022?utm_source=gold_browser_extension)

2. 源码阅读

- [npm fs-extra](https://www.npmjs.com/package/fs-extra)
- [ReactElement](https://react.jokcy.me/book/api/react-element.html)

3. leecode刷题

- [两数相加](https://leetcode-cn.com/leetbook/read/tencent/xxqfy5/)
- [旋转数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2skh7/)
- [数组去重](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)
  
4. 项目进度 
- [x] 瀑布流排版细致化

## 2021.9.16

1. 文章阅读

- [vue响应式](https://juejin.cn/post/7007711364693098503)
- [小程序摄像头](https://juejin.cn/post/7007708654791032845)

2. 源码阅读


3. leecode 刷题
- [消失的数字](https://leetcode-cn.com/problems/missing-number-lcci/)
- [整数的各位积和之差](https://leetcode-cn.com/problems/subtract-the-product-and-sum-of-digits-of-an-integer/)
- [交替位二进制数](https://leetcode-cn.com/problems/binary-number-with-alternating-bits/)

1. 项目进度
- [x] 首页排版到瀑布流

## 2021.9.8

1. 文章阅读
- [Promise不会？？看这里！！！史上最通俗易懂的Promise！！！](https://juejin.cn/post/6844903607968481287)
- [优雅的实现React中的动画过渡](https://juejin.cn/post/6877773443715203079#heading-7)

1. 源码阅读
- [UmiJS开发技巧](https://juejin.cn/post/6844904198329335822)

3. leecode刷题
- [判定字符是否唯一](https://leetcode-cn.com/problems/is-unique-lcci/)
- [判定是否互为字符重排](https://leetcode-cn.com/problems/check-permutation-lcci/)
- [URL化](https://leetcode-cn.com/problems/string-to-url-lcci/)

1. 项目进度
- [x] 个人中心排版完成
- [x] 评论排版细致化

## 2021.9.7

1. 文章阅读
- [掌握JavaScript面试：什么是闭包？](https://juejin.cn/post/6874829017006997511#heading-5)
- [三张图搞懂JavaScript的原型对象与原型链](https://zhuanlan.zhihu.com/p/294808520)

1. 源码阅读
- [你不知道的js堆内存和栈内存](https://zhuanlan.zhihu.com/p/50206683)

3. leecode刷题
- [回文数](https://leetcode-cn.com/problems/palindrome-number/)
- [移动零](https://leetcode-cn.com/leetbook/read/all-about-array/x9rh8e/)
- [整数反转](https://leetcode-cn.com/leetbook/read/tencent/x5s2oj/)

4. 项目进度
- [x] 评论排版细致
- [x] 个人中心排版完成

## 2021.9.6
1. 文章阅读

- [跨浏览器窗口通讯 ，7种方式](https://juejin.cn/post/7002012595200720927?utm_source=gold_browser_extension)
- [3个小工具](https://juejin.cn/post/7001998089938534437?utm_source=gold_browser_extension)

2. 源码阅读


3. leecode刷题
   
- [判定字符是否唯一](https://leetcode-cn.com/problems/is-unique-lcci/)
- [字符串压缩](https://leetcode-cn.com/problems/compress-string-lcci/)
  
4. 项目进度 
- [x] 评论页面细致排版
- [x] 评论功能完成

## 2021.9.5
1. 文章阅读

- [Pnpm: 最先进的包管理工具](https://juejin.cn/post/7001794162970361892?utm_source=gold_browser_extension)
- [Console 3000字完整指南，让你不只会用console.log !](https://juejin.cn/post/7001529656188862494?utm_source=gold_browser_extension)

2. 源码阅读


3. leecode刷题
   
- [回文数](https://leetcode-cn.com/problems/palindrome-number/submissions/)
- [实现 strStr()](https://leetcode-cn.com/problems/implement-strstr/)

4. 项目进度 
- [x] 评论页面细致排版
- [x] 评论功能完成

## 2021.9.3
1. 文章阅读

- [深入浅出讲解 JS 的微任务与宏任务 | 8月更文挑战](https://juejin.cn/post/7001010325193506853?utm_source=gold_browser_extension)
- [HTTP发送多种数据的多部份对象集合](https://juejin.cn/post/7001046027872534559?utm_source=gold_browser_extension)

1. 源码阅读


2. leecode刷题
   
- [URL化](https://leetcode-cn.com/problems/string-to-url-lcci/)
- [字符串压缩](https://leetcode-cn.com/problems/compress-string-lcci/)

1. 项目进度 
- [x] 工作台功能完成
- [x] 评论排版完成

## 2021.9.2

1. 文章阅读
- [JavaScript深浅拷贝](https://juejin.cn/post/6844903608010407944)
- [HTTP和HTTPS的区别](https://www.mahaixiang.cn/internet/1233.html)

2. 源码阅读
- [你不知道的js堆内存和栈内存](https://zhuanlan.zhihu.com/p/50206683)

3. leecode刷题
- [字符串压缩](https://leetcode-cn.com/problems/compress-string-lcci/)
- [字符串轮转](https://leetcode-cn.com/problems/string-rotation-lcci/)
- [移除重复节点](https://leetcode-cn.com/problems/remove-duplicate-node-lcci/)

4. 项目进度
- [x] 规划后台管理功能
- [x] 路由搭配

## 2021.8.31

1. 文章阅读
- [使用这些思路与技巧，我读懂了多个优秀的开源项目](https://juejin.cn/post/6887689159918485511)
- [使用mobx和mobx-react代替redux和react-redux](https://juejin.cn/post/6844903734384820232)

2. 源码阅读
- [jQuery](https://jquery.cuishifeng.cn/index.html)

3. leecode刷题
- [数组形式的整数加法](https://leetcode-cn.com/problems/add-to-array-form-of-integer/)
- [检查两个字符串数组是否相等](https://leetcode-cn.com/problems/check-if-two-string-arrays-are-equivalent/submissions/)

4. 项目进度
- [x] 规划后台管理功能
- [x] 路由搭配

## 2021.8.30

1. 文章阅读
- [【前端可视化】如何在React中优雅的使用ECharts](https://juejin.cn/post/7000551946029858830)
- [使用mobx和mobx-react代替redux和react-redux](https://juejin.cn/post/6844903734384820232)

2. 源码阅读
- [MobX](https://cn.mobx.js.org/)

3. leecode刷题
- [移除元素](https://leetcode-cn.com/problems/remove-element/)
- [移动零](https://leetcode-cn.com/leetbook/read/all-about-array/x9rh8e/)
- [整数反转](https://leetcode-cn.com/leetbook/read/tencent/x5s2oj/)

4. 项目进度
- [x] 规划后台管理功能
- [x] 路由搭配

## 2021.8.29

1. 文章阅读
- [JavaScript深浅拷贝](https://juejin.cn/post/6844903608010407944)
- [HTTP和HTTPS的区别](https://www.mahaixiang.cn/internet/1233.html)

2. 源码阅读
- [ES6有什么新特性](https://juejin.cn/post/6844903944104181767)

3. leecode刷题
- [字符串压缩](https://leetcode-cn.com/problems/compress-string-lcci/)
- [字符串轮转](https://leetcode-cn.com/problems/string-rotation-lcci/)
- [移除重复节点](https://leetcode-cn.com/problems/remove-duplicate-node-lcci/)

4. 项目进度
- [x] 规划后台管理功能
- [x] 路由搭配

## 2021.8.27

1. 文章阅读
- [掌握JavaScript面试：什么是闭包？](https://juejin.cn/post/6874829017006997511#heading-5)
- [三张图搞懂JavaScript的原型对象与原型链](https://zhuanlan.zhihu.com/p/294808520)

1. 源码阅读
- [你不知道的js堆内存和栈内存](https://zhuanlan.zhihu.com/p/50206683)

3. leecode刷题
- [回文数](https://leetcode-cn.com/problems/palindrome-number/)
- [移动零](https://leetcode-cn.com/leetbook/read/all-about-array/x9rh8e/)
- [整数反转](https://leetcode-cn.com/leetbook/read/tencent/x5s2oj/)

1. 项目进度
- [x] 留言板渲染完成
- [x] 留言板所有功能完成
## 2021.8.26

1. 文章阅读
- [单页面应用开发](https://jasonandjay.github.io/study/zh/standard/Spa.html#mpa%E4%B8%8Espa%E7%AE%80%E4%BB%8B)
- [路由传参的三种方法](https://www.jianshu.com/p/c63372225cc8)

1. 源码阅读
- [devServer.historyApiFallback](https://webpack.docschina.org/configuration/dev-server/#devserverhistoryapifallback)

3. leecode刷题
- [只出现一次的数字](https://leetcode-cn.com/problems/single-number/)
- [正则表达式匹配](https://leetcode-cn.com/problems/regular-expression-matching/)
- [ Nim 游戏](https://leetcode-cn.com/problems/nim-game/)

1. 项目进度
- [x] 留言板渲染完成
- [x] 留言板所有功能完成
## 2021.8.25

1. 文章阅读
- [浏览器的缓存](https://jasonandjay.github.io/study/zh/standard/Cache.html#概念)
- [整理给自己的React Hooks 思想和用法笔记](https://juejin.cn/post/6976793738454499359#heading-4)

1. 源码阅读
- [Vue 3.2 发布了，那尤雨溪是怎么发布 Vue.js 的？](https://juejin.cn/post/6997943192851054606?utm_source=gold_browser_extension)

3. leecode刷题
- [反转字符串中的单词 III](https://leetcode-cn.com/problems/reverse-words-in-a-string-iii/)
- [反转字符串](https://leetcode-cn.com/problems/reverse-string/)
- [ Nim 游戏](https://leetcode-cn.com/problems/nim-game/)

1. 项目进度
- [x] 留言板渲染完成
- [x] 留言板所有功能完成
## 2021.8.24

1. 文章阅读
- [整理给自己的React Hooks 思想和用法笔记](https://juejin.cn/post/6976793738454499359)
- [如何正确理解栈和堆？](https://juejin.cn/post/6844903810171666446)

1. 源码阅读
- [Vue 3.2 发布了，那尤雨溪是怎么发布 Vue.js 的？](https://juejin.cn/post/6997943192851054606?utm_source=gold_browser_extension)

3. leecode刷题
- [寻找两个正序数组的中位数](https://leetcode-cn.com/problems/median-of-two-sorted-arrays/)
- [正则表达式匹配](https://leetcode-cn.com/problems/regular-expression-matching/)
- [ 三数之和](https://leetcode-cn.com/problems/3sum/)

1. 项目进度
- [x] 留言板渲染完成
- [x] 留言板所有功能完成
## 2021.8.23

1. 文章阅读
- [React Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html#简介)
- [React常用性能优化方式整理](https://blog.csdn.net/weixin_41902031/article/details/80353134)

1. 源码阅读
- [我从来不理解JavaScript闭包，直到有人这样向我解释它](https://juejin.cn/post/6844903858636849159)

3. leecode刷题
- [字符串压缩](https://leetcode-cn.com/problems/compress-string-lcci/)
- [字符串轮转](https://leetcode-cn.com/problems/string-rotation-lcci/)
- [移除重复节点](https://leetcode-cn.com/problems/remove-duplicate-node-lcci/)

1. 项目进度
- [x] 留言板渲染完成
- [x] 留言板所有功能完成
## 2021.8.22

1. 文章阅读
- [Promise不会？？看这里！！！史上最通俗易懂的Promise！！！](https://juejin.cn/post/6844903607968481287)
- [优雅的实现React中的动画过渡](https://juejin.cn/post/6877773443715203079#heading-7)

1. 源码阅读
- [UmiJS开发技巧](https://juejin.cn/post/6844904198329335822)

3. leecode刷题
- [判定字符是否唯一](https://leetcode-cn.com/problems/is-unique-lcci/)
- [判定是否互为字符重排](https://leetcode-cn.com/problems/check-permutation-lcci/)
- [URL化](https://leetcode-cn.com/problems/string-to-url-lcci/)

1. 项目进度
- [x] 留言板渲染完成
- [x] 留言板所有功能完成
## 2021.8.20

1. 文章阅读
- [带你入门react+dva+umi——从0到1保姆级教学(2)](https://juejin.cn/post/6983855069850501127)
- [UmiJs初认识](https://juejin.cn/post/6946130973801070605)

1. 源码阅读
- [精读《@umijs/use-request》源码](https://juejin.cn/post/6844904161583054855)

3. leecode刷题
- [移动零](https://leetcode-cn.com/leetbook/read/all-about-array/x9rh8e/)
- [整数反转](https://leetcode-cn.com/leetbook/read/tencent/x5s2oj/)

4. 项目进度
- [x] 留言板渲染完成
- [x] 留言板所有功能完成
## 2021.8.19

1. 文章阅读
- [Promise不会？？看这里！！！史上最通俗易懂的Promise！！！](https://juejin.cn/post/6844903607968481287)
- [UmiJS开发技巧](https://juejin.cn/post/6844904198329335822)

1. 源码阅读
- [精读《@umijs/use-request》源码](https://juejin.cn/post/6844904161583054855)

3. leecode刷题
- [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/tencent/xxynuj/)
- [字符串相乘](https://leetcode-cn.com/leetbook/read/tencent/xxbolr/)
- [反转字符串](https://leetcode-cn.com/leetbook/read/tencent/xxj50s/)

4. 项目进度
- [x] 留言板渲染完成
- [x] 留言板所有功能完成
## 2021.8.18

1. 文章阅读
- [类型“Element”上不存在属性“style”](https://blog.csdn.net/weixin_38883338/article/details/101517020)
- [UmiJS开发技巧](https://juejin.cn/post/6844904198329335822)

1. 源码阅读
- [精读《@umijs/use-request》源码](https://juejin.cn/post/6844904161583054855)

3. leecode刷题
- [整数转罗马数字](https://leetcode-cn.com/problems/integer-to-roman/)
- [移除元素](https://leetcode-cn.com/problems/remove-element/)
- [通配符匹配](https://leetcode-cn.com/problems/wildcard-matching/)

4. 项目进度
- [x] 留言板渲染完成
- [x] 留言板回复完成并排版
## 2021.8.17

1. 文章阅读
- [前端怎么优雅的写回调函数？](https://segmentfault.com/q/1010000022880990)
- [js实现点击评论进行显示回复框](https://www.cnblogs.com/lizhaoyao/p/8617436.html)

1. 源码阅读
- [@umijs/plugin-dva](https://umijs.org/zh-CN/plugins/plugin-dva)

3. leecode刷题
- [2的幂](https://leetcode-cn.com/leetbook/read/tencent/x5yjhd/)
- [子集](https://leetcode-cn.com/leetbook/read/tencent/x5bzb3/)
- [括号生成](https://leetcode-cn.com/leetbook/read/tencent/x5ku2e/)

4. 项目进度
- [x] 留言板数据请求
- [x] 留言板渲染完成
## 2021.8.16

1. 文章阅读
- [前端怎么优雅的写回调函数？](https://segmentfault.com/q/1010000022880990)
- [js实现点击评论进行显示回复框](https://www.cnblogs.com/lizhaoyao/p/8617436.html)

1. 源码阅读
- [@umijs/plugin-dva](https://umijs.org/zh-CN/plugins/plugin-dva)

3. leecode刷题
- [2的幂](https://leetcode-cn.com/leetbook/read/tencent/x5yjhd/)
- [子集](https://leetcode-cn.com/leetbook/read/tencent/x5bzb3/)
- [括号生成](https://leetcode-cn.com/leetbook/read/tencent/x5ku2e/)

4. 项目进度
- [x] 留言板数据请求
- [x] 留言板渲染完成
## 2021.8.15
1. 文章阅读
- [使用 State Hook](https://react.docschina.org/docs/hooks-state.html)
- [响应式布局和自适应布局详解](http://caibaojian.com/356.html)

1. 源码阅读
- [Hook 概览](https://react.docschina.org/docs/hooks-overview.html)

3. leecode刷题
- [存在重复元素](https://leetcode-cn.com/leetbook/read/tencent/x5h4n3/)
- [反转字符串中的单词 III](https://leetcode-cn.com/leetbook/read/tencent/xxjfdd/)
- [整数反转](https://leetcode-cn.com/leetbook/read/tencent/x5s2oj/)

4. 项目进度
## 2021.8.12

1. 文章阅读
- [你不知道的 Chrome 调试技巧](https://juejin.cn/book/6844733783166418958)
- [前后端是如何交互的](https://jasonandjay.github.io/study/zh/standard/Cooperation.html#_1-前端请求数据url由谁来写)

2. 源码阅读
- [webpack4的简易配置](https://jasonandjay.github.io/study/zh/webpack/)

3. leecode刷题
- [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/tencent/xxynuj/)
- [字符串相乘](https://leetcode-cn.com/leetbook/read/tencent/xxbolr/)
- [反转字符串](https://leetcode-cn.com/leetbook/read/tencent/xxj50s/)

4. 项目进度

## 2021.8.11

1. 文章阅读
- [git 在windows上 生成ssh公钥](https://www.cnblogs.com/summer_shao/p/4174307.html)
- [git 环境和git常用的操作命令](https://jasonandjay.github.io/study/zh/standard/Start.html#git环境)

2. 源码阅读
- [WePY 微信小程序：生命周期](https://www.jianshu.com/p/2e48f2468d5f)

3. leecode刷题
- [两数之和](https://leetcode-cn.com/leetbook/read/tencent/xxqfy5/)
- [移动零](https://leetcode-cn.com/leetbook/read/all-about-array/x9rh8e/)
- [寻找两个正序数组的中位数](https://leetcode-cn.com/leetbook/read/tencent/xx6c46/)

4. 项目进度

# 王浩
## 2021.9.17

1. 文章阅读

- [vue响应式原理](https://juejin.cn/post/7008799005480058911)

2. 源码阅读


3. leecode 刷题
- [数组中两元素的最大乘积](https://leetcode-cn.com/problems/maximum-product-of-two-elements-in-an-array/)
- [判断字符串的两半是否相似](https://leetcode-cn.com/problems/determine-if-string-halves-are-alike/)
- [在区间范围内统计奇数数目](https://leetcode-cn.com/problems/count-odd-numbers-in-an-interval-range/)

4. 项目进度
- [x] 茶饮小吃筛选排序
## 2021.9.16

1. 文章阅读

- [vue插槽](https://juejin.cn/post/7008540545790246920)
- [302重定向绕过安全限制](https://juejin.cn/post/7009057607457439780)

2. 源码阅读


3. leecode 刷题
- [字符串的最大公因子](https://leetcode-cn.com/problems/greatest-common-divisor-of-strings/)
- [位1的个数](https://leetcode-cn.com/problems/number-of-1-bits/)
- [到目标元素的最小距离](https://leetcode-cn.com/problems/minimum-distance-to-the-target-element/)

4. 项目进度
- [x] 茶饮小吃页面
## 2021.9.15

1. 文章阅读

- [VUE3文档梳理](https://juejin.cn/post/7007695483908915213)

2. 源码阅读


3. leecode 刷题
- [验证回文串](https://leetcode-cn.com/problems/valid-palindrome/)
- [一周中的第几天](https://leetcode-cn.com/problems/day-of-the-week/)
- [日期之间隔几天](https://leetcode-cn.com/problems/number-of-days-between-two-dates/)

4. 项目进度
## 2021.9.14

1. 文章阅读

- [vue响应式](https://juejin.cn/post/7007711364693098503)

2. 源码阅读


3. leecode 刷题
- [消失的数字](https://leetcode-cn.com/problems/missing-number-lcci/)
- [整数的各位积和之差](https://leetcode-cn.com/problems/subtract-the-product-and-sum-of-digits-of-an-integer/)
- [交替位二进制数](https://leetcode-cn.com/problems/binary-number-with-alternating-bits/)

4. 项目进度
## 2021.9.13

1. 文章阅读

- [vue3脑图](https://juejin.cn/post/7007710727725121566)

2. 源码阅读


3. leecode 刷题
- [字符串相乘](https://leetcode-cn.com/problems/multiply-strings/)
- [至少是其他数字两倍的最大数](https://leetcode-cn.com/problems/largest-number-at-least-twice-of-others/)
- [和为s的两个数字](https://leetcode-cn.com/problems/he-wei-sde-liang-ge-shu-zi-lcof/)

4. 项目进度
## 2021.9.12

1. 文章阅读

- [订阅发布模式](https://juejin.cn/post/7006944184967888909)

2. 源码阅读


3. leecode 刷题
- [最后一块石头的重量](https://leetcode-cn.com/problems/last-stone-weight/)

- [最大数值](https://leetcode-cn.com/problems/maximum-lcci/)

4. 项目进度

## 2021.9.10

1. 文章阅读

- [react源码](https://juejin.cn/post/7006226317481902116)

2. 源码阅读


3. leecode 刷题
- [ 第一个只出现一次的字符](https://leetcode-cn.com/problems/di-yi-ge-zhi-chu-xian-yi-ci-de-zi-fu-lcof/)
- [ 删除字符串中的所有相邻重复项](https://leetcode-cn.com/problems/remove-all-adjacent-duplicates-in-string/)

4. 项目进度
## 2021.9.9

1. 文章阅读

- [webpack按需加载](https://juejin.cn/post/6844903718387875847)

2. 源码阅读


3. leecode 刷题
- [寻找两个正序数组的中位数](https://leetcode-cn.com/problems/median-of-two-sorted-arrays/)
- [汉明距离](https://leetcode-cn.com/problems/hamming-distance/)

4. 项目进度
- [x] 前端支付
## 2021.9.8

1. 文章阅读

- [cookie、localStorage和sessionStorage](https://juejin.cn/post/6844903516826255373)

2. 源码阅读


3. leecode 刷题
- [整数转罗马数字](https://leetcode-cn.com/problems/integer-to-roman/)
- [括号生成](https://leetcode-cn.com/leetbook/read/tencent/x5ku2e/)
- [2 的幂](https://leetcode-cn.com/problems/power-of-two/)

4. 项目进度
- [x] 页面编辑

## 2021.9.7

1. 文章阅读

- [深入浅出TypeScript：从基础知识到类型编程](https://juejin.cn/book/6844733813021491207/section)

2. 源码阅读

3. leecode刷题

- [两个数组的交集](https://leetcode-cn.com/problems/intersection-of-two-arrays/)
- [ 斐波那契数列](https://leetcode-cn.com/problems/fibonacci-number/)

4. 项目进度

-  优化文件管理页面

## 2021.9.6

1. 文章阅读

- [使用mobx和mobx-react代替redux和react-redux](https://juejin.cn/post/6844903734384820232)

2. 源码阅读

- [MobX](https://cn.mobx.js.org/)

3. leecode刷题

- [整数反转](https://leetcode-cn.com/leetbook/read/tencent/x5s2oj/)

4. 项目进度

-  优化邮件管理页面

## 2021.9.5

1. 文章阅读

- [JavaScript深浅拷贝](https://juejin.cn/post/6844903608010407944)
- [HTTP和HTTPS的区别](https://www.mahaixiang.cn/internet/1233.html)

2. 源码阅读

3. leecode刷题

- [字符串压缩](https://leetcode-cn.com/problems/compress-string-lcci/)
- [移除重复节点](https://leetcode-cn.com/problems/remove-duplicate-node-lcci/)

4. 项目进度

- 优化系统设置

## 2021.9.3

1. 文章阅读

- [深入浅出TypeScript：从基础知识到类型编程](https://juejin.cn/book/6844733813021491207/section)

2. 源码阅读

3. leecode刷题

- [移动零](https://leetcode-cn.com/leetbook/read/all-about-array/x9rh8e/)
- [整数反转](https://leetcode-cn.com/leetbook/read/tencent/x5s2oj/)

4. 项目进度

-  系统设置
## 2021.9.2

1. 文章阅读

- [面试题](https://juejin.cn/post/7002456485674369054)


2. 源码阅读


3. leecode 刷题

- [删除数组中第K大的数字](https://leetcode-cn.com/problems/xx4gT2/)
- [拥有最多糖果的孩子](https://leetcode-cn.com/problems/kids-with-the-greatest-number-of-candies/)
- [减小和重新排列数组后的最大元素](https://leetcode-cn.com/problems/maximum-element-after-decreasing-and-rearranging/)

4. 项目进度

- [x] 系统设置
## 2021.9.1

1. 文章阅读

- [面试题](https://juejin.cn/post/7002456485674369054)


2. 源码阅读


3. leecode 刷题

- [整数转罗马数字](https://leetcode-cn.com/problems/integer-to-roman/)
- [删除回文子序列](https://leetcode-cn.com/problems/remove-palindromic-subsequences/)
- [判定是否互为字符重排](https://leetcode-cn.com/problems/check-permutation-lcci/)

4. 项目进度

- [x] 邮件管理

## 2021.8.31

1. 文章阅读

- [值传递和引用传递](https://juejin.cn/post/7002475280577085477)

2. 源码阅读


3. leecode 刷题

- [最小的k个数](https://leetcode-cn.com/problems/zui-xiao-de-kge-shu-lcof/)
- [分割等和子集](https://leetcode-cn.com/problems/partition-equal-subset-sum/)
- [第k个缺失的正整数](https://leetcode-cn.com/problems/kth-missing-positive-number/)

4. 项目进度

- [x] 登陆注册
## 2021.8.30

1. 文章阅读

- [mobx](https://www.npmjs.com/package/mobx)

2. 源码阅读


3. leecode 刷题

- [判断句子是否为全字母句](https://leetcode-cn.com/problems/check-if-the-sentence-is-pangram/)
- [字符串中的单词数](https://leetcode-cn.com/problems/number-of-segments-in-a-string/)
- [回文数](https://leetcode-cn.com/problems/palindrome-number/)

4. 项目进度

- [x] 登陆注册
## 2021.8.29

1. 文章阅读

- [V8垃圾回收机制](https://jasonandjay.github.io/study/zh/standard/Spa.html#mpa%E4%B8%8Espa%E7%AE%80%E4%BB%8B)

2. 源码阅读


3. leecode 刷题

- [字符串相乘](https://leetcode-cn.com/leetbook/read/tencent/xxbolr/)
- [括号生成](https://leetcode-cn.com/leetbook/read/tencent/x5ku2e/)
- [学生出勤记录](https://leetcode-cn.com/problems/student-attendance-record-i//)

4. 项目进度

- [x] 优化
## 2021.8.27

1. 文章阅读
- [闭包](https://jasonandjay.github.io/study/zh/standard/Spa.html#mpa%E4%B8%8Espa%E7%AE%80%E4%BB%8B)

2. 源码阅读

3. leecode刷题
- [实现 strStr() 函数](https://leetcode-cn.com/problems/implement-strstr/)
- [正则表达式匹配](https://leetcode-cn.com/problems/regular-expression-matching/)
- [各位相加](https://leetcode-cn.com/problems/add-digits/)

1. 项目进度
- [x] 留言板渲染完成
- [x] 留言板所有功能完成
## 2021.8.26

1. 文章阅读
- [generator函数](https://segmentfault.com/q/1010000022880990)

1. 源码阅读

3. leecode刷题
- [正则表达式匹配](https://leetcode-cn.com/problems/regular-expression-matching/)
- [判定字符是否唯一](https://leetcode-cn.com/problems/is-unique-lcci/)
- [字符串压缩](https://leetcode-cn.com/problems/compress-string-lcci/)
4. 项目进度
- [x] 优化
## 2021.8.25

1. 文章阅读
- [前端怎么优雅的写回调函数？](https://segmentfault.com/q/1010000022880990)
- [js实现点击评论进行显示回复框](https://www.cnblogs.com/lizhaoyao/p/8617436.html)

1. 源码阅读
- [@umijs/plugin-dva](https://umijs.org/zh-CN/plugins/plugin-dva)

3. leecode刷题
- [2的幂](https://leetcode-cn.com/leetbook/read/tencent/x5yjhd/)
- [子集](https://leetcode-cn.com/leetbook/read/tencent/x5bzb3/)
- [括号生成](https://leetcode-cn.com/leetbook/read/tencent/x5ku2e/)

4. 项目进度
- [x] 优化

## 2021.8.24

1. 文章阅读

- [深入浅出TypeScript：从基础知识到类型编程]([新建标签页 (csdn.net)](https://juejin.cn/book/6844733813021491207)) 

2. 源码阅读


3. leecode 刷题

- [移动零](https://leetcode-cn.com/leetbook/read/all-about-array/x9rh8e/)
- [有序数组的平方](https://leetcode-cn.com/problems/squares-of-a-sorted-array/)
- [2的幂](https://leetcode-cn.com/leetbook/read/tencent/x5yjhd/)

4. 项目进度

- [x] 打包上线

## 2021.8.23

1. 文章阅读

- [深入浅出TypeScript：从基础知识到类型编程]([新建标签页 (csdn.net)](https://juejin.cn/book/6844733813021491207)) 

2. 源码阅读

3. leecode 刷题

- [杨辉三角形](https://leetcode-cn.com/problems/pascals-triangle/submissions/)
- [快乐数](https://leetcode-cn.com/problems/happy-number/submissions/)

4. 项目进度

- [√]  功能完善，排版完善

## 2021.8.22

1. 文章阅读

- [es6]([新建标签页 (csdn.net)](https://www.jianshu.com/p/ac1787f6c50f)) 

2. 源码阅读

3. leecode 刷题

- [有效括号](https://leetcode-cn.com/problems/valid-parentheses/)

4. 项目进度

- [√]  loading加载

## 2021.8.20

1. 文章阅读

- [防抖节流]([新建标签页 (csdn.net)](https://blog.csdn.net/weixin_51458883/article/details/115387650)) 


2. 源码阅读

3. leecode 刷题

- [通配符匹配](https://leetcode-cn.com/problems/wildcard-matching/)

4. 项目进度

- [√]  国际化、切换主题

## 2021.8.19

1. 文章阅读

- [闭包](https://www.liaoxuefeng.com/wiki/1022910821149312/1023021250770016) 

2. 源码阅读

3. leecode 刷题

- [合并两个有序数组](https://leetcode-cn.com/problems/merge-sorted-array/)

4. 项目进度

- [√]  关于页面渲染

## 2021.8.18

1. 文章阅读

- [深入浅出TypeScript：从基础知识到类型编程](https://juejin.cn/book/6844733813021491207) 

2. 源码阅读

3. leecode 刷题

- [杨辉三角](https://leetcode-cn.com/problems/pascals-triangle/)

4. 项目进度

- [√] 搭建项目基本结构

- [√] 获取数据

- [√] 分类

  [√] tag页

## 2021.8.17

1. 文章阅读

- [深入浅出TypeScript：从基础知识到类型编程](https://juejin.cn/book/6844733813021491207) 

2. 源码阅读
3. leecode 刷题



4. 项目进度

- [√] 搭建项目基本结构
- [√] 获取数据
- [√] 首页页面简单搭建 
- [√] 列表组件封装 
- [√] 轮播图

## 2021.8.16

1. 文章阅读

- [async详解](https://blog.csdn.net/Wbiokr/article/details/79490390) 

2. 源码阅读

- [jQuery源码分析系列(11) : 延时对象 - Deferred概念](https://www.cnblogs.com/aaronjs/p/3348569.html)

3. leecode 刷题

- [删除有序数组中的重复项](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-list/)
4. 项目进度

- [√] 搭建项目基本结构
- [√] 获取数据
- [√] 首页页面简单搭建 
- [√] 列表组件封装 

## 2021.8.15

1. 文章阅读

- [BOM核心——window对象之窗口](https://juejin.cn/post/6996527739129823239)

2. 源码阅读


3. leecode 刷题
- [移除元素](https://leetcode-cn.com/problems/remove-element/)
- [合并两个有序数组](https://leetcode-cn.com/problems/merge-sorted-array/)
- [最后一个单词长度](https://leetcode-cn.com/problems/length-of-last-word/)

4. 项目进度

## 2021.8.13
1. 文章阅读

- [ this指向](https://juejin.cn/post/6994233558529212452#heading-1)

- [闭包](https://juejin.cn/post/6995710760928149535)

2. 源码阅读


3. leecode 刷题
- [x的平方根](https://leetcode-cn.com/problems/sqrtx/)

- [实现 strStr() 函数](https://leetcode-cn.com/problems/implement-strstr/)

- [回文数](https://leetcode-cn.com/problems/palindrome-number/)

4. 项目进度

## 2021.8.12

1. 文章阅读

- [数组reduce方法详解](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce) 
- [数组所有方法详解](https://www.cnblogs.com/geqin/p/6955796.html) 

2. 源码阅读

- [jquery整体架构](https://www.cnblogs.com/aaronjs/p/3278578.html)

3. leecode 刷题

- [回文数](https://leetcode-cn.com/problems/palindrome-number/)

- [罗马数字转整数](https://leetcode-cn.com/problems/roman-to-integer/)

- [字符串最长前缀](https://leetcode-cn.com/problems/longest-common-prefix/)

4. 项目进度

## 2021.8.11

1. 文章阅读

- [闭包](https://www.ruanyifeng.com/blog/2009/08/learning_javascript_closures.html) 
- [this指向和优先级](https://blog.csdn.net/MiemieWan/article/details/110471580) 

2. 源码阅读

- [jquery源码框架分析](https://blog.csdn.net/wulex/article/details/99314980)

3. leecode 刷题

- [两数之和](https://leetcode-cn.com/problems/two-sum/)

- [整数反转](https://leetcode-cn.com/problems/reverse-integer/)

- [移除元素](https://leetcode-cn.com/problems/remove-element/)

4. 项目进度

# 袁佳

## 2021.9.15

1. 文章阅读

- [异步编程梳理](https://juejin.cn/post/7002456485674369054)

2. 源码阅读

3. leecode 刷题

- [重新排列数组](https://leetcode-cn.com/problems/shuffle-the-array/)
- [字符串中的最大奇数](https://leetcode-cn.com/problems/largest-odd-number-in-string/)

## 2021.9.14

1. 文章阅读

- [JS垃圾回收机制](https://juejin.cn/post/7002475280577085477)

2. 源码阅读

3. leecode 刷题

- [找出数组最大公约数](https://leetcode-cn.com/problems/find-greatest-common-divisor-of-array/)
- [有效的完全平方数](https://leetcode-cn.com/problems/valid-perfect-square/)

## 2021.9.13

1. 文章阅读

- [JSON.stringify 的 “魅力” ](https://juejin.cn/post/7001454317450297380#heading-14)

- [js鼠标和键盘事件](https://juejin.cn/post/7001138780128149535)

2. 源码阅读


3. leecode 刷题

- [字符串中不同整数的数目](https://leetcode-cn.com/problems/number-of-different-integers-in-a-string/)
- [三除数](https://leetcode-cn.com/problems/three-divisors/)

## 2021.9.12

1. 文章阅读

- [css-bem命名规范](https://juejin.cn/post/7001777832682586119#heading-1)
- [this指向判断](https://juejin.cn/post/7001769945713344525)

2. 源码阅读


3. leecode 刷题

- [将句子排序](https://leetcode-cn.com/problems/sorting-the-sentence/)
- [阶乘尾数](https://leetcode-cn.com/problems/factorial-zeros-lcci/)

## 2021.9.10  
1. 文章阅读

- [前端怎么优雅的写回调函数？](https://segmentfault.com/q/1010000022880990)

2. 源码阅读

3. leecode 刷题

- [正则表达式匹配](https://leetcode-cn.com/problems/regular-expression-matching/)
- [各位相加](https://leetcode-cn.com/problems/add-digits/)


## 2021.9.9
1. 文章阅读

- [JSON.stringify() 、JSON. parse()、eval()详解](https://juejin.cn/post/7005857653297512455?utm_source=gold_browser_extension)
- [for 循环不是目的，map 映射更有意义！](https://juejin.cn/post/7006077858338570270?utm_source=gold_browser_extension)

2. 源码阅读

3. leecode 刷题

- [加油站](https://leetcode-cn.com/problems/gas-station/)
- [分发糖果](https://leetcode-cn.com/problems/candy/)


## 2021.9.8
1. 文章阅读

- [JavaScript系列 - 继承](https://juejin.cn/post/7005464795021312014?utm_source=gold_browser_extension)
- [简述 JavaScript 的事件捕获和事件冒泡](https://juejin.cn/post/7005558885947965454?utm_source=gold_browser_extension)

2. 源码阅读


3. leecode 刷题

- [单词接龙](https://leetcode-cn.com/problems/word-ladder/)
- [求根节点到叶节点数字之和](https://leetcode-cn.com/problems/sum-root-to-leaf-numbers/)

## 2021.9.7
1. 文章阅读

- [防抖、节流](https://juejin.cn/post/6997697142353559560)
- [5种JS函数继承方式](https://juejin.cn/post/7005025080166055967?utm_source=gold_browser_extension)

2. 源码阅读


3. leecode 刷题

- [ 加一](https://leetcode-cn.com/problems/plus-one/)
- [二进制求和](https://leetcode-cn.com/problems/add-binary/)


## 2021.9.6
1. 文章阅读

- [ JS基础篇：12、搞懂防抖、节流](https://juejin.cn/post/6997697142353559560)

- [Vue的路由实现原理: hash模式和history模式](https://juejin.cn/post/6997143974339149854)

2. 源码阅读


3. leecode 刷题

- [转换小写字母](https://leetcode-cn.com/problems/to-lower-case/)
- [字符串中的第一个唯一字符](https://leetcode-cn.com/problems/first-unique-character-in-a-string/)


## 2021.9.5
1. 文章阅读

- [前端9种图片格式基础知识](https://juejin.cn/post/7000154907156152327?utm_source=gold_browser_extension)

2. 源码阅读

3. leecode刷题
   
- [正则表达式匹配](https://leetcode-cn.com/problems/regular-expression-matching/)


## 2021.9.3

1. 文章阅读

- [mobx](https://www.npmjs.com/package/mobx)

2. 源码阅

3. leecode 刷题

- [判断句子是否为全字母句](https://leetcode-cn.com/problems/check-if-the-sentence-is-pangram/)
- [字符串中的单词数](https://leetcode-cn.com/problems/number-of-segments-in-a-string/)


## 2021.9.2
1. 文章阅读

- [闭包](https://www.liaoxuefeng.com/wiki/1022910821149312/1023021250770016) 

2. 源码阅读

3. leecode 刷题

- [合并两个有序数组](https://leetcode-cn.com/problems/merge-sorted-array/)


## 2021.9.1
1. 文章阅读
  
- [常见的跨域解决办法](常见的跨域解决办法)
- [前端安全系列](https://juejin.cn/post/7002133879452598303?utm_source=gold_browser_extension)

1. 源码阅读
  
2. leecode 刷题

- [最后一个单词的长度](https://leetcode-cn.com/problems/length-of-last-word//)
- [ 排列序列](https://leetcode-cn.com/problems/permutation-sequence/)

4. 项目进度

- [x] 搜索记录与访问统计的搜索功能
- [x] 搜索记录与访问统计的分页


## 2021.8.31
1. 文章阅读  

- [cookie和session、localStorage和sessionStorage](https://juejin.cn/post/7002520994434777119?utm_source=gold_browser_extension)
- [前端安全系列](https://juejin.cn/post/7002133879452598303?utm_source=gold_browser_extension)

2. 源码阅读
  
3. leecode 刷题

- [全排列](https://leetcode-cn.com/problems/permutations/)
- [字母异位词分组](https://leetcode-cn.com/problems/group-anagrams/)

4. 项目进度

- [x] 搜索记录与访问统计的数据渲染
- [x] 搜索记录与访问统计的删除功能

## 2021.8.30
1. 文章阅读  

- [前端安全系列](https://juejin.cn/post/7002133879452598303?utm_source=gold_browser_extension)
- [浏览器页面渲染的核心流程详解](https://juejin.cn/post/7001771011053977613?utm_source=gold_browser_extension8)

2. 源码阅读
  
3. leecode 刷题

- [在排序数组中查找元素的第一个和最后一个位置](https://leetcode-cn.com/problems/find-first-and-last-position-of-element-in-sorted-array/)
- [搜索插入位置](https://leetcode-cn.com/problems/search-insert-position/)
- [外观数列](https://leetcode-cn.com/problems/count-and-say/)

4. 项目进度

## 2021.8.29
1. 文章阅读  

- [HTTP 和 HTTPS 的区别](https://blog.csdn.net/qq_38289815/article/details/80969419n)
- [js防抖和节流 区别及实现方式](https://blog.csdn.net/zuorishu/article/details/93630578)

2. 源码阅读
  
3. leecode 刷题

- [寻找两个正序数组的中位数](https://leetcode-cn.com/problems/median-of-two-sorted-arrays/)
- [删除有序数组中的重复项](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array/)
- [下一个排列](https://leetcode-cn.com/problems/next-permutation/)

4. 项目进度

## 2021.8.27
1. 文章阅读  

- [了解promise generator async的原理](https://juejin.cn/post/7001010747400536094?utm_source=gold_browser_extension)

2. 源码阅读
  
3. leecode 刷题

- [寻找两个正序数组的中位数](https://leetcode-cn.com/leetbook/read/tencent/xx6c46/)
- [只出现一次的数字](https://leetcode-cn.com/leetbook/read/tencent/x51e9r/) 

4. 项目进度


## 2021.8.26
1. 文章阅读  

- [vue-router传递参数的几种方式](https://blog.csdn.net/crazywoniu/article/details/80942642)
- [react-router传递参数的方式](https://www.jianshu.com/p/54e0c6317905)

2. 源码阅读
  
3. leecode 刷题

- [两数之和](https://leetcode-cn.com/problems/two-sum/)
- [寻找两个正序数组的中位数](https://leetcode-cn.com/leetbook/read/tencent/xx6c46/) 

4. 项目进度


## 2021.8.25
1. 文章阅读  

- [async/await 的实现原理](https://juejin.cn/post/7000278152953987103?utm_source=gold_browser_extension)
- [Promise实现](https://juejin.cn/post/7000273590650142727?utm_source=gold_browser_extension)

2. 源码阅读
  
3. leecode 刷题

- [爬楼梯](https://leetcode-cn.com/leetbook/read/tencent/x5vk4t/)
- [括号生成](https://leetcode-cn.com/leetbook/read/tencent/x5ku2e/)

4. 项目进度


## 2021.8.24
1. 文章阅读  

- [前端的内存处理](https://juejin.cn/post/6999922082289025032?utm_source=gold_browser_extension)

2. 源码阅读
  
3. leecode 刷题

- [最大子序和](https://leetcode-cn.com/leetbook/read/tencent/x5w3sr/)
- [子集](https://leetcode-cn.com/leetbook/read/tencent/x5zwpv/)

4. 项目进度

- [x] 打包上线

## 2021.8.23
1. 文章阅读  

- [闭包](https://juejin.cn/post/6999442700143427614?utm_source=gold_browser_extension#heading-14)

2. 源码阅读

- [@pluginsugin-dva](https://umijs.org/zh-CN/plugins/plugin-dva)
  
3. leecode 刷题

- [最小栈](https://leetcode-cn.com/explore/interview/card/bytedance/245/data-structure/1049/)
- [ 俄罗斯套娃信封问题](https://leetcode-cn.com/explore/interview/card/bytedance/246/dynamic-programming-or-greedy/1031/)

4. 项目进度

- [x] 响应式布局

## 2021.8.22
1. 文章阅读  

- [Generator 函数的语法](https://es6.ruanyifeng.com/#docs/generator)

2. 源码阅读

- [@umijs/plugin-sass](https://www.npmjs.com/package/@umijs/plugin-sass)
  
3. leecode 刷题

- [字符串相乘](https://leetcode-cn.com/leetbook/read/tencent/xxbolr/)
- [整数反转](https://leetcode-cn.com/leetbook/read/tencent/x5s2oj/)

4. 项目进度

- [x] 知识小册阅读文章底部切换功能

## 2021.8.20
1. 文章阅读  

- [diff 算法](https://juejin.cn/post/6997965021401579556?utm_source=gold_browser_extension)
- [JavaScript 的核心特性--内置对象](https://juejin.cn/post/6998316553817047077?utm_source=gold_browser_extension)
- 
2. 源码阅读

- [ReactElement](https://react.jokcy.me/book/api/react-element.html)
  
3. leecode 刷题

- [实现 strStr() 函数](https://leetcode-cn.com/problems/implement-strstr/)
- [4的幂](https://leetcode-cn.com/problems/power-of-four/)


4. 项目进度

- [x] 知识小册阅读笔记楼层组件复用

## 2021.8.19
1. 文章阅读  

- [ES6 总结](https://juejin.cn/post/6997667470802681870?utm_source=gold_browser_extension)

2. 源码阅读

- [@pluginsugin-dva](https://umijs.org/zh-CN/plugins/plugin-dva)
  
3. leecode 刷题

- [罗马数字转整数](https://leetcode-cn.com/problems/roman-to-integer/)
- [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)

4. 项目进度

- [x] 阅读详情页渲染
- [x] 头部返回跳转功能  
- [x] 楼层高亮功能

## 2021.8.18
1. 文章阅读  

- [函数的底层运行机制](https://juejin.cn/post/6997435434666033189?utm_source=gold_browser_extension)
- [fetch的使用与jQuery.ajax区别](https://blog.csdn.net/qq_40413670/article/details/106470635)

2. 源码阅读

- [Viewer.js](https://www.npmjs.com/package/viewerjs)
  
3. leecode 刷题

- [ 最大子序和](https://leetcode-cn.com/explore/interview/card/bytedance/246/dynamic-programming-or-greedy/1029/)
- [最大正方形](https://leetcode-cn.com/explore/interview/card/bytedance/246/dynamic-programming-or-greedy/1028/)
  
4. 项目进度

- [x] 知识小册笔记 跳转阅读详情
- [x] 知识小册笔记 阅读详情右上方标题跳转阅读详情功能

## 2021.8.17
1. 文章阅读  

- [CSS H5 移动端ios/Android兼容性技巧](https://juejin.cn/post/6997332019445235743?utm_source=gold_browser_extension)
- [javascript包装对象](https://juejin.cn/post/6996665189357584421?utm_source=gold_browser_extension)

2. 源码阅读

- [@umijs/plugin-layout](https://www.npmjs.com/package/@umijs/plugin-layout)
  
3. leecode 刷题

- [除自身以外数组的乘积](https://leetcode-cn.com/leetbook/read/tencent/xxfcwh/)
- [字符串相乘](https://leetcode-cn.com/leetbook/read/tencent/xxbolr/)
  
4. 项目进度

- [x] 知识小册列表详情页背景
- [x] 知识小册右边列表切换详情排版


## 2021.8.16
1. 文章阅读  

- [Promise原理](https://juejin.cn/post/6994594642280857630?utm_source=gold_browser_extension)
- [了解ES6模块化](https://juejin.cn/post/6996273485936869406?utm_source=gold_browser_extension)

2. 源码阅读


3. leecode 刷题
- [旋转数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2skh7/)

- [移动0](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)

- [两数之和](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2jrse/)

4. 项目进度

## 2021.8.15

1. 文章阅读  

- [前端模块化规范](https://juejin.cn/post/6996595779037036580?utm_source=gold_browser_extension)
- [TypeScript-接口](https://juejin.cn/post/6996559915732975630?utm_source=gold_browser_extension)

2. 源码阅读

- [@umijs/plugin-access](https://www.npmjs.com/package/@umijs/plugin-access)

3. leecode 刷题

- [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/tencent/xxynuj/)
- [合并两个有序数组](https://leetcode-cn.com/leetbook/read/tencent/x5mohi/)
- [ 回文数](https://leetcode-cn.com/leetbook/read/tencent/x54pet/)

4. 项目进度

- [x] 知识小册列表的每一项封装
- [x] 动态id跳转知识小册详情


## 2021.8.13

1. 文章阅读  

- [React状态管理的一些思考](https://juejin.cn/post/6995497136510992414?utm_source=gold_browser_extension)
- [什么是 JavaScript 严格模式](https://juejin.cn/post/6995659125510111262?utm_source=gold_browser_extension)
- [深入讲解Ts](https://juejin.cn/post/6994102811218673700?utm_source=gold_browser_extension)

2. 源码阅读

- [dva-loading](https://www.npmjs.com/package/dva-loading)

3. leecode 刷题

- [排列序列](https://leetcode-cn.com/explore/interview/card/bytedance/243/array-and-sorting/1021/)
- [数组中的第K个最大元素](https://leetcode-cn.com/explore/interview/card/bytedance/243/array-and-sorting/1018/)
- [ 复原 IP 地址](https://leetcode-cn.com/explore/interview/card/bytedance/242/string/1044/)

4. 项目进度

- [x] 知识小册目录结构的搭建
- [x] dva状态管理
- [x] 知识小册页面的数据渲染 

## 2021.8.12

1. 文章阅读  

- [Hook简介](https://react.docschina.org/docs/hooks-intro.html)
- [使用 State Hook](https://react.docschina.org/docs/hooks-state.html)
- [使用useEffect](https://react.docschina.org/docs/hooks-effect.html)

1. 源码阅读

- [react-colorful](https://www.npmjs.com/package/react-colorful)

3. leecode 刷题

- [合并区间](https://leetcode-cn.com/explore/interview/card/bytedance/243/array-and-sorting/1046/)
- [  三数之和](https://leetcode-cn.com/explore/interview/card/bytedance/243/array-and-sorting/1020/)
- [ 搜索旋转排序数组](https://leetcode-cn.com/explore/interview/card/bytedance/243/array-and-sorting/1017/)

4. 项目进度


## 2021.8.11

1. 文章阅读  

- [gitlab常用命名](https://www.cnblogs.com/ray-mmss/p/10861703.html)
- [gitlab常见问题解决](https://moxiao.blog.csdn.net/article/details/79411543)

2. 源码阅读

- [vue-js-modal](https://www.npmjs.com/package/vue-js-modal)

3. leecode 刷题

- [无重复字符的最长子串](https://leetcode-cn.com/explore/interview/card/bytedance/242/string/1012/)
- [最长公共前缀](https://leetcode-cn.com/explore/interview/card/bytedance/242/string/1014/)
- [字符串的排列](https://leetcode-cn.com/explore/interview/card/bytedance/242/string/1016/)
- [strStr](https://leetcode-cn.com/problems/implement-strstr/)

4. 项目进度 
